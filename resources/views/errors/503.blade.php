@extends('errors::layout')

@section('title', 'DPM - Manutenzione')

@section('message', 'Data Protection Manager in manutenzione. Il servizio tornerà attivo in breve tempo.')
