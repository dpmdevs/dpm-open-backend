@extends('errors::layout')

@section('title', 'Error')

@section('message', 'Si è verificato un errore inaspettato. Si prega di contattare l’assistenza tecnica.')
