@extends('errors::layout')

@section('title', 'Page Not Found')

@section('message', 'Spiacenti, la pagina richiesta non è stata trovata.')
