<p>Il seguente progetto di analisi d’impatto necessita di essere revisionato:</p>
<ul>
    <li>DPIA Project name: <strong>{{$dpia->name}}</strong></li>
    <li>Goal: <i>{{$dpia->goal}}</i></li>
    <li>Data promemoria: <strong>{{$dpia->review_date->toDateString()}}</strong></li>
</ul>
