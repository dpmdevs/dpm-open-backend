<p>
    Si notifica che è stato modificato un ruolo amministrativo per l’utente
    #{{$user->id}} {{$user->user_number}} {{$user->firstname}} {{$user->lastname}}
</p>
<p>
    Nuovi ruoli:

    @if(count($user->roles) > 0)
    <ul>
        @foreach($user->roles as $role)
            <li>{{$role->display_name}}</li>
        @endforeach
    </ul>
    @else
    <p>Nessun ruolo impostato<p>
    @endif
</p>
