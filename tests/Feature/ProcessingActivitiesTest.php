<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;


use App;
use App\Models\DataType;
use App\Models\Dpo;
use App\Models\ProcessingActivity;
use App\Models\ThirdParty;
use App\Models\User;
use App\Repositories\PARepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProcessingActivitiesTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUrl;
    private $repository;
    private $paData;

    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = 'api/processing-activities';

        $this->repository = new PARepository();
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated

        $this->paData = [
            'name'                => 'test processing activity name',
            'description'         => 'test processing activity description',
            'process_types'       => [1, 2],
            'data_categories'     => [1],
            'data_types'          => [],
            'purpose'             => 'test',
            'legal_basis'         => [],
            'is_active'           => true,
            'data_source_types'   => [App\Models\DataSourceType::find(1)->getKey()],
            'data_sources'        => [factory(ThirdParty::class)->create()->getKey()],
            'identification_code' => 'test processing activity identification code'
        ];
    }

    /**
     * @test
     */
    public function createNewProcessingActivity()
    {
        $this->post($this->apiUrl, $this->paData)->assertStatus(201);

        $lastID = ProcessingActivity::orderBy('id', 'desc')->first()->getAttribute('id');

        $paBasicData = $this->repository->getPABasicData($lastID);

        $this->assertEquals($lastID, $paBasicData->getAttribute('id'));

        $paCompletions = $this->repository->getPACompletionStates($lastID);

        $this->assertEquals(5, count($paCompletions->get()->toArray()));

        $paDataCategories = $this->repository->getPADataCategories($lastID);

        $this->assertEquals(1, count($paDataCategories->get()->toArray()));

        $paDataTypes = $this->repository->getPADataTypes($lastID);

        $this->assertEquals(0, count($paDataTypes->get()->toArray()));
    }

    /**
     * @test
     */
    public function basicDataNotCompletedStep()
    {
        $this->paData['purpose'] = '';

        $paRequest = new App\Http\Requests\PARequest($this->paData);

        $paController = App::make('App\Http\Controllers\ProcessingActivitiesController');

        $this->assertFalse($paController->isBasicDataStepComplete($paRequest));
    }

    /**
     * @test
     */
    public function basicDataCompletedStep()
    {
        $paRequest = new App\Http\Requests\PARequest($this->paData);

        $paController = App::make('App\Http\Controllers\ProcessingActivitiesController');

        $this->assertTrue($paController->isBasicDataStepComplete($paRequest));
    }

    /**
     * @test
     */
    public function updateBasicData()
    {
        $paId = $this->createProcessingActivity()->getAttribute('id');

        $paUpdateRequest = [
            'name'                => 'test processing activity name',
            'description'         => 'updated description',
            'process_types'       => [1],
            'data_categories'     => [1],
            'data_types'          => [factory(DataType::class)->create(['data_category_id' => 1])->getKey()],
            'purpose'             => 'test',
            'legal_basis'         => [],
            'is_active'           => true,
            'data_source_types'   => [App\Models\DataSourceType::find(1)->getKey()],
            'data_sources'        => [factory(ThirdParty::class)->create()->getKey()],
            'identification_code' => 'updated identification code'
        ];

        $this->put("{$this->apiUrl}/{$paId}", $paUpdateRequest)->assertStatus(200);

        $bd = $this->repository->getPABasicData($paId);

        $this->assertEquals('updated description', $bd->getAttribute('description'));
        $this->assertEquals(1, count($bd->getAttribute('processTypes')));
        $this->assertEquals(1, count($bd->getAttribute('dataCategories')));
        $this->assertEquals(1, count($bd->getAttribute('dataTypes')));
        $this->assertEquals(1, count($bd->getAttribute('dataSources')));
    }

    /**
     * @test
     */
    public function storeDataSubjects()
    {
        $dsId = factory(App\Models\DataSubject::class)->create()->getKey();

        $subjectsData = [
            'data_subjects'       => [$dsId],
            'controllers'         => [],
            'internal_processors' => [],
            'external_processors' => [],
            'dpos'                => []
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/{$paId}/subjects", $subjectsData)
            ->assertStatus(200);

        $ds = $this->repository->getPADataSubjects($paId);

        $this->assertEquals($dsId, $ds->get()->first()->getAttribute('id'));
        $this->assertTrue(
            $this->repository->getPACompletionState($paId, 1)->get()->first()->getAttribute('visited')
        );
    }

    /**
     * @test
     */
    public function storeControllers()
    {
        $controllerId = App\Models\Controller::first()->getAttribute('id');
        $dsId = factory(App\Models\DataSubject::class)->create()->getKey();

        $subjectsData = [
            'data_subjects'       => [$dsId],
            'controllers'         => [$controllerId],
            'internal_processors' => [],
            'external_processors' => [],
            'dpos'                => []
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/subjects", $subjectsData)
            ->assertStatus(200);

        $cId = $this->repository
            ->getPAControllers($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($controllerId, $cId);
    }

    /**
     * @test
     */
    public function storeInternalProcessors()
    {
        $processorId = User::all()->first()->getAttribute('id');
        $dsId = factory(App\Models\DataSubject::class)->create()->getKey();

        $subjectsData = [
            'data_subjects'       => [$dsId],
            'controllers'         => [],
            'internal_processors' => [$processorId],
            'external_processors' => [],
            'dpos'                => []
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/subjects", $subjectsData)
            ->assertStatus(200);

        $pId = $this->repository
            ->getPAInternalProcessors($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($processorId, $pId);
    }

    /**
     * @test
     */
    public function storeExternalProcessors()
    {
        $thirdPartiesId = factory(ThirdParty::class)->create()->getKey();
        $dsId = factory(App\Models\DataSubject::class)->create()->getKey();

        $subjectsData = [
            'data_subjects'       => [$dsId],
            'controllers'         => [],
            'internal_processors' => [],
            'external_processors' => [$thirdPartiesId],
            'dpos'                => []
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/subjects", $subjectsData)
            ->assertStatus(200);

        $pId = $this->repository
            ->getPAExternalProcessors($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($thirdPartiesId, $pId);
    }

    /**
     * @test
     */
    public function storeDPOs()
    {
        $dpoId = Dpo::first()->getAttribute('id');
        $dsId = factory(App\Models\DataSubject::class)->create()->getKey();

        $subjectsData = [
            'data_subjects'       => [$dsId],
            'controllers'         => [],
            'internal_processors' => [],
            'external_processors' => [],
            'dpos'                => [$dpoId]
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/subjects", $subjectsData)
            ->assertStatus(200);

        $pId = $this->repository
            ->getPADPOs($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($dpoId, $pId);
    }

    /**
     * @test
     * PARepository::getPAFullData Should Return One Record
     */
    public function regressionForMultiplePA()
    {
        $this->createProcessingActivity();

        $pa = ProcessingActivity::get()->first();
        $res = $this->repository->getPAFullData($pa->getAttribute('id'));

        $this->assertEquals(1, count($res->get()->toArray()));
    }

    /**
     * @test
     */
    public function createDisclosuresIntraUE()
    {
        $disclosureData = [
            'extra_ue'     => false,
            'company_name' => 'test company name',
            'answers'      => []
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/disclosure", $disclosureData)
            ->assertStatus(200);

        $dId = $this->repository
            ->getPADisclosures($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertTrue($dId > 0);
    }

    /**
     * @test
     */
    public function createDisclosuresExtraUE()
    {
        $disclosureData = [
            'extra_ue'     => true,
            'company_name' => 'test company name',
            'answers'      => [
                [
                    'question_id' => App\Models\Question::all()->first()->getAttribute('id'),
                    'answer'      => 'test answer'
                ],
                [
                    'question_id' => App\Models\Question::find(2)->getAttribute('id'),
                    'answer'      => 'test answer 2'
                ]
            ]
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $response = $this->put("{$this->apiUrl}/$paId/disclosure", $disclosureData)
            ->assertStatus(200);

        $disc = $this->repository
            ->getPADisclosures($paId)
            ->get()
            ->first();

        $answers = $disc->answers();

        $this->assertTrue(count($answers->get()->toArray()) === 2);

        $aId = $answers->first()->getAttribute('id');

        $response->assertJson(['id' => $disc->getAttribute('id')]);

        $this->assertTrue($aId > 0);
    }

    /**
     * @test
     */
    public function updateDisclosuresExtraUE()
    {
        $disclosureData = [
            'extra_ue'     => true,
            'company_name' => 'test company name',
            'answers'      => [
                [
                    'question_id' => App\Models\Question::all()->first()->getAttribute('id'),
                    'answer'      => 'test answer'
                ]
            ]
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/disclosure", $disclosureData)
            ->assertStatus(200);

        $dId = $this->repository
            ->getPADisclosures($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $updatedCompanyName = 'test company name updated';

        $disclosureData = [
            'id'           => $dId,
            'extra_ue'     => true,
            'company_name' => $updatedCompanyName,
            'answers'      => [
                [
                    'question_id' => App\Models\Question::all()->first()->getAttribute('id'),
                    'answer'      => 'test answer'
                ]
            ]
        ];

        $this->put("{$this->apiUrl}/$paId/disclosure", $disclosureData)
            ->assertStatus(200);

        $company_name = $this->repository
            ->getPADisclosures($paId)
            ->get()
            ->first()
            ->getAttribute('company_name');

        $this->assertEquals($updatedCompanyName, $company_name);
    }

    /**
     * @test
     */
    public function storeApplications()
    {
        $application = factory(App\Models\Application::class)->create();

        $data = [
            'retentions'                    => [],
            'applications'                  => [$application->getAttribute('id')],
            'risk_estimate'                 => 0,
            'notes'                         => '',
            'retention_description'         => '',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => ''
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)
            ->assertStatus(200);

        $cId = $this->repository
            ->getApplications($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($application->getAttribute('id'), $cId);
    }

    /**
     * @test
     */
    public function storeRetentions()
    {
        $retention = factory(App\Models\Retention::class)->create();

        $data = [
            'retentions'                    => [$retention->getAttribute('id')],
            'applications'                  => [],
            'risk_estimate'                 => 0,
            'notes'                         => '',
            'retention_description'         => '',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => '',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)
            ->assertStatus(200);

        $rId = $this->repository
            ->getRetentions($paId)
            ->get()
            ->first()
            ->getAttribute('id');

        $this->assertEquals($retention->getAttribute('id'), $rId);
    }

    /**
     * @test
     */
    public function storeRiskEstimate()
    {
        $risk_estimate = 1;

        $data = [
            'retentions'                    => [],
            'applications'                  => [],
            'risk_estimate'                 => $risk_estimate,
            'notes'                         => '',
            'retention_description'         => '',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => '',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)->assertStatus(200);

        $DBEstimate = $this->repository->getPAFullData($paId)->get()->first()->getAttribute('risk_estimate');

        $this->assertEquals($risk_estimate, $DBEstimate);
    }

    /**
     * @test
     */
    public function storeNotes()
    {
        $data = [
            'retentions'                    => [],
            'applications'                  => [],
            'risk_estimate'                 => 1,
            'notes'                         => 'test notes',
            'retention_description'         => '',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => '',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)->assertStatus(200);

        $DBNotes = $this->repository->getPAFullData($paId)->get()->first()->getAttribute('notes');

        $this->assertEquals('test notes', $DBNotes);
    }

    /**
     * @test
     */
    public function storeIsProcessAutomated()
    {
        $data = [
            'retentions'                    => [],
            'applications'                  => [],
            'risk_estimate'                 => 1,
            'notes'                         => '',
            'retention_description'         => '',
            'is_process_automated'          => true,
            'has_profiling_activity'        => true,
            'automated_process_description' => '',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)->assertStatus(200);

        $isProcessAutomated = $this->repository->getPAFullData($paId)->get()->first()->getAttribute('is_process_automated');

        $this->assertTrue($isProcessAutomated);
    }

    /**
     * @test
     */
    public function storeRetentionDescription()
    {
        $data = [
            'retentions'                    => [],
            'applications'                  => [],
            'risk_estimate'                 => 1,
            'notes'                         => '',
            'retention_description'         => 'test-ret-description',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => 'Test automated process description',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)->assertStatus(200);

        $retentionDescription = $this->repository->getPAFullData($paId)->get()->first()->getAttribute('retention_description');

        $this->assertEquals('test-ret-description', $retentionDescription);
    }

    /**
     * @test
     */
    public function storeAutomatedProcessDescription()
    {
        $data = [
            'retentions'                    => [],
            'applications'                  => [],
            'risk_estimate'                 => 1,
            'notes'                         => '',
            'retention_description'         => '',
            'is_process_automated'          => false,
            'has_profiling_activity'        => false,
            'automated_process_description' => 'Test automated process description',
        ];

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/other-data", $data)->assertStatus(200);

        $automatedProcessDescription = $this->repository->getPAFullData($paId)->get()->first()->getAttribute('automated_process_description');

        $this->assertEquals('Test automated process description', $automatedProcessDescription);
    }

    /**
     * @test
     */
    public function storeSecurityMeasures()
    {
        $orgSMCategory = factory(App\Models\SecurityMeasureCategory::class)->create();

        factory(App\Models\SecurityMeasure::class)->create([
            'security_level'                  => 1,
            'security_measures_categories_id' => $orgSMCategory->getKey(),
        ]);

        $secMeasures = $orgSMCategory->minSecurityMeasures()->get()->toArray();

        $answers = [];

        foreach ($secMeasures as $secMeasure) {
            $answers['security_measures'][] = $secMeasure['id'];
        }

        $paId = $this->createProcessingActivity()->getAttribute('id');

        $this->put("{$this->apiUrl}/$paId/security-measures", $answers)
            ->assertStatus(200);

        $dbSecMeasure = $this->repository
            ->getSecurityMeasures($paId)
            ->get()
            ->first();

        $this->assertEquals($answers['security_measures'][0], $dbSecMeasure->getAttribute('id'));
    }

    /**
     * @test
     */
    public function toggleUnits()
    {
        $pa = $this->createProcessingActivity();
        $paId = $pa->getAttribute('id');

        $unit1 = factory(App\Models\Unit::class)->create();
        $unit2 = factory(App\Models\Unit::class)->create();

        $unitRequest = [
            'units' => [
                $unit1->getAttribute('id'),
                $unit2->getAttribute('id')
            ]
        ];

        $this->put("{$this->apiUrl}/$paId/units", $unitRequest)
            ->assertStatus(200);

        $associated = $pa->units()->get()->toArray();

        $this->assertEquals($associated[0]['id'], $unit1->getAttribute('id'));
        $this->assertEquals($associated[1]['id'], $unit2->getAttribute('id'));
        $this->assertEquals(2, count($associated));
    }

    /**
     * @test
     */
    public function getAssociatedUnits()
    {
        $pa = $this->createProcessingActivity();
        $paId = $pa->getAttribute('id');

        $unit1Id = factory(App\Models\Unit::class)->create()->getAttribute('id');
        $unit2Id = factory(App\Models\Unit::class)->create()->getAttribute('id');

        $pa->units()->sync([$unit1Id, $unit2Id]);

        $response = $this->get("{$this->apiUrl}/$paId/units");
        $response->assertStatus(200);

        $jResponse = $response->json();

        $this->assertEquals(2, count($jResponse));
        $this->assertEquals($unit1Id, $jResponse[0]['id']);
        $this->assertEquals($unit2Id, $jResponse[1]['id']);
    }

    /**
     * @test
     */

    public function forceDeletePA()
    {
        $pa = $this->createProcessingActivity();

        $paId = $pa->getAttribute('id');

        $this->delete("{$this->apiUrl}/$paId/force");

        $this->assertNull(ProcessingActivity::find($paId));
    }

    /**
     * @return ProcessingActivity
     */
    private function createProcessingActivity(): ProcessingActivity
    {
        return $this->repository->store($this->paData);
    }
}
