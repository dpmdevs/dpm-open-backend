<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;


use App\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RoleControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUrl;
    private $roleData;

    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = 'api/roles';

        $this->roleData = [
            'name'         => 'test-name',
            'display_name' => 'test-display_name',
        ];
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(\App\Models\User::class)->create();
        $this->be($user); //You are now authenticated
    }


    /**
     * @test post endpoint
     *
     */
    public function create()
    {
        $dbRole = $this->postRole($this->roleData);

        $this->assertEquals($this->roleData['name'], $dbRole['name']);
        $this->assertEquals($this->roleData['display_name'], $dbRole['display_name']);
    }

    /**
     * @test post endpoint
     *
     */
    public function update()
    {
        $dbRole = $this->postRole($this->roleData);

        $updatedData = $this->roleData;

        $updatedData['id'] = $dbRole['id'];
        $updatedData['display_name'] = 'mod-display_name';

        $this->put($this->apiUrl . '/' . $dbRole['id'], $updatedData)
            ->assertStatus(200);

        $updatedPerm = Role::find($dbRole['id']);

        $this->assertEquals('mod-display_name', $updatedPerm->getAttribute('display_name'));
    }

    /**
     * @test delete endpoint
     */
    public function destroy()
    {
        $permId = $this->postRole($this->roleData)['id'];

        $this->delete($this->apiUrl . "/$permId")
            ->assertStatus(200);

        $this->assertNull(Role::find($permId));
    }

    private function postRole($perm)
    {
        $this->postJson($this->apiUrl, $perm)
            ->assertStatus(201);

        return Role::where('name', '=', $perm['name'])->first()->toArray();
    }
}