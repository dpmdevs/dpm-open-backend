<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;

use App;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class ThirdPartiesTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUrl;
    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = 'api/third-parties';

        $this->repository = new App\Repositories\ThirdPartiesRepository();
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated
    }

    /**
     * @test
     */
    public function toggleApplications()
    {
        // PREPARE
        $application = factory(App\Models\Application::class)->create();
        $applicationId = $application->getAttribute('id');
        $thirdPartyId = factory(App\Models\ThirdParty::class)->create()->getAttribute('id');
        $putURL = "{$this->apiUrl}/$thirdPartyId/applications";
        $putPayload = ['applications' => [$applicationId]];

        //ATTACH
        $this->put($putURL, $putPayload)->assertStatus(200);

        //VERIFY
        $associatedTP = $this->repository->getApplications($thirdPartyId)[0];

        $this->assertEquals($application->getAttribute('id'), $associatedTP->getAttribute('id'));
        $this->assertEquals($application->getAttribute('company_name'), $associatedTP->getAttribute('company_name'));

        //DETACH
        $this->put($putURL, $putPayload)->assertStatus(200);

        //VERIFY
        $this->assertEmpty($this->repository->getApplications($thirdPartyId));
    }

    /**
     * @test
     */
    public function getExternalProcessor()
    {
        $tp = factory(App\Models\ThirdParty::class)->create();
        $pa = factory(App\Models\ProcessingActivity::class)->create();
        $dc = factory(App\Models\DataCategory::class)->create();
        $ds = factory(App\Models\DataSubject::class)->create();
        $rt = factory(App\Models\Retention::class)->create();

        $pa->dataCategories()->attach($dc);
        $pa->dataSubjects()->attach($ds);
        $pa->retentions()->attach($rt);
        $pa->externalProcessors()->attach($tp);

        $response = $this->get($this->apiUrl . '/external-processors')->json();

        $returnedTP = new App\Models\ThirdParty();

        foreach ($response as $r) {
            if ($r['id'] == $tp->getKey()) $returnedTP = $r;
        }

        $this->assertEquals($returnedTP['id'], $tp->getKey());
        $this->assertEquals($returnedTP['processing_activities'][0]['retentions'][0]['id'], $rt->getKey());
        $this->assertEquals($returnedTP['processing_activities'][0]['data_categories'][0]['id'], $dc->getKey());
        $this->assertEquals($returnedTP['processing_activities'][0]['data_subjects'][0]['id'], $ds->getKey());
    }
}