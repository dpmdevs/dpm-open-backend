<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;

use App\Models\Config;
use App\Models\Controller;
use App\Models\Dpo;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ConfigTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUrl;
    private $controller;
    private $dpo;
    private $config;

    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = 'api/config';
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated
        $this->controller = factory(Controller::class)->create();
        $this->dpo = factory(Dpo::class)->create();
        $this->config = factory(Config::class)->create();

        Config::first()->update($this->config->getAttributes());
    }

    /**
     * @test
     */
    public function toggleController()
    {
        $this->put("{$this->apiUrl}/controller", ['controller_id' => $this->controller->getKey()])
            ->assertStatus(200);

        $this->assertEquals(Config::first()->getAttribute('controller_id'), $this->controller->getKey());
    }

    /**
     * @test
     */
    public function getController()
    {
        $response = $this->get("{$this->apiUrl}/controller")->assertStatus(200);

        $this->assertTrue(count($response->json()) > 0);
    }

    /**
     * @test
     */
    public function getAvailableController()
    {
        $controllers = $this->get("{$this->apiUrl}/available-controllers")->assertStatus(200)->json();

        $associatedController = Config::first()->controller->getKey();

        foreach ($controllers as $controller) {
            $this->assertNotEquals($associatedController, $controller['id']);
        }
    }

    /**
     * @test
     */
    public function toggleDPO()
    {
        $this->put("{$this->apiUrl}/dpo", ['dpo_id' => $this->dpo->getKey()])
            ->assertStatus(200);

        $this->assertEquals(Config::first()->getAttribute('dpo_id'), $this->dpo->getKey());
    }

    /**
     * @test
     */
    public function getDPO()
    {
        $response = $this->get("{$this->apiUrl}/dpo")->assertStatus(200);

        $this->assertTrue(count($response->json()) > 0);
    }

    /**
     * @test
     */
    public function getAvailableDPO()
    {
        $dpos = $this->get("{$this->apiUrl}/available-dpos")->assertStatus(200)->json();

        $associatedDpo = Config::first()->dpo->getKey();

        foreach ($dpos as $dpo) {
            $this->assertNotEquals($associatedDpo, $dpo['id']);
        }
    }
}