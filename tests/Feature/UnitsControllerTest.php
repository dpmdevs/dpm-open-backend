<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;


use App;
use App\Http\Controllers\UnitsController;
use App\Http\Requests\UnitRequest;
use App\Models\User;
use App\Repositories\UnitRepository;
use App\Role;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UnitsControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUri;

    /**
     * @var UnitsController
     */
    private $controller;

    /**
     * @var UnitRepository
     */
    private $repository;

    /**
     * @var Generator
     */
    private $faker;


    public function __construct()
    {
        parent::__construct();

        $this->apiUri = '/api/units';
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); //You are now authenticated

        $this->controller = App::make(UnitsController::class);
        $this->repository = App::make(UnitRepository::class);
        $this->faker = App::make(Generator::class);
    }

    /**
     * @test
     */
    public function createUnitTest()
    {
        $code = $this->faker->text(10);

        $request = new UnitRequest([
            'code'        => $code,
            'name'        => $this->faker->text(20),
            'description' => $this->faker->text(),
        ]);

        $response = $this->controller->createHandler($request);

        $this->assertEquals(201, $response->getStatusCode());

        $unit = $this->repository->findBy('code', $code)->first();

        $this->assertEquals($code, $unit->getAttribute('code'));
    }

    /**
     * @test
     */
    public function createChildUnitTest()
    {
        $baseUnit = factory(App\Models\Unit::class)->create();

        $code = $this->faker->text(10);

        $request = new UnitRequest([
            'code'        => $code,
            'name'        => $this->faker->text(20),
            'description' => $this->faker->text(),
            'parent_id'   => $baseUnit->getAttribute('id')
        ]);

        $response = $this->controller->createHandler($request);

        $this->assertEquals(201, $response->getStatusCode());

        $unit = $this->repository->findBy('code', $code)->first();

        $this->assertEquals($code, $unit->getAttribute('code'));

        $this->assertEquals($code, $baseUnit->getChildren()->first()->getAttribute('code'));
    }

    /**
     * @test
     */
    public function updateUnitTest()
    {
        $unitId = factory(App\Models\Unit::class)->create()->getAttribute('id');

        $updateCode = $this->faker->text(10);

        $request = new UnitRequest([
            'code' => $updateCode,
        ]);

        $response = $this->controller->updateHandler($unitId, $request);

        $this->assertEquals(200, $response->getStatusCode());

        $unit = $this->repository->find($unitId)->first();

        $this->assertEquals($updateCode, $unit->getAttribute('code'));
    }

    /**
     * @test
     */
    public function deleteUnitTest()
    {
        $baseUnit = factory(App\Models\Unit::class)->create();
        $childUnit = factory(App\Models\Unit::class)->create();

        $baseUnit->addChild($childUnit);

        $this->assertEquals($childUnit->getAttribute('id'), $baseUnit->getChildren()->first()->getAttribute('id'));

        $response = $this->controller->delete($childUnit->getAttribute('id'));

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(null, $baseUnit->getChildren()->first());
    }

    /**
     * @test
     */
    public function toggleDirector()
    {
        $baseUnit = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();

        $this->put($this->apiUri . '/' . $baseUnit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->assertCount(1, $baseUnit->directors()->get()->toArray());
        $this->assertEquals($user->id, $baseUnit->directors()->first()->id);
        $this->assertEquals($user->roles()->first()->name, Role::UNIT_DIRECTOR);
    }

    /**
     * @test
     */
    public function toggleSameDirectorOnMultipleUnits()
    {
        $unit = factory(App\Models\Unit::class)->create();
        $unit2 = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);
        $this->put($this->apiUri . '/' . $unit2->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->assertCount(1, $unit->directors()->get()->toArray());
        $this->assertCount(1, $unit2->directors()->get()->toArray());
        $this->assertCount(2, $user->directedUnits()->get()->toArray());

        $this->assertEquals($user->id, $unit->directors()->first()->id);
        $this->assertEquals($user->id, $unit2->directors()->first()->id);

        $this->assertEquals(Role::UNIT_DIRECTOR, $user->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function detachDirector()
    {
        $unit = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();

        //attach
        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        //detach
        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->assertCount(0, $unit->directors()->get());

        $this->assertCount(1, $user->roles()->get());
        $this->assertEquals(Role::USER_IN_CHARGE, $user->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function detachDirectorWithMultipleUnits()
    {
        $unit = factory(App\Models\Unit::class)->create();
        $unit2 = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);
        $this->put($this->apiUri . '/' . $unit2->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->assertCount(0, $unit->directors()->get());
        $this->assertCount(1, $unit2->directors()->get());

        $this->assertCount(1, $user->roles()->get());
        $this->assertEquals(Role::UNIT_DIRECTOR, $user->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function completeDetachDirectorWithMultipleUnits()
    {
        $unit = factory(App\Models\Unit::class)->create();
        $unit2 = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);
        $this->put($this->apiUri . '/' . $unit2->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);
        $this->put($this->apiUri . '/' . $unit2->getAttribute('id') . '/directors', ['directors' => [$user->id]]);

        $this->assertCount(0, $unit->directors()->get());
        $this->assertCount(0, $unit2->directors()->get());

        $this->assertCount(1, $user->roles()->get());
        $this->assertEquals(Role::USER_IN_CHARGE, $user->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function substituteDirector()
    {
        $unit = factory(App\Models\Unit::class)->create();

        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user->id]]);
        $this->put($this->apiUri . '/' . $unit->getAttribute('id') . '/directors', ['directors' => [$user2->id]]);

        $this->assertCount(1, $unit->directors()->get());

        $this->assertCount(1, $user->roles()->get());
        $this->assertEquals(Role::USER_IN_CHARGE, $user->roles()->first()->getAttribute('name'));
        $this->assertCount(1, $user2->roles()->get());

        $this->assertEquals($user2->username, $unit->directors()->first()->username);
    }

    /**
     * @test
     */
    public function getUnitRelatedProcessingActivities()
    {
        $unit = factory(App\Models\Unit::class)->create();
        $pa = factory(App\Models\ProcessingActivity::class)->create();
        $unit->processingActivities()->attach($pa);

        $relatedPa = $this->repository->getUnitRelatedProcessingActivities($unit->getKey())->get();

        $this->assertEquals($pa->getKey(), $relatedPa[0]['id']);
    }
}