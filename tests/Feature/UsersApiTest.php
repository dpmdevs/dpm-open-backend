<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;

use App\Models\Unit;
use App\Models\User;
use App\Repositories\UsersRepository;
use App\Role;
use Hash;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

$_SERVER['REMOTE_ADDR'] = 'unit-test';

class UsersApiTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var UsersRepository
     */
    private $userRepository;
    private $user;
    private $apiUri = '/api/users';

    public function setUp()
    {
        parent::setUp();

        $this->userRepository = app(UsersRepository::class);
        $this->withoutMiddleware();

        $this->user = factory(User::class)->create();
        $this->be($this->user); // User is now authenticated
    }

    /**
     * Test user api endpoint when not auth should return 400 Bad Request
     *
     * @return void
     */
    public function testEndpointWithNoAuth()
    {
        $this->withMiddleware();
        $this->get('api/users')->assertStatus(401);
    }

    /**
     * 422 - Unprocessable Entity
     */
    public function testPOSTWhenEmptyBodyReturns422()
    {
        $this->json('POST', $this->apiUri, [])->assertStatus(422);
    }


    public function testPOSTWhenValidBodyReturns200()
    {
        $this->createTestUser();

        $this->assertEquals(1, User::where('username', '=', 'username')->count());
    }


    public function testUpdateUserEndpoint()
    {
        $user = $this->createTestUser();

        $userId = $user->getAttribute('id');

        $userData = [
            'username'    => 'username',
            'firstname'   => 'updated-firstname',
            'lastname'    => 'updated-lastname',
            'user_number' => 'updated-user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => '',
            'id'          => $userId
        ];

        $this->put($this->apiUri . "/$userId", $userData)
            ->assertStatus(200)
            ->assertSee('{}');

        $user = $user->fresh();

        $this->assertEquals('updated-firstname', $user->getAttribute('firstname'));
    }

    public function testUpdateUserEndpointWhenNotFoundIdShouldReturn404()
    {
        $this->createTestUser();

        $userData = [
            'username'    => 'username',
            'password'    => 'updated-password',
            'firstname'   => 'updated-firstname',
            'lastname'    => 'updated-lastname',
            'user_number' => 'updated-user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];


        // TODO - Laravel default 404 "No query results for model .." page, custom json or message should be preferable

        $this->put($this->apiUri . "/not-valid-id", $userData)
            ->assertStatus(302);

        // firstname same value as before
        $user = User::select('firstname')->where('username', '=', 'username')->first();

        $this->assertEquals('firstname', $user->getAttribute('firstname'));
    }

    public function testDeleteUserEndpoint()
    {
        $userId = factory(User::class)->create()->getKey();

        $this->assertNull(User::find($userId)->getAttribute('deleted_at'));

        $this->delete($this->apiUri . "/$userId")->assertStatus(200);

        $this->assertNotNull(User::withTrashed()->find($userId)->getAttribute('deleted_at'));
    }


    public function testDeleteUserEndpointNotFoundId()
    {
        $this->delete($this->apiUri . "/not-found-id")->assertStatus(404);
    }

    public function testGetLoggedUser()
    {
        $user = factory(User::class)->create();

        $response = $this
            ->actingAs($user, 'api')
            ->get('api/user')
            ->assertStatus(200);

        $this->assertJson($user->toJson(), $response->getContent());
    }

    /**
     * When update password should not be required
     */
    public function testRegressionUpdateWithEmptyPassword()
    {
        $userId = $this->createTestUser()->getAttribute('id');

        $userData = [
            'id'          => $userId,
            'username'    => 'username',
            'firstname'   => 'updated-firstname',
            'lastname'    => 'updated-lastname',
            'user_number' => 'updated-user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];

        $this->put($this->apiUri . "/$userId", $userData)->assertStatus(200);

        $user = User::find($userId);

        $this->assertEquals('updated-firstname', $user->getAttribute('firstname'));
    }


    /**
     * @test
     */
    public function toggleRole()
    {
        /* Set */
        $user = $this->createTestUser();
        $userId = $user->getAttribute('id');

        $roleName = 'test-role';

        $role = Role::create([
            'name'         => $roleName,
            'display_name' => 'test-role-dn'
        ]);

        $roleId = $role->getAttribute('id');

        $this->assertFalse($user->hasRole($roleName));

        /*Expectation*/
        $uri = $this->apiUri . "/$userId/role/$roleId";

        $this->put($uri)->assertStatus(200);

        /*Assertion*/
        $this->assertTrue($user->hasRole($roleName));
    }

    /**
     * @test
     */
    public function getUserRoles()
    {
        /* SET */
        $user = $this->createTestUser();
        $userId = $user->getAttribute('id');

        $roleName = 'test-role';

        $role = Role::create([
            'name'         => $roleName,
            'display_name' => 'test-role-dn'
        ]);

        $user->attachRole($role);

        /*EXPECTATION*/
        $response = $this->get($this->apiUri . "/$userId/roles");

        /*ASSERTIONS*/
        $response->assertStatus(200);
        $this->assertEquals($user->roles()->get()->toJson(), $response->getContent());
    }


    /**
     * @test
     */
    public function getUserAvailableRoles()
    {
        /* SET */
        $user = $this->createTestUser();
        $userId = $user->getAttribute('id');

        $roleName = 'test-role';

        $role = Role::create([
            'name'         => $roleName,
            'display_name' => 'test-role-dn'
        ]);

        $user->attachRole($role);

        $availableRoleName = 'avl-test-role';

        Role::create([
            'name'         => $availableRoleName,
            'display_name' => 'avl-test-role-dn'
        ]);


        /*EXPECTATION*/
        $queryString = 'searchCriteria=[]';
        $responseAvailable = $this->get($this->apiUri . "/$userId/available-roles?$queryString");

        /*ASSERTIONS*/
        $responseAvailable->assertStatus(200);

        $found = false;
        foreach (json_decode($responseAvailable->getContent(), true) as $role) {
            $found = $role['name'] === $availableRoleName;
            if ($found) break;
        }

        $this->assertTrue($found);
    }

    /**
     * @test
     */
    public function withTrashedShouldReturnOnlyDeletedUsers()
    {
        $this->user->setSuperadminRole();
        $user = $this->createTestUser();

        $userId = $user->getAttribute('id');

        $this
            ->delete($this->apiUri . "/$userId")
            ->assertStatus(200)
            ->assertSee('{}');

        $uri = $this->apiUri . $this->creteQueryParams('id', 'desc', 0, 10, true);

        $results = $this->get($uri)->assertStatus(200)->json();

        $found = false;

        foreach ($results['data'] as $result) {
            if ($result['username'] === $user->getAttribute('username')) $found = true;
        }

        $this->assertTrue($found);
    }

    /**
     * @test restore soft deleted user
     * @throws \Exception
     */

    public function restore()
    {
        $this->user->setSuperadminRole();
        $user = $this->createTestUser();
        $user->delete();

        $userId = $user->getAttribute('id');

        $this->put($this->apiUri . "/$userId/restore")
            ->assertStatus(200)
            ->assertSee('{}');


        $uri = $this->apiUri . $this->creteQueryParams('id', 'desc', 0, 10, false);

        $results = $this->get($uri)->json();

        $found = false;

        foreach ($results['data'] as $result) {
            if ($result['username'] === $user->getAttribute('username')) $found = true;
        }

        $this->assertTrue($found);
    }

    /**
     * @test
     */
    public function getUserUnits()
    {
        $u = factory(User::class)->create();
        $uId = $u->getAttribute('id');

        $unit1Id = factory(Unit::class)->create()->getAttribute('id');
        $unit2Id = factory(Unit::class)->create()->getAttribute('id');

        $u->units()->sync([$unit1Id, $unit2Id]);

        $response = $this->get($this->apiUri . "/$uId/working-units");

        $jResponse = $response->json();

        $this->assertEquals(2, count($jResponse));
        $this->assertEquals($unit1Id, $jResponse[0]['id']);
        $this->assertEquals($unit2Id, $jResponse[1]['id']);
    }

    /**
     * @test
     */
    public function toggleUserWorkingUnit()
    {
        $u = factory(User::class)->create();
        $uId = $u->getAttribute('id');

        $unit1 = factory(Unit::class)->create();

        $unitRequest = [
            'unitIds' => [
                $unit1->getAttribute('id')
            ]
        ];

        $this->put("{$this->apiUri}/$uId/working-units", $unitRequest)
            ->assertStatus(200);

        $associated = $u->workingUnits()->get()->toArray();

        $this->assertEquals($associated[0]['id'], $unit1->getAttribute('id'));
        $this->assertCount(1, $associated);
        $this->assertEquals(Role::USER_IN_CHARGE, $u->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function attachUserInChargeUnits()
    {
        $u = factory(User::class)->create();
        $userId = $u->getAttribute('id');

        $unit1 = factory(Unit::class)->create();
        $unit2 = factory(Unit::class)->create();

        $unitRequest = [
            'unitIds' => [
                $unit1->getAttribute('id'),
                $unit2->getAttribute('id')
            ]
        ];

        $this->put("{$this->apiUri}/$userId/in-charge-units", $unitRequest)
            ->assertStatus(200);

        $associated = $u->inChargeUnits()->get()->toArray();

        $this->assertEquals($associated[0]['id'], $unit1->getAttribute('id'));
        $this->assertEquals($associated[1]['id'], $unit2->getAttribute('id'));
        $this->assertCount(2, $associated);
        $this->assertEquals(Role::USER_IN_CHARGE, $u->roles()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function duplicateAttachUserInChargeUnits422ErrorResponse()
    {
        $u = factory(User::class)->create();
        $userId = $u->getAttribute('id');

        $unit1 = factory(Unit::class)->create();

        $u->toggleWorkingUnits([$unit1->getKey()]);

        $unitRequest = [
            'unitIds' => [
                $unit1->getAttribute('id')
            ]
        ];

        $this->put("{$this->apiUri}/$userId/in-charge-units", $unitRequest)
            ->assertStatus(422)
            ->assertJson(['message' => "L’unità " . $unit1->getAttribute('name')
                . " risulta già associata all’utente "
                . $u->getAttribute('firstname') . " " . $u->getAttribute('lastname')]);
    }

    /**
     * @test
     */
    public function detachUserInChargeUnits()
    {
        $u = factory(User::class)->create();
        $userId = $u->getAttribute('id');

        $unit1 = factory(Unit::class)->create();
        $unit2 = factory(Unit::class)->create();

        $unitRequest = [
            'unitIds' => [
                $unit1->getAttribute('id'),
                $unit2->getAttribute('id')
            ]
        ];

        $this->put("{$this->apiUri}/$userId/in-charge-units", $unitRequest)
            ->assertStatus(200);

        $associated = $u->inChargeUnits()->get()->toArray();

        $this->assertEquals($associated[0]['id'], $unit1->getAttribute('id'));
        $this->assertEquals($associated[1]['id'], $unit2->getAttribute('id'));
        $this->assertCount(2, $associated);
        $this->assertEquals(Role::USER_IN_CHARGE, $u->roles()->first()->getAttribute('name'));

        $this->delete("{$this->apiUri}/$userId/in-charge-units/" . $unit1->getKey(), $unitRequest)
            ->assertStatus(200);

        $this->delete("{$this->apiUri}/$userId/in-charge-units/" . $unit2->getKey(), $unitRequest)
            ->assertStatus(200);

        $this->assertCount(0, $u->inChargeUnits()->get());
    }

    /**
     * @test
     */
    public function directedUnits()
    {
        $user = factory(User::class)->create();

        $unit1 = factory(Unit::class)->create();
        $unit2 = factory(Unit::class)->create();
        $unit1->addChild($unit2);

        $user->directedUnits()->sync([$unit1->getKey(), $unit2->getKey()]);

        $directedUnits = $this
            ->actingAs($user)
            ->get($this->apiUri . '/directed-units')
            ->assertStatus(200)
            ->json()
        ['data'];

        $this->assertEquals($directedUnits[0]['id'], $unit1->getKey());
        $this->assertEquals($directedUnits[0]['children'][0]['id'], $unit2->getKey());
    }

    /**
     * @test
     */
    public function directedUnitsWithSubNode()
    {
        $user = factory(User::class)->create();

        $unit1 = factory(Unit::class)->create();
        $unit1->update(['position' => 0]);

        $unit2 = factory(Unit::class)->create();

        $unit1->addChild($unit2);

        $unit3 = factory(Unit::class)->create();
        $unit4 = factory(Unit::class)->create();
        $unit4->update(['position' => 1]);
        $unit3->addChild($unit4);

        $user->directedUnits()->sync([$unit1->getKey(), $unit2->getKey(), $unit4->getKey()]);

        $directedUnits = $this
            ->actingAs($user)
            ->get($this->apiUri . '/directed-units')
            ->assertStatus(200)
            ->json()
        ['data'];

        $this->assertEquals($directedUnits[0]['id'], $unit1->getKey());
        $this->assertEquals($directedUnits[0]['children'][0]['id'], $unit2->getKey());
        $this->assertEquals($directedUnits[1]['id'], $unit4->getKey());
    }

    /**
     * @test
     */
    public function getUsersInChargeAllUnits()
    {
        $this->get($this->apiUri . '/users-in-charge?sortField=id')->assertStatus(200);
    }

    /**
     * @test
     */
    public function getUsersInChargeDirectorUnitFilter()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $unit1 = factory(Unit::class)->create();

        $user->directedUnits()->sync([$unit1->getKey()]);
        $user->setDirectorRole();

        $user2->toggleWorkingUnits([$unit1->getKey()]);
        $user2->toggleDelegateToUnit($unit1->getKey());

        $this
            ->actingAs($user)
            ->get($this->apiUri . '/users-in-charge?sortField=id')
            ->assertStatus(200)
            ->assertJsonCount(1);
    }

    /**
     * @test
     */
    public function getAvailableUsersInCharge()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $unit1 = factory(Unit::class)->create();

        $user->directedUnits()->sync([$unit1->getKey()]);
        $user->setDirectorRole();

        $user2->toggleWorkingUnits([$unit1->getKey()]);

        $this
            ->actingAs($user)
            ->get($this->apiUri . '/available-users-in-charge')
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function getUsersInChargeUnitFilter()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $unit1 = factory(Unit::class)->create();
        $unit2 = factory(Unit::class)->create();
        $user->toggleWorkingUnits([$unit1->getKey()]);
        $user2->toggleWorkingUnits([$unit2->getKey()]);

        $all = $this
            ->get($this->apiUri . '/users-in-charge')
            ->assertStatus(200)
            ->json();

        $this->assertTrue(count($all) > 1);

        $this
            ->get($this->apiUri . '/users-in-charge?unitId=' . $unit1->getKey())
            ->assertStatus(200)
            ->assertJsonCount(1);
    }

    /**
     * @test
     */
    public function toggleDelegatedUnit()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $unit1 = factory(Unit::class)->create();

        $user->directedUnits()->sync([$unit1->getKey()]);
        $user->setDirectorRole();

        $user2->toggleWorkingUnits([$unit1->getKey()]);

        $this->put($this->apiUri . '/' . $user2->getKey() . '/delegate-unit/' . $unit1->getKey());

        $u = $this
            ->actingAs($user)
            ->get($this->apiUri . '/users-in-charge?sortField=id')
            ->assertStatus(200)
            ->assertJsonCount(1)
            ->json();

        $this->assertEquals($unit1->getKey(), $u[0]['delegated_units'][0]['id']);

        $this->put($this->apiUri . '/' . $user2->getKey() . '/delegate-unit/' . $unit1->getKey());

        $u = $this
            ->actingAs($user)
            ->get($this->apiUri . '/users-in-charge?sortField=id')
            ->assertStatus(200)
            ->assertJsonCount(1)
            ->json();

        $this->assertCount(0, $u[0]['delegated_units']);
    }

    /**
     * @test
     */
    public function toggleDelegatedUnitButNotInChargeUnit()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $unit1 = factory(Unit::class)->create();

        $user->directedUnits()->sync([$unit1->getKey()]);
        $user->setDirectorRole();

        $this->put($this->apiUri . '/' . $user2->getKey() . '/delegate-unit/' . $unit1->getKey());

        $u = $this
            ->actingAs($user)
            ->get($this->apiUri . '/users-in-charge?sortField=id')
            ->assertStatus(200)
            ->assertJsonCount(1)
            ->json();

        $this->assertEquals($unit1->getKey(), $u[0]['delegated_units'][0]['id']);
    }

    public function testForceDeleteUserEndpoint()
    {
        $userId = factory(User::class)->create()->getKey();

        $this->assertNotNull(User::find($userId));

        $this->delete($this->apiUri . "/$userId/force")->assertStatus(200);

        $this->assertNull(User::find($userId));
        $this->assertNull(User::withTrashed()->find($userId));
    }


    public function testRegressionOnAvatarNullValue()
    {
        $userData = [
            'username'    => 'username',
            'password'    => '1234A5!a',
            'firstname'   => 'firstname',
            'lastname'    => 'lastname',
            'user_number' => 'user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];

        $this->json('POST', $this->apiUri, $userData)->assertStatus(201);

        $this->assertEquals(1, User::where('username', '=', 'username')->count());
    }

    /**
     * @test
     */
    public function getDirectors()
    {
        $u = factory(User::class)->create();
        $u->setDirectorRole();

        $directors = $this->userRepository->getDirectors('')->get()->toArray();

        $this->assertTrue(count($directors) > 0);

        foreach ($directors as $director)
            $this->assertTrue(User::find($director['id'])->isUnitDirector());
    }

    /**
     * @test
     */
    public function getFilteredDirectors()
    {
        $u = factory(User::class)->create();
        $u->setDirectorRole();

        $jsonQuery = json_encode([["columnName" => "lastname", "keyword" => $u->getAttribute('lastname')]]);

        $directors = $this->get('api/users/directors?searchOrCriteria=' . $jsonQuery)->json();

        $this->assertCount(1, $directors);

        foreach ($directors as $director)
            $this->assertTrue(User::find($director['id'])->isUnitDirector());
    }

    /**
     * @test
     */
    public function samePasswordValidation()
    {
        $password = '4321aD1!s';
        $u = factory(User::class)->create(['password' => $password]);

        $userData = [
            'id'          => $u->getKey(),
            'username'    => 'username',
            'firstname'   => 'firstname',
            'lastname'    => 'lastname',
            'user_number' => 'user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];
        $userData['password'] = $password;

        $this->put($this->apiUri . '/' . $u->getKey(), $userData)
            ->assertStatus(422)
            ->assertSee('{"message":"Errore di validazione","errors":{"password":"Non \u00e8 consentito riutilizzare la stessa password"}}');
    }

    /**
     * @test
     */
    public function changePasswordEndpoint()
    {
        $password = '4321aD1!s';
        $u = factory(User::class)->create(['password' => $password]);

        $invalidPassword = '1234';
        $newPassword = 'new4aDs!';

        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [])
            ->assertStatus(422);

        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [
            'current_password' => $invalidPassword
        ])
            ->assertStatus(422);

        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [
            'current_password' => $invalidPassword,
            'password'         => $newPassword
        ])
            ->assertStatus(422);

        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [
            'current_password' => $password,
            'password'         => $invalidPassword
        ])
            ->assertStatus(422);


        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [
            'current_password' => $password,
            'password'         => $password
        ])
            ->assertStatus(422);

        $this->post($this->apiUri . '/' . $u->getKey() . '/change-pwd', [
            'current_password' => $password,
            'password'         => $newPassword
        ])
            ->assertStatus(200);

        $this->assertTrue(Hash::check($newPassword, User::find($u->getKey())->password));
    }

    /**
     * @return User
     */
    private function createTestUser(): User
    {
        $userData = [
            'username'    => 'username',
            'password'    => 'aaaa123A!',
            'firstname'   => 'firstname',
            'lastname'    => 'lastname',
            'user_number' => 'user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];

        $this->json('POST', $this->apiUri, $userData)
            ->assertStatus(201)
            ->assertSee('{}');

        return User::where('username', '=', 'username')->first();
    }


    /**
     * @param $sort
     * @param $dir
     * @param $start
     * @param $limit
     * @param $trashed
     *
     * @return string
     */
    private function creteQueryParams($sort, $dir, $start, $limit, $trashed): string
    {
        return "?sortField=$sort&sortDirection=$dir&start=$start&limit=$limit&trashed=$trashed";
    }
}
