<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;


use App;
use App\Models\Control;
use App\Models\DPIAProject;
use App\Models\Risk;
use App\Models\User;
use App\Repositories\ControlsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ControlsApiTest extends TestCase
{
    use DatabaseTransactions;

    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new ControlsRepository();
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated

    }

    /**
     * @test
     */
    public function attachExistingControlApi()
    {
        $risk = factory(Risk::class)->create([
            'protection_category_id' => 1,
            'dpia_project_id'        => factory(DPIAProject::class)->create()->getKey()
        ]);

        $apiUrl = "api/risks/{$risk->getKey()}/controls";

        $control = Control::create([
            'name' => 'test'
        ]);

        $data = [
            'control_id'                => $control->getKey(),
            'identifiability'           => 1,
            'prejudicial_effect'        => 1,
            'vulnerability'             => 1,
            'risk_sources_capabilities' => 1
        ];

        $this->post($apiUrl, $data)->assertStatus(200);

        $riskRepo = App::make(App\Repositories\RiskRepository::class);

        $this->assertEquals($control->getKey(), $riskRepo->findRiskById($risk->getKey())->controls()->first()->getKey());
        $this->assertEquals($control->getAttribute('name'), $riskRepo->findRiskById($risk->getKey())->controls()->first()->getAttribute('name'));
    }

    /**
     * @test
     */
    public function updateControlApi()
    {
        $risk = factory(Risk::class)->create([
            'protection_category_id' => 1,
            'dpia_project_id'        => factory(DPIAProject::class)->create()->getKey()
        ]);

        $control = Control::create([
            'name' => 'test'
        ]);

        $apiUrl = "api/risks/{$risk->getKey()}/controls";

        $data = [
            'control_id'                => $control->getKey(),
            'identifiability'           => 1,
            'prejudicial_effect'        => 1,
            'vulnerability'             => 1,
            'risk_sources_capabilities' => 1
        ];

        $this->post($apiUrl, $data)->assertStatus(200);

        $apiUrl = "api/risks/{$risk->getKey()}/controls/{$control->getKey()}";

        $data['identifiability'] = 2;

        $this->put($apiUrl, $data)->assertStatus(200);

        $riskRepo = App::make(App\Repositories\RiskRepository::class);

        $this->assertEquals($control->getKey(),
            $riskRepo->findRiskById($risk->getKey())->controls()->first()->getKey());

        $this->assertEquals(2,
            $riskRepo->findRiskById($risk->getKey())->controls()->first()->pivot->identifiability);
    }
}
