<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;


use App;
use App\Models\DPIAProject;
use App\Models\User;
use App\Repositories\DPIARepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DPIAApiTest extends TestCase
{
    use DatabaseTransactions;

    protected $apiUrl = 'api/dpia';
    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new DPIARepository();
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated
    }

    /**
     * @test
     */
    public function updateReviewDate()
    {
        $dpia = factory(DPIAProject::class)->create();

        $date = '2018-01-01';

        $this->put("{$this->apiUrl}/{$dpia->id}/review-date", ['review_date' => $date])
            ->assertStatus(200);

        $dpia = $this->repository->findDPIAById($dpia->id);

        $this->assertEquals($date, $dpia->getAttribute('review_date')->toDateString());
    }

    /**
     * @test
     */
    public function updateReviewDateNotValid()
    {
        $dpia = factory(DPIAProject::class)->create();

        $date = 'invalid-date';

        $r = $this->put("{$this->apiUrl}/{$dpia->id}/review-date", ['review_date' => $date])
            ->assertStatus(422);

        $this->assertEquals(2, count($r->json()['errors']['review_date']));
    }

    /**
     * @test
     */
    public function regressionOnDeletedProcessingActivity()
    {
        $dpia = factory(DPIAProject::class)->create();

        $p = $dpia->processingActivity()->first();

        $p->delete();

        $sortField = "id";
        $sortDirection = "desc";
        $start = 0;
        $limit = 10;
        $trashed = false;
        $searchParams = "";
        $searchOrParams = "";

        $dpiaResult = App::make(DPIARepository::class)
            ->query($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);

        $this->assertEquals($p->getKey(), $dpiaResult->data[0]['processing_activity']['id']);
    }
}
