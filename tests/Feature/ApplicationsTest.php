<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Feature;

use App;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class ApplicationsTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUrl;
    private $repository;

    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = 'api/applications';

        $this->repository = new App\Repositories\ApplicationRepository();
    }

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(User::class)->create();
        $this->be($user); // User is now authenticated
    }

    /**
     * @test
     */
    public function createWithThirdParty()
    {
        $tpId = factory(App\Models\ThirdParty::class)->create()->getKey();

        $data = [
            'name'          => 'app test',
            'description'   => '',
            'brand'         => '',
            'third_parties' => [$tpId]
        ];

        $this->post("{$this->apiUrl}", $data)
            ->assertStatus(201);

        $app = App\Models\Application::where('name', '=', 'app test')->first();

        $this->assertEquals($app->thirdParties[0]->getKey(), $tpId);
    }

    /**
     * @test
     */
    public function createWithServers()
    {
        $sId = factory(App\Models\Server::class)->create()->getKey();

        $data = [
            'name'        => 'app test',
            'description' => '',
            'brand'       => '',
            'servers'     => [$sId]
        ];

        $this->post("{$this->apiUrl}", $data)
            ->assertStatus(201);

        $app = App\Models\Application::where('name', '=', 'app test')->first();

        $this->assertEquals($app->servers[0]->getKey(), $sId);
    }

    /**
     * @test
     */
    public function toggleTecSecurityMeasures()
    {
        $tecSMCategory = factory(App\Models\SecurityMeasureCategory::class)->create(['family' => 'tec']);

        factory(App\Models\SecurityMeasure::class)->create([
            'security_level'                  => 1,
            'security_measures_categories_id' => $tecSMCategory->getKey(),
        ]);

        $secMeasures = $tecSMCategory->minSecurityMeasures()->get()->toArray();

        $answers = [];

        foreach ($secMeasures as $secMeasure) {
            $answers['security_measures'][] = $secMeasure['id'];
        }

        $appId = factory(App\Models\Application::class)->create()->getAttribute('id');

        $this->put("{$this->apiUrl}/$appId/security-measures", $answers)
            ->assertStatus(200);

        $dbSecMeasure = $this->repository
            ->getSecurityMeasures($appId)
            ->get()
            ->first();

        $this->assertEquals($answers['security_measures'][0], $dbSecMeasure->getAttribute('id'));
    }

    /**
     * @test
     */
    public function toggleThirdParties()
    {
        // PREPARE
        $tp = factory(App\Models\ThirdParty::class)->create();
        $appId = factory(App\Models\Application::class)->create()->getAttribute('id');

        //ATTACH
        $this->put("{$this->apiUrl}/$appId/third-parties", ['thirdParties' => [$tp->getAttribute('id')]])
            ->assertStatus(200);

        //VERIFY
        $associatedTP = $this->repository->getThirdParties($appId)[0];

        $this->assertEquals($tp->getAttribute('id'), $associatedTP->getAttribute('id'));
        $this->assertEquals($tp->getAttribute('company_name'), $associatedTP->getAttribute('company_name'));
        $this->assertEquals($tp->getAttribute('description'), $associatedTP->getAttribute('description'));

        //DETACH
        $this->put("{$this->apiUrl}/$appId/third-parties", ['thirdParties' => [$tp->getAttribute('id')]])
            ->assertStatus(200);

        //VERIFY
        $this->assertEmpty($this->repository->getThirdParties($appId));
    }

    /**
     * @test
     */
    public function getTagThirdPartiesWhenNotAssociated()
    {
        $numOfThirdParties = App\Models\ThirdParty::count();

        factory(App\Models\ThirdParty::class)->create();
        factory(App\Models\ThirdParty::class)->create();

        $appId = factory(App\Models\Application::class)->create()->getAttribute('id');

        $available = $this->get("{$this->apiUrl}/$appId/available-third-parties")->json();

        $associated = $this->repository->getThirdParties($appId);

        $this->assertCount($numOfThirdParties + 2, $available);
        $this->assertCount(0, $associated);
    }

    /**
     * @test
     */
    public function getApplicationsCount()
    {
        $apiCount = $this->get($this->apiUrl. '/count')->assertStatus(200)->json();

        $this->assertEquals(App\Models\Application::count(), $apiCount['count']);
    }
}