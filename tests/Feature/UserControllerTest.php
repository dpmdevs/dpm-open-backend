<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use App\Http\Controllers\UsersController;
use App\Models\User;
use App\Permission;
use App\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $apiUri = '/api/users';

    public function setUp()
    {
        parent::setUp();

        $this->withoutMiddleware();

        $user = factory(\App\Models\User::class)->create();
        $user->setSuperadminRole();
        $this->be($user); //You are now authenticated
    }


    /**
     * @test
     * @dataProvider passwordValidationProvider
     *
     * @param $password
     * @param $status
     * @param $message
     */
    public function testPasswordValidation(string $password, int $status, string $message)
    {
        $userData = [
            'username'    => 'username',
            'firstname'   => 'firstname',
            'lastname'    => 'lastname',
            'user_number' => 'user_number',
            'email'       => '',
            'avatar'      => null,
            'fiscal_code' => ''
        ];

        $userData['password'] = $password;

        $response = $this->json('POST', $this->apiUri, $userData);

        $response->assertStatus($status);

        $this->assertEquals($message, $response->json('errors')['password'][0]);
    }

    public function passwordValidationProvider()
    {
        $emptyPassword = [
            '',
            201,
            ''
        ];

        $atLeast8Chars = [
            '1234567',
            422,
            'Il campo password deve essere lungo almeno 8 caratteri.'
        ];

        $atLeastOneUpperCase = [
            '12345678',
            422,
            'Il campo password deve essere composto da 8 caratteri con minuscole, maiuscole, numeri e caratteri speciali.'
        ];

        $atLeastOneSpecialChar = [
            '1234567A',
            422,
            'Il campo password deve essere composto da 8 caratteri con minuscole, maiuscole, numeri e caratteri speciali.'
        ];

        $atLeastOneNumber = [
            'aaaaaaaAm',
            422,
            'Il campo password deve essere composto da 8 caratteri con minuscole, maiuscole, numeri e caratteri speciali.'
        ];

        $validPassword = [
            'aaaa123A!',
            201,
            ''
        ];

        return [
            $emptyPassword,
            $atLeast8Chars,
            $atLeastOneUpperCase,
            $atLeastOneSpecialChar,
            $atLeastOneNumber,
            $validPassword
        ];
    }

    /**
     * @test
     * @dataProvider permissionsProvider
     *
     * @param array $sections
     * @param array $data
     * @param array $results
     */
    public function createSections(array $sections, array $data, array $results)
    {
        $uc = app(UsersController::class);

        $this->assertEquals($results, $uc->addPermissionToSections($sections, $data));
    }

    public function permissionsProvider()
    {
        $this->createApplication();

        $emptyPermissions = [
            [],
            [],
            []
        ];

        $permission = $this->createTestPermission();

        $section = $this->createSectionFromPermission($permission);

        $emptyPermissionNonEmptySections = [
            [$section],
            [],
            [$section]
        ];

        $samePermission = [
            [$section],
            $permission,
            [$section]
        ];

        $permission2 = $this->createTestPermission();
        $section2 = $this->createSectionFromPermission($permission2);

        $samePermissionMultipleSections = [
            [$section, $section2],
            $permission,
            [$section, $section2]
        ];

        $samePermissionMultipleSections2 = [
            [$section, $section2],
            $permission2,
            [$section, $section2]
        ];

        $permission3 = $this->createTestPermission();

        $subSection = $this->createSectionFromPermission($permission3);
        $subSection['path'] = $section['path'] . "/" . $subSection['path'];

        $section['children'][] = $subSection;

        $subPermission = $this->createTestPermission();
        $subSection2 = $this->createSectionFromPermission($subPermission);
        $subPermission['name'] = $section['path'] . "/" . $subPermission['name'];
        $subSection2['path'] = $section['path'] . "/" . $subSection2['path'];

        $newSection = $section;
        $newSection['children'][] = $subSection2;

        $subSectionPermission = [
            [$section],
            $subPermission,
            [$newSection]
        ];

        $duplicateSubSectionPermission = [
            [$newSection],
            $subPermission,
            [$newSection]
        ];

        return [
            $emptyPermissions,
            $emptyPermissionNonEmptySections,
            $samePermission,
            $samePermissionMultipleSections,
            $samePermissionMultipleSections2,
            $subSectionPermission,
            $duplicateSubSectionPermission
        ];
    }

    public function isSuperAdmin()
    {
        $superAdminUser = factory(User::class)->create();

        $this->assertFalse($superAdminUser->isSuperAdmin());
        $this->assertFalse($superAdminUser->isAdmin());

        $superAdminUser->setSuperadminRole();

        $this->assertTrue($superAdminUser->isSuperAdmin());
        $this->assertTrue($superAdminUser->isAdmin());
    }

    /**
     * @test
     */
    public function standardUser()
    {
        $this->assertEquals(factory(User::class)->create()->isAdmin(), false);
    }

    /**
     * @test
     */
    public function userWithRolesRole()
    {
        $role = factory(Role::class)->create();
        $role->attachPermission(Permission::whereName('subjects/roles')->first());
        $user = factory(User::class)->create();
        $user->attachRole($role);

        $this->assertEquals($user->isAdmin(), true);
    }


    /**
     * @test
     */
    public function userWithUsersRole()
    {
        $userRole = factory(Role::class)->create();
        $userRole->attachPermission(Permission::whereName('subjects/roles')->first());

        $user = factory(User::class)->create();
        $user->attachRole($userRole);

        $this->assertTrue($user->isAdmin());
    }


    /**
     * @test
     */
    public function userWithSuperadminRole()
    {
        $superAdminUser = factory(User::class)->create();
        $superAdminUser->setSuperadminRole();

        $this->assertEquals($superAdminUser->isAdmin(), true);
    }

    /**
     * @param $permission
     * @return array
     */
    private function createSectionFromPermission($permission)
    {
        return [
            'name'      => $permission['display_name'],
            'path'      => $permission['name'],
            'iconClass' => $permission['iconClass']
        ];
    }

    /**
     * @return mixed
     */
    private function createTestPermission()
    {
        $permissionModel = factory(Permission::class)->create();
        $permission = $permissionModel->only(['name', 'display_name', 'iconClass']);
        $permissionModel->forceDelete();

        return $permission;
    }

}
