<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RiskTest extends TestCase
{
    use DatabaseTransactions;

    const NEGLIGIBLE = 1;
    const LIMITED = 2;
    const SIGNIFICANT = 3;
    const MAXIMUM = 4;

    /**
     * @test
     */
    public function defaultRiskLevel()
    {
        $r = $this->createRisk();

        $this->assertEquals(0, $r->getAttribute('identifiability'));
        $this->assertEquals(0, $r->getAttribute('prejudicial_effect'));
        $this->assertEquals(0, $r->getAttribute('vulnerability'));
        $this->assertEquals(0, $r->getAttribute('risk_sources_capabilities'));
        $this->assertEquals(0, $r->getAttribute('severity'));
        $this->assertEquals(0, $r->getAttribute('likelihood'));
        $this->assertEquals(0, $r->getAttribute('risk_level'));
    }

    /**
     * @test
     * @dataProvider riskLevelDataProvider
     *
     * @param array $data
     * @param array $results
     */
    public function calculateRiskLevel(array $data, array $results)
    {
        $r = $this->createRisk();
        $r->setAttribute('identifiability', $data['identifiability']);
        $r->setAttribute('prejudicial_effect', $data['prejudicial_effect']);
        $r->setAttribute('vulnerability', $data['vulnerability']);
        $r->setAttribute('risk_sources_capabilities', $data['risk_sources_capabilities']);
        $r->save();

        $this->assertEquals($results['identifiability'], $r->getAttribute('identifiability'));
        $this->assertEquals($results['prejudicial_effect'], $r->getAttribute('prejudicial_effect'));
        $this->assertEquals($results['vulnerability'], $r->getAttribute('vulnerability'));
        $this->assertEquals($results['risk_sources_capabilities'], $r->getAttribute('risk_sources_capabilities'));
        $this->assertEquals($results['severity'], $r->getAttribute('severity'));
        $this->assertEquals($results['likelihood'], $r->getAttribute('likelihood'));
        $this->assertEquals($results['risk_level'], $r->getAttribute('risk_level'));

        $this->assertEquals($results['risk_level'], $r->residualRisk()->first()->getAttribute('risk_level'));
    }

    public function riskLevelDataProvider()
    {
        $allNegligible = [
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
            ],
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::NEGLIGIBLE
            ]
        ];

        $onlyPrejudicialEffect = [
            [
                'identifiability'           => 0,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => 0,
                'risk_sources_capabilities' => 0,
            ],
            [
                'identifiability'           => 0,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => 0,
                'risk_sources_capabilities' => 0,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::NEGLIGIBLE
            ]
        ];

        $onlyRiskSourcesCapabilities = [
            [
                'identifiability'           => 0,
                'prejudicial_effect'        => 0,
                'vulnerability'             => 0,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
            ],
            [
                'identifiability'           => 0,
                'prejudicial_effect'        => 0,
                'vulnerability'             => 0,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::NEGLIGIBLE
            ]
        ];

        $severityTo2 = [
            [
                'identifiability'           => self::SIGNIFICANT,
                'prejudicial_effect'        => self::LIMITED,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
            ],
            [
                'identifiability'           => self::SIGNIFICANT,
                'prejudicial_effect'        => self::LIMITED,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
                'severity'                  => self::LIMITED,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::LIMITED
            ]
        ];

        $severityTo3 = [
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::LIMITED,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
            ],
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::LIMITED,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
                'severity'                  => self::SIGNIFICANT,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::SIGNIFICANT
            ]
        ];

        $severityTo4 = [
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::MAXIMUM,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
            ],
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::MAXIMUM,
                'vulnerability'             => self::NEGLIGIBLE,
                'risk_sources_capabilities' => self::NEGLIGIBLE,
                'severity'                  => self::MAXIMUM,
                'likelihood'                => self::NEGLIGIBLE,
                'risk_level'                => self::MAXIMUM
            ]
        ];

        $likelihoodTo2 = [
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::SIGNIFICANT,
                'risk_sources_capabilities' => self::LIMITED,
            ],
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::SIGNIFICANT,
                'risk_sources_capabilities' => self::LIMITED,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::LIMITED,
                'risk_level'                => self::LIMITED
            ]
        ];

        $likelihoodTo3 = [
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::SIGNIFICANT,
                'risk_sources_capabilities' => self::SIGNIFICANT,
            ],
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::SIGNIFICANT,
                'risk_sources_capabilities' => self::SIGNIFICANT,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::SIGNIFICANT,
                'risk_level'                => self::SIGNIFICANT
            ]
        ];

        $likelihoodTo4 = [
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::MAXIMUM,
                'risk_sources_capabilities' => self::MAXIMUM,
            ],
            [
                'identifiability'           => self::NEGLIGIBLE,
                'prejudicial_effect'        => self::NEGLIGIBLE,
                'vulnerability'             => self::MAXIMUM,
                'risk_sources_capabilities' => self::MAXIMUM,
                'severity'                  => self::NEGLIGIBLE,
                'likelihood'                => self::MAXIMUM,
                'risk_level'                => self::MAXIMUM
            ]
        ];

        $allMaximum = [
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::MAXIMUM,
                'vulnerability'             => self::MAXIMUM,
                'risk_sources_capabilities' => self::MAXIMUM,
            ],
            [
                'identifiability'           => self::MAXIMUM,
                'prejudicial_effect'        => self::MAXIMUM,
                'vulnerability'             => self::MAXIMUM,
                'risk_sources_capabilities' => self::MAXIMUM,
                'severity'                  => self::MAXIMUM,
                'likelihood'                => self::MAXIMUM,
                'risk_level'                => 16
            ]
        ];

        return [
            $allNegligible,
            $onlyPrejudicialEffect,
            $onlyRiskSourcesCapabilities,
            $severityTo2,
            $severityTo3,
            $severityTo4,
            $likelihoodTo2,
            $likelihoodTo3,
            $likelihoodTo4,
            $allMaximum
        ];
    }

    /**
     * @test
     */
    public function emptyRiskControl()
    {
        $r = $this->createRisk();
        $this->assertCount(0, $r->controls()->get()->toArray());
    }

    /**
     * @test
     */
    public function residualRiskCalculationOnControlAttached()
    {
        $r = $this->createRisk();
        $r->setAttribute('identifiability', self::SIGNIFICANT);
        $r->setAttribute('prejudicial_effect', self::LIMITED);
        $r->setAttribute('vulnerability', self::MAXIMUM);
        $r->setAttribute('risk_sources_capabilities', self::MAXIMUM);
        $r->save();

        $t = App\Models\Control::create([
            'name'        => 't1',
            'threat_type' => 'mitigate'
        ]);

        $r->attachControl($t->getKey(), [
            'vulnerability' => self::LIMITED,
        ]);

        $residualRisk = $r->residualRisk()->first()->toArray();

        $this->assertCount(1, $r->residualRisk()->get()->toArray());
        $this->assertEquals(self::LIMITED, $residualRisk['severity']);
        $this->assertEquals(3, $residualRisk['likelihood']);
        $this->assertEquals(6, $residualRisk['risk_level']);
    }

    /**
     * @test
     */
    public function residualRiskCalculationOnMultipleControls()
    {
        $r = $this->createRisk();
        $r->setAttribute('identifiability', self::MAXIMUM);
        $r->setAttribute('prejudicial_effect', self::MAXIMUM);
        $r->setAttribute('vulnerability', self::MAXIMUM);
        $r->setAttribute('risk_sources_capabilities', self::MAXIMUM);
        $r->save();

        $t = App\Models\Control::create([
            'name'        => 't1',
            'threat_type' => 'mitigate likelihood'
        ]);

        $r->attachControl($t->getKey(), [
            'vulnerability' => self::LIMITED,
        ]);

        $t = App\Models\Control::create([
            'name'        => 't2',
            'threat_type' => 'mitigate severity'
        ]);

        $r->attachControl($t->getKey(), [
            'identifiability' => self::SIGNIFICANT,
        ]);

        $residualRisk = $r->residualRisk()->first()->toArray();

        $this->assertCount(1, $r->residualRisk()->get()->toArray());
        $this->assertEquals(3, $residualRisk['likelihood']);
        $this->assertEquals(2, $residualRisk['severity']);
        $this->assertEquals(3 * 2, $residualRisk['risk_level']);
    }

    /**
     * @test
     */
    public function updateAcceptanceMotivation()
    {
        $r = $this->createRisk();

        $this->withoutMiddleware()
            ->put('api/risks/' . $r->getKey() . '/acceptance-motivation', ['acceptance_motivation' => 'test'])
            ->assertStatus(200);

        $r = App\Models\Risk::find($r->getKey());

        $this->assertEquals('test', $r->getAttribute('acceptance_motivation'));
    }

    /**
     * @return App\Models\Risk
     */
    private function createRisk()
    {
        return factory(App\Models\Risk::class)->create([
            'protection_category_id' => 1,
            'dpia_project_id'        => factory(App\Models\DPIAProject::class)->create()->getKey()
        ]);
    }
}
