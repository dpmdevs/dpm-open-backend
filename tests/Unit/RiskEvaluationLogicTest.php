<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Tests\Unit;

use App\Libs\RiskEvaluationLogic;
use Tests\TestCase;

class RiskEvaluationLogicTest extends TestCase
{
    /**
     * @test
     */
    public function createObj()
    {
        $this->assertInstanceOf(RiskEvaluationLogic::class, new RiskEvaluationLogic());
    }

    /**
     * @test
     * @dataProvider mapSeverityAndLikelihoodProvider
     *
     * @param int $expected
     * @param $value
     */
    public function mapSeverityAndLikelihood(int $expected, $value)
    {
        $this->assertEquals($expected, RiskEvaluationLogic::map($value));
    }

    /**
     * @return array
     */
    public function mapSeverityAndLikelihoodProvider()
    {
        return [
            [1, 0],
            [1, 1],
            [1, 2],
            [1, 3],
            [1, 4],
            [2, 5],
            [3, 6],
            [4, 7],
            [4, 8],
            [1, -1],
            [1, '1'],
        ];
    }

    /**
     * @test
     * @dataProvider calculateDataProvider
     *
     * @param int $expected
     * @param int $identifiability
     * @param int $prejudicial_effect
     */
    public function calculateSeverity(int $expected, int $identifiability, int $prejudicial_effect)
    {
        $this->assertEquals($expected, RiskEvaluationLogic::calculateSeverity($identifiability, $prejudicial_effect));
    }

    /**
     * @test
     * @dataProvider calculateDataProvider
     *
     * @param int $expected
     * @param int $vulnerability
     * @param int $risk_sources_capabilities
     */
    public function calculateLikelihood(int $expected, int $vulnerability, int $risk_sources_capabilities)
    {
        $this->assertEquals($expected, RiskEvaluationLogic::calculateLikelihood($vulnerability, $risk_sources_capabilities));
    }

    /**
     * @return array
     */
    public function calculateDataProvider()
    {
        return [
            [1, 1, 1],
            [1, 2, 1],
            [1, 2, 2],
            [1, 3, 1],
            [2, 3, 2],
            [3, 3, 3],
            [2, 4, 1],
            [3, 4, 2],
            [4, 4, 3],
            [4, 4, 4],
        ];
    }

    /**
     * @test
     */
    public function calculateRiskLevel()
    {
        $this->assertEquals(6, RiskEvaluationLogic::calculateRiskLevel(2, 3));
    }

}