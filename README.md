## Requirements
* PHP 7.2+
* MySQL 5.6+ | MariaDB 15+
* Composer v1.5+
* Nodejs v8+

## DPM Backend
Clone dpm backend into web server applications folder.

```bash
git clone https://bitbucket.org/dpmdevs/dpm-open-backend.git 
cd dpm-open-backend && composer install
```

### Create environment file .env and application key
Fill DB_* constants with database name, username and password

```bash
echo "APP_ENV=production
APP_KEY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=dev-connection
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dpm
DB_USERNAME=????
DB_PASSWORD=????

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
" > .env

php artisan key:generate
php artisan jwt:secret
```

### Create database and schema
```bash
echo "create database dpm;" | mysql
cd dpm-open-backend && php artisan migrate --seed
```

### Validate dpm-open-backend installation
```bash
./vendor/bin/phpunit
```

### DPM Frontend
```bash
git clone https://bitbucket.org/dpmdevs/dpm-open-ui.git
cd dpm-open-ui && npm install
ln -s ../assets src/assets
```

### Create environment files

Create file environment.prod.ts under src/environments with this content:

```typescript
import { Environment } from './environment.interface';
 
const serverPort = '';
export const serverUrl = location.protocol + '//' + location.hostname + ':' + serverPort + '/';
 
export const environment: Environment = {
    production: true,
    serverUrl,
    apiUrl: `${serverUrl}api/`,
    authUrl: `${serverUrl}api/auth/`,
    customerLogo: ''
};
```

### Deploy UI into back end
```bash
npm run deploy:default
mv build/dist-new build/dist
ln -s build/dist [dpm backend path]/public/dist
```

### Apache 2 conf example
```apacheconfig
<VirtualHost *:80>

    ServerAdmin webmaster@localhost

    DocumentRoot /var/www/dpm-open-backend/public


    <Directory /var/www/dpm-open-backend/public >
            Options FollowSymLinks MultiViews
            AllowOverride all
            Order allow,deny
            allow from all
    </Directory>


	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

Default login credentials: superadmin/superadmin
