<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'IndexController@index');


// CONVENIENT ROUTE FOR TESTING EMAIL
//Route::get('/mailable', function () {
//    return new App\Mail\DPIAReviewReminder(\App\Models\DPIAProject::find(1));
//});