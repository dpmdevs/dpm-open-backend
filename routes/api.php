<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {

    Route::post('login', 'Auth\AuthController@login');

    Route::get('check-user-authentication', 'Auth\AuthController@checkUserAuthentication');

    Route::get('logout', 'Auth\AuthController@logout')->middleware('jwt.auth');
});

Route::group(['middleware' => 'jwt.auth'], function () {

    /*
     * Applications
     */
    App::make(\App\Http\Controllers\Routers\ApplicationsRouter::class)->makeRoutes();

    /*
     * Config
     */
    App::make(\App\Http\Controllers\Routers\ConfigRouter::class)->makeRoutes();

    /*
     * Data categories
     */
    App::make(\App\Http\Controllers\Routers\DataCategoriesRouter::class)->makeRoutes();

    /*
     * Data Subjects
     */
    App::make(\App\Http\Controllers\Routers\DataSubjectsRouter::class)->makeRoutes();

    /*
     * Legal Basis
     */
    App::make(\App\Http\Controllers\Routers\LegalBasisRouter::class)->makeRoutes();


    /*
     * Processing activities completion steps
     */
    \App\Http\Controllers\PACompletionController::routes();

    /*
     * Processing activities
     */
    App::make(\App\Http\Controllers\Routers\ProcessingActivityRouter::class)->makeRoutes();

    /*
     * Questionnaire
     */
    \App\Http\Controllers\QuestionnaireController::routes();

    /*
      * Retentions
      */
    App::make(\App\Http\Controllers\Routers\RetentionRouter::class)->makeRoutes();

    /*
     * Role Routes
     */
    App::make(\App\Http\Controllers\Routers\RolesRouter::class)->makeRoutes();


    /*
     * Min security measures categories
     */
    App::make(\App\Http\Controllers\Routers\SecurityMeasuresRouter::class)->makeRoutes();

    /*
     * Third parties
     */
    App::make(\App\Http\Controllers\Routers\ThirdPartiesRouter::class)->makeRoutes();

    /*
     * Units
    */
    App::make(\App\Http\Controllers\Routers\UnitsRouter::class)->makeRoutes();

    /*
    * User Routes
    */
    App::make(\App\Http\Controllers\Routers\UsersRoutes::class)->makeRoutes();

    /*
    * Servers
    */
    App::make(\App\Http\Controllers\Routers\ServersRouter::class)->makeRoutes();

    /*
    * DPIA
    */
    App::make(\App\Http\Controllers\Routers\DPIARoutes::class)->makeRoutes();
    \App\Http\Controllers\ProtectionCategoriesController::routes();
    \App\Http\Controllers\RiskSourcesController::routes();
    App::make(\App\Http\Controllers\Routers\RisksRoutes::class)->makeRoutes();
    App::make(\App\Http\Controllers\Routers\ControlsRouter::class)->makeRoutes();

    /*
     * Qualifications
     */
    App::make(\App\Http\Controllers\Routers\QualificationsRouter::class)->makeRoutes();

    /*
     * Controllers
     */
    App::make(\App\Http\Controllers\Routers\ControllersRouter::class)->makeRoutes();

    /*
     * DPOs
     */
    App::make(\App\Http\Controllers\Routers\DposRouter::class)->makeRoutes();

    /*
     * Operating Systems
     */
    App::make(\App\Http\Controllers\Routers\OSsRoutes::class)->makeRoutes();
});

Route::get('events/last', function () {
    return response()->json(['name' => 'ultima modifica', 'timestamp' => \Carbon\Carbon::yesterday()->timestamp]);
});
