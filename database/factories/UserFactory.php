<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'username'    => $faker->unique()->userName,
        'password'    => $faker->unique()->regexify('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/'),
        'firstname'   => $faker->firstName,
        'lastname'    => $faker->lastName,
        'email'       => $faker->unique()->safeEmail,
        'user_number' => $faker->unique()->numberBetween(),
        'fiscal_code' => $faker->unique()->regexify('^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z][0-9]{3}[A-Z]$'),
    ];
});
