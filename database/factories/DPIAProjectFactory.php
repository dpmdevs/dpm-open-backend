<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DPIAProject;
use Carbon\Carbon;

$factory->define(DPIAProject::class, function (Faker\Generator $faker) {
    return [
        'name'                   => $faker->text(10),
        'status'                 => 'in corso',
        'goal'                   => $faker->text(20),
        'user_id'                => function () {
            return factory(\App\Models\User::class)->create()->getKey();
        },
        'processing_activity_id' => function () {
            $p = factory(\App\Models\ProcessingActivity::class)->create();
            $p->processTypes()->attach(2);

            return $p->getKey();
        },
        'review_date'            => Carbon::now()->addYear()->toDateString(),
        'proportionality_reason'  => ''
    ];
});
