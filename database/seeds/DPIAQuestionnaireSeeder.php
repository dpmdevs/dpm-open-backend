<?php

use Illuminate\Database\Seeder;

class DPIAQuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDataSubjectsRightQst();
        $this->createPrinciplesQst();
    }

    private function createDataSubjectsRightQst()
    {
        $quest = \App\Models\Questionnaire::create([
                'code' => 'dpia-data-subjects-rights',
                'name' => 'Vengono garantiti i seguenti diritti agli interessati?'
            ]
        );

        $quest->questions()->createMany([
            ['question' => 'Accesso'],
            ['question' => 'Rettifica'],
            ['question' => 'Cancellazione'],
            ['question' => 'Portabilità'],
            ['question' => 'Opposizione'],
        ]);
    }

    private function createPrinciplesQst()
    {
        $quest = \App\Models\Questionnaire::create([
                'code' => 'dpia-principles',
                'name' => 'Sono presi in considerazione i seguenti principi?'
            ]
        );

        $quest->questions()->createMany([
            [
                'title'    => 'Liceità, Correttezza e Trasparenza',
                'question' => 'I dati personali sono trattati in modo <strong>lecito, corretto e trasparente</strong> nei confronti dell’interessato'
            ],
            [
                'title'    => 'Limitazione della finalità',
                'question' => 'I dati sono raccolti per <strong>finalità determinate, esplicite e legittime</strong>, e successivamente trattati in modo che non sia incompatibile con tali finalità'
            ],
            [
                'title'    => 'Limitazione della conservazione',
                'question' => 'I dati sono conservati in una forma che consenta l’identificazione degli interessati <strong>per un arco di tempo non superiore al conseguimento delle finalità per le quali sono trattati</strong>'
            ],
            [
                'title'    => 'Integrità e riservatezza',
                'question' => '<strong>I dati sono trattati in maniera da garantire un’adeguata sicurezza dei dati personali</strong>'
            ]
        ]);
    }
}
