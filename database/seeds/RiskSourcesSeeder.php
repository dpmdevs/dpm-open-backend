<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Seeder;

class RiskSourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRiskSource('Insider', 'persons who belong to the organisation: user, system operator, grid operator, service operator, call centre operator, commercial service employee');
        $this->createRiskSource('Outsider', 'persons from outside the organisation: recipient, provider, competitor, authorized third party, government organisation, human activity surrounding, external/sub-contracted maintenance');
        $this->createRiskSource('Machine', 'non-human sources: corrupt sensor, computer virus, natural disaster such as lightning, energy imbalance, energy disruption an outage.');
    }

    /**
     * @param $name
     * @param $description
     */
    private function createRiskSource($name, $description)
    {
        \App\Models\RiskSource::create([
            'name'        => $name,
            'description' => $description,
        ]);
    }
}
