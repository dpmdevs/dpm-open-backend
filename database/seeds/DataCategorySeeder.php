<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use App\Models\DataCategory;
use Illuminate\Database\Seeder;

class DataCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDataCategory('Anagrafici', '', 'personal');
        $this->createDataCategory('Caratteristiche anagrafiche', '', 'personal');
        $this->createDataCategory('Finanziari', '', 'personal');
        $this->createDataCategory('Professione e istruzione', '', 'personal');
        $this->createDataCategory('Etnia e opinioni', '', 'sensitive');
        $this->createDataCategory('Sanitari', '', 'sensitive');
        $this->createDataCategory('Biometrici', '', 'sensitive');
        $this->createDataCategory('Sanitari super sensibili', '', 'sensitive');
        $this->createDataCategory('Giudiziari', '', 'sensitive');
    }

    /**
     * @param $name
     * @param $description
     * @param $family
     */
    private function createDataCategory($name, $description, $family)
    {
        DataCategory::create([
            'name'        => $name,
            'description' => $description,
            'family'      => $family,
        ]);
    }
}
