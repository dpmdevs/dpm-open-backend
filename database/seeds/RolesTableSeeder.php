<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use App\Models\User;
use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superRole = Role::create([
            'name'         => Role::SUPERADMIN,
            'display_name' => 'admin',
            'description'  => 'Adminstration role',
        ]);

        $superRole->attachPermissions(Permission::all());

        $adminUserData = [
            'username'    => 'superadmin',
            'password'    => 'superadmin',
            'firstname'   => 'Administrator',
            'lastname'    => 'Administrator',
            'email'       => '',
            'user_number' => -1000,
            'fiscal_code' => '',
        ];

        $superAdmin = User::create($adminUserData);

        $superAdmin->update(['id' => 0]);
        $superAdmin->attachRole($superRole);

        $this->createDirectorRole();

        $this->userInChargeRole();
    }


    private function createDirectorRole()
    {
        $unitDirectorRole = Role::create([
            'name'         => Role::UNIT_DIRECTOR,
            'display_name' => 'Responsabile unità operativa',
            'description'  => 'Responsabile funzionale di unità operativa',
        ]);

        $unitDirectorRole->attachPermissions([
            Permission::findByName('p-activities'),
            Permission::findByName('about'),
        ]);
    }

    private function userInChargeRole()
    {
        $role = Role::create([
            'name'         => Role::USER_IN_CHARGE,
            'display_name' => 'Incaricato',
            'description'  => 'Incaricato al trattamento',
        ]);

        $role->attachPermissions([
            Permission::findByName('about'),
        ]);
    }

}

