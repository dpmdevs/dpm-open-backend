<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createPermission('dashboard', '', 'fa fa-tachometer');
        $this->createPermission('p-activities', 'Reg. Trattamenti', 'fa fa-book');
        $this->createPermission('dpia', 'Analisi d’impatto', 'fa fa-exclamation-triangle');


        $this->createPermission('assets', 'Asset', 'fa fa-pencil-square-o');
        $this->createPermission('assets/applications', 'Applicativi');
        $this->createPermission('assets/data-categories', 'Categorie di dati');
        $this->createPermission('assets/data-subjects', 'Categorie interessati');
        $this->createPermission('assets/controls', 'Controlli di sicurezza');
        $this->createPermission('assets/retentions', 'Conservazione');
        $this->createPermission('assets/security-measures', 'Misure di sicurezza');
        $this->createPermission('assets/servers', 'Server');
        $this->createPermission('subjects', 'Soggetti', 'fa fa-users');
        $this->createPermission('subjects/dpos', 'DPO');
        $this->createPermission('subjects/qualifications', 'Qualifiche');
        $this->createPermission('subjects/roles', 'Ruoli');
        $this->createPermission('subjects/third-parties', 'Terze parti');
        $this->createPermission('subjects/controllers', 'Titolari');
        $this->createPermission('subjects/units', 'Unità');
        $this->createPermission('subjects/users', 'Utenti');
        $this->createPermission('config', 'Configurazione', 'fa fa-cog');
        $this->createPermission('about', 'About', 'fa fa-info');
    }

    private function createPermission(string $name, string $display_name = '', string $iconClass = '')
    {
        if ($display_name === '') $display_name = ucfirst($name);

        Permission::create([
            'name'         => $name,
            'display_name' => $display_name,
            'iconClass'    => $iconClass,
        ]);
    }
}
