<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Seeder;

class QuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disclosureQuestionnaire();

        $this->dpiaPreAssessmentQuestionnaire();
    }

    private function disclosureQuestionnaire()
    {
        $quest = \App\Models\Questionnaire::create([
                'code' => 'disclosure-group-1',
                'name' => 'Trasferimento sulla base di una decisione di adeguatezza'
            ]
        );

        $quest->questions()->createMany([
            ['question' => 'AD - Andorra 2010/625/EU'],
            ['question' => 'AR - Argentina 2003/490/EC'],
            ['question' => 'CA - Canada 2002/2/EC'],
            ['question' => 'CH - Svizzera 2000/518/EC'],
            ['question' => 'FO - Isole Faeroe 2010/146/EU'],
            ['question' => 'GG - Guernsey 2003/821/EC'],
            ['question' => 'IL - State of Israel 2011/61/EU'],
            ['question' => 'IM - Isle of Man 2004/411/EC'],
            ['question' => 'JE - Jersey 2008/393/EC'],
            ['question' => 'NZ - New Zealand 2013/65/EU'],
            ['question' => 'US - United States - Privacy Shield  2016/1250/EU'],
            ['question' => 'UY - Eastern Republic of Uruguay 2012/484/EU'],
        ]);

        $quest = \App\Models\Questionnaire::create([
                'code' => 'disclosure-group-2',
                'name' => 'Trasferimento soggetto a garanzie adeguate'
            ]
        );

        $quest->questions()->createMany([
            ['question' => 'uno strumento giuridicamente vincolante e avente efficacia esecutiva tra autorità pubbliche o organismi pubblici'],
            ['question' => 'le norme vincolanti d’impresa (BCR)'],
            ['question' => 'le clausole tipo di protezione dei dati adottate dalla Commissione (Eu Standard Clause)'],
            ['question' => 'le clausole tipo di protezione dei dati adottate da un’autorità di controllo (National Standard Clause)'],
            ['question' => 'un codice di condotta'],
            ['question' => 'un meccanismo di certificazione'],
        ]);

        $quest = \App\Models\Questionnaire::create([
                'code' => 'disclosure-group-3',
                'name' => 'Deroghe in specifiche situazioni'
            ]
        );

        $quest->questions()->createMany([
            ['question' => 'il trasferimento sia necessario all’esecuzione di un contratto concluso tra l’interessato e il titolare del trattamento ovvero all’esecuzione di misure precontrattuali adottate su istanza dell’interessato'],
            ['question' => 'il trasferimento sia necessario per la conclusione o l’esecuzione di un contratto stipulato tra il titolare del trattamento e un’altra persona fisica o giuridica a favore dell’interessato'],
            ['question' => 'il trasferimento sia necessario per importanti motivi di interesse pubblico'],
            ['question' => 'il trasferimento sia necessario per accertare, esercitare o difendere un diritto in sede giudiziaria'],
            ['question' => 'il trasferimento sia necessario per tutelare gli interessi vitali dell’interessato o di altre persone, qualora l’interessato si trovi nell’incapacità fisica o giuridica di prestare il proprio consenso'],
            ['question' => 'il trasferimento sia effettuato a partire da un registro che, a norma del diritto dell’Unione o degli Stati membri, mira a fornire informazioni al pubblico e può esser consultato tanto dal pubblico in generale quanto da chiunque sia in grado di dimostrare un legittimo interesse (WARNING--Il trasferimanto non può riguardare la totalità dei dati personali o intere categorie di dati personali contenute nel registro. Se il registro è destinato a essere consultato da persone aventi un legittimo interesse, il trasferimento è ammesso soltanto su richiesta di tali persone o qualora tali persone ne siano i destinatari.)'],
        ]);
    }

    private function dpiaPreAssessmentQuestionnaire()
    {
        $quest = \App\Models\Questionnaire::create([
                'code' => 'dpia-pre-assessment',
                'name' => 'Indicare quali dei seguenti criteri sono applicabili all’attività di trattamento analizzata'
            ]
        );

        $quest->questions()->createMany([
            [
                'title'    => 'Trattamenti valutativi o di scoring',
                'question' => "Trattamenti valutativi o di scoring: compresa la profilazione e attività predittive, in particolare a partire da “aspetti riguardanti il rendimento professionale, la situazione economica, la salute, le preferenze o gli interessi personali, l’affidabilità o il comportamento, l’ubicazione o gli spostamenti dell’interessato”.<br />
                    <br />
                    <i>Esempi:</i>
                    <ul>
                        <li>Un istituto finanziario che effettui lo screening dei propri clienti utilizzando un database di rischio creditizio ovvero un database per la lotta alle frodi o al riciclaggio e al finanziamento del terrorismo (AML/CTF)</li>
                        <li>Una società che crei profili comportamentali o di marketing a partire dalle operazioni o dalla navigazione compiute sul proprio sito web.</li>
                    </ul>      
                "
            ],
            [
                'title'    => 'Decisioni automatizzate che producono significativi effetti giuridici o di analoga natura',
                'question' => "Trattamenti finalizzati ad assumere decisioni su interessati che producano “effetti giuridici sulla persona fisica”. Per esempio, il trattamento può comportare l’esclusione di una persona fisica da determinati benefici ovvero la sua discriminazione.<br />
                    <br />
                    <i>Esempi:</i>
                    <ul>
                        <li>Calcolo del premio assicurativo automobilistico in maniera automatica sulla base del comportamento alla guida degli assicurati</li>
                        <li>Calcolo del plafond della carta di credito in maniera automatizzata in base a dati di credito “non convenzionali”, come la residenza e le abitudini di consumo dell’interessato</li>
                    </ul>     
                "
            ],
            [
                'title'    => 'Monitoraggio sistematico',
                'question' => "Trattamenti utilizzati per osservare, monitorare o controllare gli interessati, compresa la raccolta di dati attraverso reti o “la sorveglianza sistematica di un’area accessibile al pubblico”. In particolare quando per gli interessanti non sia possibile sottrarsi a questa tipologia di trattamenti. <br />
                    <br />
                    <i>Esempi:</i>
                    <ul>
                        <li>Sistema di controllo accessi al centro storico di un comune tramite videosorveglianza e riconoscimento delle targhe</li>
                        <li>Azienda che controlla sistematicamente le attività dei dipendenti, compreso l’utilizzo dei terminali informatici, la navigazione su Internet, ecc</li>
                    </ul>   
                "
            ],
            [
                'title'    => 'Dati sensibili o dati di natura estremamente personale',
                'question' => "Trattamenti che coinvolgano categorie particolari di dati personali di cui all’art. 9 (per esempio, informazioni sulle opinioni politiche di una persona fisica) oltre ai dati personali relativi a condanne penali o reati di cui all’art. 10. <br />
                <br />
                <i>Esempi:</i>
                    <ul>
                        <li>Un ospedale che conserva le cartelle cliniche dei pazienti</li>
                        <li>Un investigatore privato che conserva informazioni su soggetti responsabili di reati</li>
                    </ul>
                "
            ],
            [
                'title'    => 'Trattamento di dati su larga scala',
                'question' => "Si prendano in considerazione i seguenti fattori al fine di stabilire se un trattamento sia svolto su larga scala:<br />
                    <ul>
                        <li>numero di soggetti interessati dal trattamento, in termini numerici o di percentuale rispetto alla popolazione di riferimento</li>
                        <li>volume dei dati e/o ambito delle diverse tipologie di dati oggetto di trattamento</li>
                        <li>durata, o persistenza, dell’attività di trattamento</li>
                        <li>ambito geografico dell’attività di trattamento</li>
                    </ul>
                    <i>Esempi:</i>
                    <ul>
                        <li>Mailing list per invio di informazioni promozionali</li>
                        <li>Raccolta di dati pubblici tratti dai social media per la creazione di profili</li>
                    </ul>
                "
            ],
            [
                'title'    => 'Combinazione o raffronto di insiemi di dati',
                'question' => "Combinazione di insiemi di dati derivanti da due o più trattamenti svolti per diverse finalità e/o da titolari distinti, secondo modalità che esulano dalle ragionevoli aspettative dell’interessato.<br />
                    <br />
                     <i>Esempi:</i>
                    <ul>
                        <li>Raccolta di dati pubblici tratti dai social media per la creazione di profili</li>
                        <li>Azienda che fornisce una applicazione per valutare i ristoranti che combina dati di geo localizzazione e preferenze di consumo per fornire pubblicità mirata in tempo reale in base alla posizione del soggetto</li>
                    </ul>
                "
            ],
            [
                'title'    => 'Dati relativi a interessati vulnerabili',
                'question' => "Il trattamento di questa tipologia di informazioni rappresenta un criterio ai fini della DPIA
                    in quanto è più accentuato lo squilibrio di poteri fra interessato e titolare del trattamento. Categorie di interessati vulnerabili sono generalmente definiti come “ogni interessato per il quale si possa identificare una situazione di disequilibrio nel rapporto con il rispettivo titolare del trattamento”<br >
                    <br />
                    <i>Esempio:</i>
                    A titolo esemplificativo le seguenti categorie di interessati sono da ritenersi vulnerabili:
                    <ul>
                        <li>minori</li>
                        <li>dipendenti</li>
                        <li>soggetti con patologie psichiatriche</li>
                        <li>richiedenti asilo</li>
                        <li>anziani</li>
                        <li>pazienti</li>
                    </ul>
                "
            ],
            [
                'title'    => 'Utilizzi innovativi o applicazione di nuove soluzioni tecnologiche',
                'question' => "Le conseguenze sul piano individuale e sociale del ricorso a una nuova tecnologia sono talora ignote. La DPIA aiuterà il titolare a comprendere e gestire tali rischi.<br />
                  <br />
                     <i>Esempi:</i>
                    <ul>
                        <li>L’associazione fra tecniche dattiloscopiche e riconoscimento del volto per migliorare il controllo degli accessi fisici</li>
                        <li>Alcune applicazioni legate all’Internet delle cose potrebbero avere impatti significativi sulla vita privata e le abitudini delle persone</li>
                    </ul>  
                "
            ],
            [
                'title'    => 'Esercizio di un diritto o conclusone di un contratto subordinato al trattamento:',
                'question' => "Ciò comprende i trattamenti finalizzati a consentire, modificare o negare l’accesso degli interessati a un servizio o la stipulazione di un contratto.<br />
                <i>Esempio:</i>
                    <ul>
                        <li>Screening dei clienti di una banca attraverso i dati registrati in una centrale rischi al fine di stabilire se ammetterli o meno a un finanziamento</li>
                    </ul>  
                "
            ]
        ]);

    }

}
