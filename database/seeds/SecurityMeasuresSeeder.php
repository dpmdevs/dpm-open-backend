<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use App\Models\SecurityMeasureCategory;
use Illuminate\Database\Seeder;

class SecurityMeasuresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        * ORGANIZZATIVE
        */

        $cat = $this->createMinSecurityMeasureCategory('org', 'Politiche, regolamenti e manuali');

        $secMeasures = [
            ['name' => 'È stato definito un manuale per la gestione del protocollo informatico', 'security_level' => 1],
            ['name' => 'Gli aspetti relativi alla sicurezza ICT e alla protezione dei dati sono contemplati nel piano di progetto e nella gestione del progetto.', 'security_level' => 2],
            ['name' => 'Documento/regolamento sulle politiche di accesso alle informazioni, creazione utenze e relativi profili e permessi', 'security_level' => 2],
            ['name' => 'Formazione relativa al processo/applicativo in esame', 'security_level' => 1],
            ['name' => 'Formazione relativa alla normativa sulla protezione dei dati', 'security_level' => 1],
            ['name' => 'Sono state prese in considerazione le misure per ottemperare ai principi di Privacy by design', 'security_level' => 1],
            ['name' => 'Sono state prese in considerazione le misure per ottemperare ai principi di Privacy by default', 'security_level' => 1],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('org', 'Ruoli e responsabilità');

        $secMeasures = [
            ['name' => 'Sono definiti ruoli e responsabilità interne, in ambito sicurezza ICT e protezione dei dati', 'security_level' => 1],
            ['name' => 'Sono definiti ruoli e responsabilità con terze parti, in ambito sicurezza ICT e protezione dei dati', 'security_level' => 1],
            ['name' => 'I soggetti che trattano dati personali hanno sottoscritto un accordo di riservatezza.', 'security_level' => 1],
            ['name' => 'Sono previste misure incentivanti per i soggetti coinvolti', 'security_level' => 2],
            ['name' => 'Sono previsti specifici accordi e misure di garanzia per i dati che escano o vengano comunicati a entità stabilite fuori dallo spazio economico europeo', 'security_level' => 1],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('org', 'Protezione dei Dati');

        $secMeasures = [
            ['name' => 'Sono in vigore procedure per classificare le categorie di dati', 'security_level' => 1],
            ['name' => 'Sono in vigore procedure per garantire la minimizzazione dei dati', 'security_level' => 2],
            ['name' => 'Sono in vigore procedure gestire la conservazione dei dati.', 'security_level' => 1],
            ['name' => 'Sono in vigore procedure minimizzare la conservazione dei dati.', 'security_level' => 1],
            ['name' => 'Sono in vigore procedure per notificare gli incidenti di sicurezza e le violazioni dei dati personali', 'security_level' => 1],
            ['name' => 'Le procedure per la notificazione di gli incidenti di sicurezza e le violazioni dei dati personali fanno uso di strumenti automatizzati che riducono i tempi necessari ad individuare categorie di dati coinvolti, misure di sicurezza applicate.', 'security_level' => 2],


        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('org', 'Gestione utenze');

        $secMeasures = [
            ['name' => 'È in vigore una procedura formale per il rilascio delle credenziali e per la loro cancellazione', 'security_level' => 1],
            ['name' => 'La procedura per il rilascio delle credenziali è effettuata tramite mezzi automatizzati', 'security_level' => 2],
            ['name' => 'La gestione dei privilegi e soggetta al principio del privilegio minimo ed è sottoposta a controlli formali', 'security_level' => 2],
            ['name' => 'Le utenze rilasciate vengono controllate periodicamente tramite un procedimento formale al fine di garantirne la coerenza.', 'security_level' => 2],
            ['name' => 'I privilegi di amministrazione vengono rilasciati ai soli utenti che abbiano le competenze adeguate e la necessità operativa.', 'security_level' => 1],
            ['name' => 'È mantenuto un inventario delle utenze amministrative.', 'security_level' => 1],
            ['name' => 'Le utenze amministrative sono formalmente autorizzate.', 'security_level' => 1],

        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('org', 'Cifratura');

        $secMeasures = [
            ['name' => 'Nelle politiche di sicurezza ICT sono definite le politiche sull’uso della cifratura', 'security_level' => 2],
            ['name' => 'È implementato un sistema di gestione delle chiavi crittografiche', 'security_level' => 2],
            ['name' => 'Le chiavi private sono adeguatamente protette', 'security_level' => 1],
            ['name' => 'Formazione relativa alla cifratura', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        //---
        $cat = $this->createMinSecurityMeasureCategory('org', 'Copie di sicurezza');

        $secMeasures = [
            ['name' => 'È definito un piano formalmente approvato al fine di garantire la Continuità Operativa e il Disaster Recovery', 'security_level' => 1],
            ['name' => 'È stato redatto un manuale per la conservazione digitale', 'security_level' => 1],
            ['name' => 'Sono previste procedure di controllo al fine di garantire l’effettività delle procedure di ripristino', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        /*
         * TECNICHE
         */

        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Credenziali');

        $secMeasures = [
            ['name' => 'È garantita la qualità delle password tramite la validazione, (8 caratteri con minuscole, maiuscole, numeri e caratteri speciali)', 'security_level' => 1],
            ['name' => 'Le credenziali già utilizzate non possono essere riutilizzate a breve distanza di tempo', 'security_level' => 1],
            ['name' => 'Le credenziali soprattutto quelle delle utenze amministrative vengono sostituite con frequenza semestrale', 'security_level' => 1],
            ['name' => 'Blocco accesso a seguito di ripetuti tentativi di accesso falliti consecutivi', 'security_level' => 2],
            ['name' => 'Autenticazione a più fattori', 'security_level' => 2],
            ['name' => 'Sono utilizzati sistemi centralizzati di gestione delle password (Single Sign On)', 'security_level' => 3],
            ['name' => 'Integrazione con il Domain Controller', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Applicazione');

        $secMeasures = [
            ['name' => 'Validazione restrittiva degli input', 'security_level' => 1],
            ['name' => 'Gestione corretta dei messaggi di errore facendo si che questi non rivelino informazioni riservate sul sistema', 'security_level' => 1],
            ['name' => 'Presenza di banner nell’interfaccia grafica che indichino la presenza di dati confidenziali o sensibili', 'security_level' => 2],
            ['name' => 'Eventuali connessioni in entrata ed uscita effettuate tramite connessioni protette che fanno uso di protocolli intrinsecamente sicuri', 'security_level' => 1],
            ['name' => 'Il codice sorgente dell’applicazione è stato revisionato da terze parti indipendenti', 'security_level' => 3],
            ['name' => 'Timeout di sessione se l’utente non è attivo per un determinato periodo di tempo', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Protezione dei Dati');

        $secMeasures = [
            ['name' => 'Analisi per identificare se e quali dati personali siano trattati', 'security_level' => 1],
            ['name' => 'Analisi per identificare se e quali dati sensibili siano trattati', 'security_level' => 1],
            ['name' => 'I dati sono conservati cifrati', 'security_level' => 2],
            ['name' => 'I dati sono conservati separati', 'security_level' => 2],
            ['name' => 'I dati sono conservati anonimizzati', 'security_level' => 2],
            ['name' => 'Si fa uso delle tecniche di “differential privacy”', 'security_level' => 3],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Certificazioni e codici di condotta');

        $secMeasures = [
            ['name' => 'Certificazione ISO 9001', 'security_level' => 2],
            ['name' => 'Certificazione ISO 27001', 'security_level' => 2],
            ['name' => 'Certificazione ISO 27018', 'security_level' => 2],
            ['name' => 'Il servizio ha aderito al codice di condotta CISPE', 'security_level' => 3],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Gestione utenze');

        $secMeasures = [
            ['name' => 'Implementazione del principio del privilegio minimo', 'security_level' => 1],
            ['name' => 'Disattivazione account e password predefiniti del fornitore', 'security_level' => 1],
            ['name' => 'Tutte le utenze, in particolare quelle amministrative, sono nominative e riconducibili ad una sola persona', 'security_level' => 1],
            ['name' => 'Generazione di un’allerta alla aggiunta o soppressione di un’utenza amministrativa', 'security_level' => 1],
            ['name' => 'Generazione di un’allerta alla aumento dei privilegi di utenza amministrativa', 'security_level' => 2],
            ['name' => 'Vengono tracciati nei log tutti i tentativi di accesso falliti delle utenze amministrative', 'security_level' => 1],
            ['name' => 'Vengono tracciate nei log tutte le azioni delle utenze amministrative', 'security_level' => 1],
            ['name' => 'Non è consentito l’accesso diretto ai sistemi con le utenze amministrative, gli amministratori accedono con utenze normali ed eseguono i singoli comandi con utenze amministrative', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);

        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Cifratura');

        $secMeasures = [
            ['name' => 'Trasferimento dati usando SSL/TLS', 'security_level' => 1],
            ['name' => 'Chiavi di cifratura personali per ogni utente', 'security_level' => 2],
            ['name' => 'Cifratura del disco', 'security_level' => 2],
            ['name' => 'Cifratura della base dati', 'security_level' => 2],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);


        //---
        $cat = $this->createMinSecurityMeasureCategory('tec', 'Copie di sicurezza');

        $secMeasures = [
            ['name' => 'Viene effettuata una copia di sicurezza con cadenza settimanale delle informazioni strettamente necessarie per il completo ripristino del sistema', 'security_level' => 1],
            ['name' => 'Le procedure di backup riguardano l’intero sistema, (OS, Applicazione, Dati)', 'security_level' => 2],
            ['name' => 'I backup sono effettuati con più di un sistema per contrastare malfunzionamenti in fase di ripristino', 'security_level' => 1],
            ['name' => 'Sono effettuati con cadenza semestrale operazioni di ripristino al fine di verificarne il corretto funzionamento', 'security_level' => 2],
            ['name' => 'Le copie di sicurezza sono mantenute sicure tramite la loro cifratura', 'security_level' => 2],
            ['name' => 'Le copie di sicurezza sono mantenute sicure tramite misure di sicurezza fisiche', 'security_level' => 1],
            ['name' => 'Le copie di sicurezza sono isolate dal sistema', 'security_level' => 1],
            ['name' => 'Le copie di sicurezza sono conservate in dispositivi posti a distanza maggiore di 500km', 'security_level' => 2],
            ['name' => 'Sono in funzione apparati che assicurano la ridondanza', 'security_level' => 3],
        ];

        $cat->minSecurityMeasures()->createMany($secMeasures);
    }


    /**
     * @param $family
     * @param $name
     * @param $description
     *
     * @return SecurityMeasureCategory
     */
    private function createMinSecurityMeasureCategory($family, $name, $description = '')
    {
        return SecurityMeasureCategory::create([
            'name'        => $name,
            'description' => $description,
            'family'      => $family
        ]);
    }
}
