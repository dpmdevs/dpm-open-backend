<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpiaRiskSourcesTable extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     *
     */
    public function up()
    {
        Schema::connection($this->connection)->create('dpia_risk_sources', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->text('description')->nullable();

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('dpia_risk_to_risk_sources', function (Blueprint $table) {

            $table->integer('risk_id')->unsigned();
            $table->foreign('risk_id')->references('id')->on('dpia_risks')->onDelete('cascade');

            $table->integer('risk_source_id')->unsigned();
            $table->foreign('risk_source_id')->references('id')->on('dpia_risk_sources');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('dpia_risk_to_risk_sources');
        Schema::connection($this->connection)->dropIfExists('dpia_risk_sources');
    }
}
