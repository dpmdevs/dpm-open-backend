<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProcessingActivities extends \App\Database\DPMMigration
{
    /** Run the migrations.
     *
     * @throws Exception
     */
    public function up()
    {
        Schema::connection($this->connection)->create('processing_activities', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');
            $table->text('description');
            $table->text('notes')->nullable();

            $table->text('purpose')->nullable();
            $table->text('legal_references')->nullable();
            $table->smallInteger('risk_estimate')->unsigned()->nullable();
            $table->boolean('is_process_automated')->default(false);
            $table->boolean('has_profiling_activity')->default(false);
            $table->string('automated_process_description')->nullable();
            $table->string('identification_code', 255)->nullable();
            $table->text('retention_description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection($this->connection)->create('process_types', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
        });

        Schema::connection($this->connection)->create('pa_process_types', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->integer('process_type_id')->unsigned();
            $table->foreign('process_type_id')->references('id')->on('process_types');

            $table->unique(['pa_id', 'process_type_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_data_categories', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('data_category_id')->unsigned();
            $table->foreign('data_category_id')->references('id')->on('data_categories');

            $table->unique(['pa_id', 'data_category_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_data_types', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('data_type_id')->unsigned();
            $table->foreign('data_type_id')->references('id')->on('data_types');

            $table->unique(['pa_id', 'data_type_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_legal_basis', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->integer('legal_basis_id')->unsigned();
            $table->foreign('legal_basis_id')->references('id')->on('legal_basis');

            $table->unique(['pa_id', 'legal_basis_id']);

            $table->timestamps();
        });


        Schema::connection($this->connection)->create('pa_data_subjects', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('data_subjects_id')->unsigned();
            $table->foreign('data_subjects_id')->references('id')->on('data_subjects');

            $table->unique(['pa_id', 'data_subjects_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_internal_processors', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unique(['pa_id', 'user_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_external_processors', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('third_parties_id')->unsigned();
            $table->foreign('third_parties_id')->references('id')->on('third_parties');

            $table->unique(['pa_id', 'third_parties_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('pa_applications', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->integer('application_id')->unsigned();
            $table->foreign('application_id')->references('id')->on('applications');

            $table->unique(['pa_id', 'application_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('pa_applications');
        Schema::connection($this->connection)->drop('pa_data_types');
        Schema::connection($this->connection)->drop('pa_data_categories');
        Schema::connection($this->connection)->drop('pa_process_types');
        Schema::connection($this->connection)->drop('pa_data_subjects');
        Schema::connection($this->connection)->drop('pa_legal_basis');
        Schema::connection($this->connection)->drop('pa_internal_processors');
        Schema::connection($this->connection)->drop('pa_external_processors');
        Schema::connection($this->connection)->drop('process_types');
        Schema::connection($this->connection)->drop('processing_activities');
    }
}
