<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpiaProjectsTable extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('dpia_projects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->text('goal')->nullable();
            $table->string('status', 50); //[In progress|Closed|Done|..]
            $table->date('review_date')->nullable();
            $table->text('notes')->nullable();
            $table->boolean('proportionality_evaluation')->default(false);
            $table->index('status');
            $table->text('proportionality_reason');


            $table->integer('processing_activity_id')->unsigned();
            $table->foreign('processing_activity_id')->references('id')->on('processing_activities');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::connection($this->connection)->table('dpia_risks', function (Blueprint $table) {
            $table->integer('dpia_project_id')->after('description')->unsigned();
            $table->foreign('dpia_project_id', 'dpia_project_id_foreign')->references('id')->on('dpia_projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('dpia_risks', function (Blueprint $table) {
            $table->dropForeign('dpia_project_id_foreign');
        });
        Schema::connection($this->connection)->dropIfExists('dpia_projects');
    }
}
