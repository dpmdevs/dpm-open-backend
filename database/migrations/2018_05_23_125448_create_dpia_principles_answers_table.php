<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDpiaPrinciplesAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('dpia_principles_answers', function (Blueprint $table) {

            $table->integer('dpia_project_id')->unsigned();
            $table->foreign('dpia_project_id')->references('id')->on('dpia_projects');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions');

            $table->text('answer')->nullable();
            $table->text('description')->nullable();

            $table->unique(['dpia_project_id', 'question_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dpia_principles_answers');
    }
}
