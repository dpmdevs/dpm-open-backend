<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('company_name', 255)->nullable();
            $table->string('company_number', 255)->nullable();
            $table->string('firstname', 255)->nullable();
            $table->string('lastname', 255)->nullable();
            $table->string('title', 255)->nullable();
            $table->string('email', 255);
            $table->string('certified_mail', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('address', 255)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::connection($this->connection)->table('config', function (Blueprint $table) {
            $table->integer('dpo_id')->unsigned()->after('id')->nullable();
            $table->foreign('dpo_id')->references('id')->on('dpos')->onDelete('set null');
        });

        Schema::connection($this->connection)->create('pa_dpos', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');


            $table->integer('dpo_id')->unsigned();
            $table->foreign('dpo_id')->references('id')->on('dpos');

            $table->unique(['pa_id', 'dpo_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('config', function ($t) {
            $t->dropColumn('dpo_id');
        });

        Schema::dropIfExists('pa_dpos');

        Schema::dropIfExists('dpos');
    }
}
