<?php

use App\Database\DPMMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends DPMMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)
            ->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('password', 255)->nullable();
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('email', 255)->nullable();
            $table->string('user_number', 255);
            $table->string('fiscal_code', 255)->nullable();

            $table->string('sex', 1)->nullable();

            $table->date('dismissal_date')->nullable();

            $table->string('binding_field', 255)->nullable();

            $table->binary('avatar')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('users');
    }
}
