<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCascadeReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pa_completion', function (Blueprint $table) {
            $table->dropForeign('pa_completion_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_process_types', function (Blueprint $table) {
            $table->dropForeign('pa_process_types_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_data_categories', function (Blueprint $table) {
            $table->dropForeign('pa_data_categories_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_data_types', function (Blueprint $table) {
            $table->dropForeign('pa_data_types_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_legal_basis', function (Blueprint $table) {
            $table->dropForeign('pa_legal_basis_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_data_subjects', function (Blueprint $table) {

            $table->dropForeign('pa_data_subjects_data_subjects_id_foreign');
            $table->foreign('data_subjects_id')->references('id')->on('data_subjects')->onDelete('cascade');

            $table->dropForeign('pa_data_subjects_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_internal_processors', function (Blueprint $table) {
            $table->dropForeign('pa_internal_processors_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_external_processors', function (Blueprint $table) {
            $table->dropForeign('pa_external_processors_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_applications', function (Blueprint $table) {
            $table->dropForeign('pa_applications_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_disclosure_subjects', function (Blueprint $table) {
            $table->dropForeign('pa_disclosure_subjects_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_retentions', function (Blueprint $table) {
            $table->dropForeign('pa_retentions_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_dpos', function (Blueprint $table) {
            $table->dropForeign('pa_dpos_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_data_source_types', function (Blueprint $table) {
            $table->dropForeign('pa_data_source_types_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_data_sources', function (Blueprint $table) {
            $table->dropForeign('pa_data_sources_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_controllers', function (Blueprint $table) {
            $table->dropForeign('pa_controllers_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_units', function (Blueprint $table) {
            $table->dropForeign('pa_units_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('pa_security_measures', function (Blueprint $table) {
            $table->dropForeign('pa_security_measures_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('app_security_measures', function (Blueprint $table) {
            $table->dropForeign('app_security_measures_application_id_foreign');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
        });

        Schema::table('pa_disclosure_subjects', function (Blueprint $table) {
            $table->dropForeign('pa_disclosure_subjects_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');
        });

        Schema::table('disclosure_subjects_answers', function (Blueprint $table) {
            $table->dropForeign('disclosure_subjects_answers_disc_subj_id_foreign');
            $table->foreign('disc_subj_id')->references('id')->on('pa_disclosure_subjects')->onDelete('cascade');
        });

        Schema::table('dpia_projects', function (Blueprint $table) {

            DB::statement('ALTER TABLE `dpia_projects` MODIFY `processing_activity_id` INTEGER UNSIGNED NULL;');

            $table->dropForeign('dpia_projects_processing_activity_id_foreign');
            $table->foreign('processing_activity_id', 'dpia_projects_processing_activity_id_foreign')
                ->references('id')->on('processing_activities')
                ->onDelete('SET NULL');
        });

        Schema::table('dpia_risks', function (Blueprint $table) {
            $table->dropForeign('dpia_project_id_foreign');
            $table->foreign('dpia_project_id', 'dpia_project_id_foreign')
                ->references('id')->on('dpia_projects')
                ->onDelete('cascade');
        });

        Schema::table('pa_applications', function (Blueprint $table) {
            $table->dropForeign('pa_applications_application_id_foreign');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
        });

        Schema::table('app_servers', function (Blueprint $table) {
            $table->dropForeign('app_servers_application_id_foreign');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');

            $table->dropForeign('app_servers_server_id_foreign');
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('cascade');
        });

        Schema::table('app_third_parties', function (Blueprint $table) {
            $table->dropForeign('app_third_parties_application_id_foreign');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');

            $table->dropForeign('app_third_parties_third_party_id_foreign');
            $table->foreign('third_party_id')->references('id')->on('third_parties')->onDelete('cascade');
        });

        Schema::table('dpia_risk_controls', function (Blueprint $table) {
            $table->dropForeign('dpia_risk_controls_control_id_foreign');
            $table->foreign('control_id')->references('id')->on('dpia_controls')->onDelete('cascade');
        });


        Schema::table('pa_retentions', function (Blueprint $table) {
            $table->dropForeign('pa_retentions_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->dropForeign('pa_retentions_retention_id_foreign');
            $table->foreign('retention_id')->references('id')->on('retentions')->onDelete('cascade');
        });


        Schema::table('pa_dpos', function (Blueprint $table) {
            $table->dropForeign('pa_dpos_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->dropForeign('pa_dpos_dpo_id_foreign');
            $table->foreign('dpo_id')->references('id')->on('dpos')->onDelete('cascade');
        });


        Schema::table('pa_controllers', function (Blueprint $table) {
            $table->dropForeign('pa_controllers_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->dropForeign('pa_controllers_controller_id_foreign');
            $table->foreign('controller_id')->references('id')->on('controllers')->onDelete('cascade');
        });

        Schema::table('pa_units', function (Blueprint $table) {
            $table->dropForeign('pa_units_unit_id_foreign');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
        });


        Schema::table('users_units', function (Blueprint $table) {
            $table->dropForeign('users_units_unit_id_foreign');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');

            $table->dropForeign('users_units_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('dpia_pre_ass_answers', function (Blueprint $table) {
            $table->dropForeign('dpia_pre_ass_answers_dpia_project_id_foreign');
            $table->foreign('dpia_project_id')->references('id')->on('dpia_projects')->onDelete('cascade');

            $table->dropForeign('dpia_pre_ass_answers_question_id_foreign');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::table('pa_internal_processors', function (Blueprint $table) {

            $table->dropForeign('pa_internal_processors_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->dropForeign('pa_internal_processors_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });


        Schema::table('pa_external_processors', function (Blueprint $table) {

            $table->dropForeign('pa_external_processors_pa_id_foreign');
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->dropForeign('pa_external_processors_third_parties_id_foreign');
            $table->foreign('third_parties_id')->references('id')->on('third_parties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
