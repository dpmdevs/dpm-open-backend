<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUnits extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     *
     */
    public function up()
    {

        Schema::connection($this->connection)->create('units', function (Blueprint $table) {

            $table->increments('id');

            $table->string('code', 255)->unique();
            $table->string('name', 255)->nullable();
            $table->text('description')->nullable();

            //  column is used to simplify immediate ancestor querying and, for example, to simplify building the whole tree
            $table->unsignedInteger('parent_id')->nullable();

            //  column is used widely by the package to make entities sortable
            $table->integer('position', false, true);

            // column is also used to simplify queries and reduce their number
            $table->integer('real_depth', false, true);

            $table->foreign('parent_id')->references('id')->on('units')->onDelete('set null');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection($this->connection)->create('units_closure', function (Blueprint $table) {
            // is a regular autoincremented column
            $table->increments('closure_id');

            //  points on a parent node
            $table->unsignedInteger('ancestor');

            //  points on a child node
            $table->unsignedInteger('descendant');

            //  shows a node depth in the tree
            $table->unsignedInteger('depth');

            $table->foreign('ancestor')->references('id')->on('units')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('descendant')->references('id')->on('units')->onDelete('cascade')->onUpdate('cascade');
            $table->engine = 'InnoDB';
        });

        Schema::connection($this->connection)->create('pa_units', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->integer('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('units');

            $table->unique(['pa_id', 'unit_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('users_units', function (Blueprint $table) {

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('units');

            $table->boolean('is_working_unit')->default(true);
            $table->boolean('is_delegate')->default(false);

            $table->unique(['user_id', 'unit_id']);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('users_units');
        Schema::connection($this->connection)->drop('pa_units');
        Schema::connection($this->connection)->drop('units_closure');
        Schema::connection($this->connection)->drop('units');
    }
}
