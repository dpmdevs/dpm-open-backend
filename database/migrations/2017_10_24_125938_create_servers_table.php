<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('servers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('brand', 255)->nullable();
            $table->string('supplier', 255)->nullable();
            $table->string('model', 255)->nullable();
            $table->string('description', 255);
            $table->ipAddress('ip')->nullable();
            $table->string('hostname', 255)->nullable();
            $table->text('notes')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::connection($this->connection)->create('app_servers', function (Blueprint $table) {

            $table->integer('application_id')->unsigned();
            $table->foreign('application_id')->references('id')->on('applications');

            $table->integer('server_id')->unsigned();
            $table->foreign('server_id')->references('id')->on('servers');

            $table->unique(['application_id', 'server_id']);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('app_servers');
        Schema::connection($this->connection)->dropIfExists('servers');
    }
}
