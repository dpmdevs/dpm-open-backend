<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControllersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('controllers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('company_name', 255);
            $table->string('company_number', 255)->nullable();
            $table->string('fiscal_code', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('city')->nullable();
            $table->string('zip_code', 20)->nullable();
            $table->string('holder_firstname', 255)->nullable();
            $table->string('holder_lastname', 255)->nullable();
            $table->string('holder_title', 255)->nullable();
            $table->string('holder_qualification', 255)->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('web')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection($this->connection)->table('config', function (Blueprint $table) {
            $table->integer('controller_id')->unsigned()->after('id')->nullable();
            $table->foreign('controller_id')->references('id')->on('controllers')->onDelete('set null');
        });

        Schema::connection($this->connection)->create('pa_controllers', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->integer('controller_id')->unsigned();
            $table->foreign('controller_id')->references('id')->on('controllers');

            $table->unique(['pa_id', 'controller_id']);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('pa_controllers');

        Schema::connection($this->connection)->table('config', function ($t) {
            $t->dropColumn('controller_id');
        });

        Schema::connection($this->connection)->dropIfExists('controllers');
    }
}
