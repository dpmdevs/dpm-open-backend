<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDpiaRisksTable extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {

        Schema::connection($this->connection)->create('dpia_protection_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->text('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::connection($this->connection)->create('dpia_risks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->text('acceptance_motivation')->nullable();
            $table->text('protection_goals');

            $table->integer('protection_category_id')->unsigned();
            $table->foreign('protection_category_id', 'protection_category_id_foreign')
                ->references('id')->on('dpia_protection_categories');


            $table->smallInteger('identifiability')->unsigned()->default(0);// 1..4
            $table->smallInteger('prejudicial_effect')->unsigned()->default(0);// 1..4
            $table->smallInteger('vulnerability')->unsigned()->default(0);// 1..4
            $table->smallInteger('risk_sources_capabilities')->unsigned()->default(0);// 1..4

            $table->smallInteger('severity')->unsigned()->default(0);// 1..4
            $table->smallInteger('likelihood')->unsigned()->default(0);// 1..4
            $table->smallInteger('risk_level')->unsigned()->default(0);// 1..16

            $table->softDeletes();
            $table->timestamps();
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('dpia_risks');
        Schema::connection($this->connection)->dropIfExists('dpia_protection_categories');
    }
}
