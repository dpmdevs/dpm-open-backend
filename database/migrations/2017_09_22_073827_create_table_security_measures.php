<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSecurityMeasures extends \App\Database\DPMMigration
{
    /** Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('security_measures_categories', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');
            $table->string('description')->nullable();
            $table->string('family');

            $table->timestamps();
            $table->softDeletes();

            $table->index('family');
        });

        Schema::connection($this->connection)->create('security_measures', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name', 5000);
            $table->smallInteger('security_level')->unsigned()->default(0);
            $table->boolean('is_adopted')->default(false);

            $table->integer('security_measures_categories_id')->unsigned();


            $table->foreign('security_measures_categories_id', 'sec_meas_categories_min_sec_meas')
                ->references('id')
                ->on('security_measures_categories');

            $table->timestamps();
            $table->softDeletes();
        });


        Schema::connection($this->connection)->create('pa_security_measures', function (Blueprint $table) {

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities')->onDelete('cascade');

            $table->integer('security_measure_id')->unsigned();
            $table->foreign('security_measure_id')->references('id')->on('security_measures')->onDelete('cascade');

            $table->unique(['pa_id', 'security_measure_id']);

            $table->timestamps();
        });

        Schema::connection($this->connection)->create('app_security_measures', function (Blueprint $table) {

            $table->integer('application_id')->unsigned();
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');

            $table->integer('security_measure_id')->unsigned();
            $table->foreign('security_measure_id')->references('id')->on('security_measures')->onDelete('cascade');

            $table->unique(['application_id', 'security_measure_id']);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('app_security_measures');
        Schema::connection($this->connection)->drop('pa_security_measures');
        Schema::connection($this->connection)->drop('security_measures');
        Schema::connection($this->connection)->drop('security_measures_categories');
    }
}
