<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableThirdParties extends \App\Database\DPMMigration
{
    /** Run the migrations.
     *
     * @throws Exception
     */
    public function up()
    {
        Schema::connection($this->connection)->create('third_parties', function (Blueprint $table) {

            $table->increments('id');

            $table->string('company_name')->unique();
            $table->text('description')->nullable();
            $table->string('processor_name')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection($this->connection)->create('app_third_parties', function (Blueprint $table) {

            $table->integer('application_id')->unsigned();
            $table->foreign('application_id')->references('id')->on('applications');


            $table->integer('third_party_id')->unsigned();
            $table->foreign('third_party_id')->references('id')->on('third_parties');

            $table->unique(['application_id', 'third_party_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('app_third_parties');
        Schema::connection($this->connection)->drop('third_parties');
    }
}
