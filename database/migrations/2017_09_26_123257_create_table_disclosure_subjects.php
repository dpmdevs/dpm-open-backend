<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDisclosureSubjects extends \App\Database\DPMMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('pa_disclosure_subjects', function (Blueprint $table) {

            $table->increments('id');

            $table->boolean('extra_ue');
            $table->string('company_name', 5000);

            $table->integer('pa_id')->unsigned();
            $table->foreign('pa_id')->references('id')->on('processing_activities');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection($this->connection)->create('disclosure_subjects_answers', function (Blueprint $table) {

            $table->integer('disc_subj_id')->unsigned();
            $table->foreign('disc_subj_id')->references('id')->on('pa_disclosure_subjects');

            $table->integer('question_id')->unsigned();
            $table->foreign('question_id')->references('id')->on('questions');

            $table->text('answer');

            $table->unique(['disc_subj_id', 'question_id']);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->drop('disclosure_subjects_answers');
        Schema::connection($this->connection)->drop('pa_disclosure_subjects');
    }
}
