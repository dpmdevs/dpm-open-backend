<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App\Models\Unit;

class UnitRepository extends BaseTableRepository
{
    protected $properties = ['directors.qualifications'];

    /**
     * UnitRepository constructor.
     */
    public function __construct()
    {
        $this->makeTagHandlers('directors');
    }

    public function getTree($withTrashed = true)
    {
        $columns = ['id', 'code', 'name', 'position', 'description', 'deleted_at'];

        $query = $this->initQuery()->with($this->properties);

        if ($withTrashed)
            $query->withTrashed();

        $totalRecords = $query->count();
        $result = $query->orderBy('position', 'asc')->get()->toTree($columns)->toArray();

        return new TableResult($result, $totalRecords);
    }


    public function getDirectorUnitTree(int $directorId)
    {
        $columns = ['id', 'code', 'name', 'position', 'description', 'deleted_at'];

        $query = $this
            ->initQuery()
            ->with($this->properties)
            ->withDirector($directorId);

        $totalRecords = $query->count();

        $result = $query->orderBy('position', 'asc')->get()->toTree($columns)->toArray();

        return new TableResult($result, $totalRecords);
    }

    /**
     * @return Unit
     */
    protected function getDataModel()
    {
        return new Unit();
    }

    /**
     * @param int $id
     * @return void
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $this->findUnit($id)
            ->deleteSubtree(true);
    }

    /**
     * @param int $id
     *
     * @return void
     */
    public function restore(int $id)
    {
        $this->findTrashedUnit($id)->restoreSubtree(true);
    }

    public function detachDirectors(int $id)
    {
        $this->findUnit($id)->directors()->detach();
    }

    public function syncDirectors(int $unitId, array $usersId)
    {
        return $this
            ->find($unitId)
            ->first()
            ->directors()
            ->sync($usersId);
    }

    /**
     * @param int $id
     * @return Unit
     */
    public function findUnit(int $id)
    {
        return $this->find($id)->first();
    }

    /**
     * @param int $unitId
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getUnitRelatedProcessingActivities(int $unitId)
    {
        return $this->findUnit($unitId)->processingActivities();
    }

    /**
     * @param int $id
     * @return Unit
     */
    private function findTrashedUnit(int $id)
    {
        return $this->findBy('id', $id)->onlyTrashed()->first();
    }
}
