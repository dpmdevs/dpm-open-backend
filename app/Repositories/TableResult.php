<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


class TableResult
{
    public $data = [];
    public $totalRecords = 0;
    public $query;

    /**
     * TableResult constructor.
     *
     * @param array $data
     * @param int $totalRecords
     */
    public function __construct(array $data, int $totalRecords)
    {
        $this->data = $data;
        $this->totalRecords = $totalRecords;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param int $totalRecords
     */
    public function setTotalRecords(int $totalRecords)
    {
        $this->totalRecords = $totalRecords;
    }

    /**
     * @return array
     */
    public function pack()
    {
        return [
            'data'     => $this->data,
            'metadata' => [
                'totalRecords' => $this->totalRecords
            ]
        ];
    }
}