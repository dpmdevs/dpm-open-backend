<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseTableRepository extends BaseRepository implements TableRepositoryInterface
{
    /**
     * @var array
     * Related eager loaded Models
     */
    protected $properties = [];

    /**
     * @return Model
     */
    protected abstract function getDataModel();

    /**
     * @param array $entityData
     *
     * @return mixed
     */
    public function store(array $entityData)
    {
        return $this->getDataModel()->create($entityData);
    }

    /**
     * @param int $id
     * @param array $entityData
     *
     * @return boolean
     */
    public function update(int $id, array $entityData)
    {
        return $this->find($id)->first()->update($entityData);
    }

    /**
     * @param int $id
     *
     * @return boolean | null
     */
    public function delete(int $id)
    {
        return $this->find($id)->first()->delete();
    }

    /**
     * @param int $id
     *
     * @return boolean | null
     */
    public function forceDelete(int $id)
    {
        return $this->find($id)->first()->forceDelete();
    }


    /**
     * @param int $id
     *
     * @return boolean | null
     */
    public function restore(int $id)
    {
        return $this->getDataModel()->onlyTrashed()->where('id', '=', $id)->restore();
    }

    /**
     * @param string $sortField
     * @param string $sortDirection
     * @param int $start
     * @param int $limit
     * @param bool $trashed
     * @param string $searchParams
     * @param string $searchOrParams
     *
     * @return TableResult
     */
    public function query(string $sortField, string $sortDirection, int $start, int $limit, bool $trashed,
                          string $searchParams, string $searchOrParams): TableResult
    {
        $query = $this->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);

        return new TableResult($query->get()->toArray(), $query->totalCount);
    }

    /**
     * @param Builder $query
     * @param string $searchParams
     *
     * @return Builder
     */
    public function addSearchParams(Builder $query, string $searchParams): Builder
    {
        if ($searchParams === '') return $query;

        foreach ($this->decodeParams($searchParams) as $param) {

            $keyword = $this->extractKeywordFromParam($param);

            if (count($keyword) === 0 || $keyword[0] === '') continue;

            if ($this->isAggregateFilter($param)) {
                $query = $this->addAggregateFilter($query, $param->columnName, $keyword[0]);
            } else {
                $query->where($param->columnName, 'LIKE', $keyword[0]);
            }
        }

        return $query;
    }

    /**
     * @param Builder $query
     * @param string $searchOrParams
     *
     * @return Builder
     */
    public function addSearchOrParams(Builder $query, string $searchOrParams): Builder
    {
        if ($searchOrParams === '') return $query;

        $query->where(function ($subQuery) use ($searchOrParams) {
            foreach ($this->decodeParams($searchOrParams) as $param) {

                $columnName = $param->columnName;

                foreach ($this->extractKeywordFromParam($param) as $keyword) {

                    if ($keyword === '') continue;

                    if ($this->isAggregateFilter($param)) {
                        $subQuery = $this->addOrAggregateFilter($subQuery, $columnName, $keyword);
                    } else {
                        $subQuery->orWhere($param->columnName, 'LIKE', $keyword);
                    }
                }
            }

            return $subQuery;
        });

        return $query;
    }


    /**
     * @param $field
     * @param $value
     *
     * @return Builder
     */
    public function findBy($field, $value)
    {
        return $this->getDataModel()->withTrashed()->where($field, '=', $value);
    }

    /**
     * @param int $id
     * @return Builder
     *
     * @throws ModelNotFoundException
     */
    public function find(int $id)
    {
        $result = $this->findBy('id', $id);

        if (!is_null($result) && count($result->get()->toArray()) > 0) {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class($this->getDataModel()), $id
        );
    }

    /**
     * @return Builder
     */
    protected function initQuery(): Builder
    {
        return $this->getDataModel()->query()->with($this->properties);
    }

    /**
     * @param Builder $query
     * @param int $start
     * @param int $limit
     * @return Builder
     */
    protected function addStartLimit(Builder $query, int $start, int $limit): Builder
    {
        if ($limit > 0)
            $query->skip($start)
                ->take($limit);

        return $query;
    }

    /**
     * @param Builder $query
     * @param string $sortField
     * @param string $sortDirection
     * @return Builder
     */
    protected function addSortAndOrder(Builder $query, string $sortField, string $sortDirection)
    {
        return $query->orderBy($sortField, $sortDirection);
    }

    /**
     * @param string $searchParams
     * @return array
     */
    public function decodeParams(string $searchParams)
    {
        return ($searchParams === '') ? [] : json_decode(html_entity_decode($searchParams));
    }

    /**
     * @param $param
     * @return array
     */
    private function extractKeywordFromParam($param): array
    {
        $keywords = [];

        if (isset($param->keyword)) {

            if ($param->keyword === '') return $keywords;

            $trimmed = explode(' ', trim($param->keyword));

            foreach ($trimmed as $t)
                $keywords[] = is_numeric($t) ? $t : "%$t%";
        }

        return $keywords;
    }

    /**
     * @param $param
     * @return string
     */
    private function isAggregateFilter($param): string
    {
        return strstr($param->columnName, '.');
    }

    /**
     * @param Builder $query
     * @param string $aggregateColumnName
     * @param string $keyword
     *
     * @return Builder
     */
    protected function addAggregateFilter(Builder $query, $aggregateColumnName, $keyword)
    {
        list($table, $columnName) = explode('.', $aggregateColumnName);

        $query->whereHas($table, function ($query) use ($columnName, $keyword) {
            $query->where($columnName, 'LIKE', "%$keyword%");
        });

        return $query;
    }

    /**
     * @param Builder $query
     * @param string $aggregateColumnName
     * @param string $keyword
     *
     * @return Builder
     */
    private function addOrAggregateFilter(Builder $query, $aggregateColumnName, $keyword)
    {
        list($table, $columnName) = explode('.', $aggregateColumnName);

        $query->orwhereHas($table, function ($query) use ($columnName, $keyword) {
            $query->where($columnName, 'LIKE', "%$keyword%");
        });

        return $query;
    }

    /**
     * @param int $id
     * @param string $attributeName
     * @param $attributeValue
     * @return bool
     */
    public function updateAttribute(int $id, string $attributeName, $attributeValue)
    {
        return $this->find($id)->firstOrFail()->update([$attributeName => $attributeValue]);
    }

    /**
     * @return Builder
     */
    public function getServerOSs()
    {
        return $this->getDataModel()->isServer()->orderBy('name', 'asc');
    }

    /**
     * @return Builder
     */
    public function getClientOSs()
    {
        return $this->getDataModel()->isClient()->orderBy('name', 'asc');
    }

    /**
     * @param string $relation
     */
    protected function makeTagHandlers(string $relation)
    {
        $this->makeGetAssociatedTagsHandler($relation);
        $this->makeGetAvailableTagsHandler($relation);
        $this->makeToggleTagHandler($relation);
    }

    /**
     * @param string $relation
     * @param int $entityId
     * @param array $relationIds
     * @return mixed
     */
    public function toggleTag(string $relation, int $entityId, array $relationIds)
    {
        $toggleFunctionName = "toggle" . ucfirst($relation);

        return $this->$toggleFunctionName($entityId, $relationIds);
    }

    /**
     * @param string $relation
     * @param int $entityId
     * @return mixed
     */
    public function getAssociatedTags(string $relation, int $entityId)
    {
        $getAssociatedTagFunctionName = "get" . ucfirst($relation);

        return $this->$getAssociatedTagFunctionName($entityId);
    }

    /**
     * @param string $relation
     * @param int $entityId
     * @param string $searchCriteria
     * @param string $searchOrCriteria
     * @param Model $relationClass
     * @param string $entityPivotKeyName
     * @param string $relationKeyName
     * @param string $relationPivotTableName
     * @param string $sortColumn
     * @param string $sortDir
     * @return mixed
     */
    public function getAvailableTag(string $relation,
                                    int $entityId,
                                    string $searchCriteria,
                                    string $searchOrCriteria,
                                    Model $relationClass,
                                    string $entityPivotKeyName,
                                    string $relationKeyName,
                                    string $relationPivotTableName,
                                    string $sortColumn = 'id',
                                    string $sortDir = 'desc')
    {
        $getAvailableTagFunctionName = "getAvailable" . ucfirst($relation);

        return $this->$getAvailableTagFunctionName(
            $entityId,
            $searchCriteria,
            $searchOrCriteria,
            $relationClass,
            $entityPivotKeyName,
            $relationKeyName,
            $relationPivotTableName,
            $sortColumn,
            $sortDir
        );
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        return call_user_func($this->{$name}, $arguments);
    }

    /**
     * @param string $relation
     */
    private function makeGetAssociatedTagsHandler(string $relation)
    {
        $getAssociated = function (array $arguments) use ($relation) {

            $entityId = $arguments[0];

            return $this
                ->find($entityId)
                ->first()
                ->$relation()
                ->get();
        };
        $this->addMethod("get" . ucfirst($relation), $getAssociated);
    }

    /**
     * @param string $relation
     */
    private function makeGetAvailableTagsHandler(string $relation)
    {
        $getAvailable = function (array $arguments) {

            $entityId = $arguments[0];
            $searchCriteria = $arguments[1];
            $searchOrCriteria = $arguments[2];
            $relationClass = $arguments[3];
            $entityPivotKeyName = $arguments[4];
            $relationKeyName = $arguments[5];
            $relationPivotTableName = $arguments[6];
            $sortColumn = $arguments[7] !== '' ? $arguments[7] : 'id';
            $sortDir = $arguments[8] !== '' ? $arguments[8] : 'desc';

            $query = $this
                ->addSearchParams($relationClass::select('*'), $searchCriteria);

            $query = $this
                ->addSearchOrParams($query, $searchOrCriteria)
                ->whereNotIn('id', function ($query)
                use ($entityId, $entityPivotKeyName, $relationKeyName, $relationPivotTableName) {
                    return $query->select($relationKeyName)
                        ->from($relationPivotTableName)
                        ->where($entityPivotKeyName, '=', $entityId);
                })
            ->orderBy($sortColumn, $sortDir);

            return $query->get();
        };

        $this->addMethod("getAvailable" . ucfirst($relation), $getAvailable);
    }

    /**
     * @param string $relation
     */
    private function makeToggleTagHandler(string $relation)
    {
        $toggle = function (array $arguments) use ($relation) {

            $entityId = $arguments[0];
            $relationIds = $arguments[1];

            return $this
                ->find($entityId)
                ->first()
                ->$relation()
                ->toggle($relationIds);
        };
        $this->addMethod("toggle" . ucfirst($relation), $toggle);
    }

    private function addMethod(string $name, \Closure $method)
    {
        $this->{$name} = $method;
    }

    /**
     * @param string $sortField
     * @param string $sortDirection
     * @param int $start
     * @param int $limit
     * @param bool $trashed
     * @param string $searchParams
     * @param string $searchOrParams
     *
     * @return Builder
     */
    public function makeQuery(
        string $sortField,
        string $sortDirection,
        int $start,
        int $limit,
        bool $trashed,
        string $searchParams,
        string $searchOrParams): Builder
    {
        $query = $this->initQuery();

        if ($trashed) $query->onlyTrashed();

        $query = $this->addSearchParams($query, $searchParams);

        $query = $this->addSearchOrParams($query, $searchOrParams);

        $query->totalCount = $query->count();

        $query = $this->addSortAndOrder($query, $sortField, $sortDirection);

        $query = $this->addStartLimit($query, $start, $limit);

        return $query;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->getDataModel()->count();
    }
}
