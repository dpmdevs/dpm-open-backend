<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use App\Models\Application;
use App\Models\SecurityMeasure;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ApplicationRepository extends BaseTableRepository
{
    /**
     * @var array
     * Related eager loaded Models
     */
    protected $properties = ['thirdParties'];

    /**
     * ApplicationRepository constructor.
     */
    public function __construct()
    {
        $this->makeTagHandlers('thirdParties');
        $this->makeTagHandlers('servers');
    }

    /**
     * @param Application $application
     */
    public function setAdoptedSecurityMeasures(Application $application)
    {
        $adoptedSecurityMeasures = SecurityMeasure::isAdopted()
            ->withCategoryFamily('tec')
            ->get()
            ->pluck('id')
            ->toArray();

        $application->securityMeasures()->toggle($adoptedSecurityMeasures);
    }

    public function setAdoptedSecurityMeasuresToAll()
    {
        foreach ($this->getDataModel()->all() as $app)
            $this->setAdoptedSecurityMeasures($app);
    }

    /**
     * @return Application
     */
    protected function getDataModel()
    {
        return new Application();
    }

    /**
     * @param int $id
     * @return BelongsToMany
     */
    public function getSecurityMeasures(int $id)
    {
        return $this->findApplicationById($id)->securityMeasures();
    }

    /**
     * @param int $applicationId
     * @param array $securityMeasures
     */
    public function toggleSecurityMeasures(int $applicationId, array $securityMeasures)
    {
        $this->findApplicationById($applicationId)
            ->securityMeasures()
            ->toggle($securityMeasures);
    }

    /**
     * @param int $id
     * @return Application
     */
    private function findApplicationById(int $id)
    {
        return $this
            ->find($id)
            ->first();
    }
}
