<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App\Models\DataCategory;
use App\Models\DataType;
use Illuminate\Database\Eloquent\Builder;

class DataCategoriesRepository extends BaseTableRepository
{
    private $family = '';

    /**
     * @return DataCategory
     */
    protected function getDataModel()
    {
        return new DataCategory();
    }

    /**
     * @param $dataCategoryId
     * @return mixed
     */
    public function getDataTypes($dataCategoryId)
    {
        return $this->getDataModel()
            ->find($dataCategoryId)
            ->dataTypes()
            ->withTrashed()
            ->get(['id', 'name', 'description', 'deleted_at']);
    }

    /**
     * @param string $family
     */
    public function setFamily(string $family)
    {
        $this->family = $family;
    }

    /**
     * @param int $categoryId
     * @param array $dataTypeData
     */
    public function createDataType(int $categoryId, array $dataTypeData)
    {
        $dataType = new DataType;
        $dataType->fill($dataTypeData);

        $this->getDataModel()
            ->find($categoryId)
            ->dataTypes()
            ->save($dataType);
    }

    /**
     * @param int $dataTypeId
     * @param array $dataTypeData
     */
    public function updateDataType(int $dataTypeId, array $dataTypeData)
    {
        DataType::findOrFail($dataTypeId)->update($dataTypeData);
    }

    /**
     * @param int $dataTypeId
     */
    public function deleteDataType(int $dataTypeId)
    {
        DataType::withTrashed()->findOrFail($dataTypeId)->delete();
    }

    public function restoreDataType(int $dataTypeId)
    {
        DataType::withTrashed()->findOrFail($dataTypeId)->restore();
    }

    /**
     * @return Builder
     */
    protected function initQuery(): Builder
    {
        return $this->getDataModel()
            ->where('family', '=', $this->family)
            ->with('dataTypes')
            ->select('*');
    }
}