<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use App;
use App\Models\Assignments\AssignmentsApplications;
use App\Models\Assignments\AssignmentsDevices;
use App\Models\Assignments\AssignmentsPa;
use App\Models\ProcessingActivity;

class AssignmentsRepository extends BaseRepository
{
    /**
     * @var AssignmentsPa
     */
    protected $assignmentsPa;

    /**
     * @var AssignmentsApplications
     */
    protected $assignmentsApplications;

    /**
     * @var AssignmentsDevices
     */
    private $assignmentsDevices;

    public function __construct()
    {
        $this->assignmentsPa = new AssignmentsPa();
        $this->assignmentsApplications = new AssignmentsApplications();
        $this->assignmentsDevices = new AssignmentsDevices();
    }

    /**
     * @param int $userId
     * @param int $processingActivityId
     * @param array $unitIds
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function assignProcessingActivity(int $userId, int $processingActivityId, array $unitIds = [])
    {
        $a = $this->assignmentsPa->firstOrCreate([
            'user_id' => $userId,
            'pa_id'   => $processingActivityId
        ]);

        $a->units()->attach($unitIds);

        return $a;
    }

    /**
     * @param int $userId
     * @param int $applicationId
     * @param array $unitIds
     */
    public function assignApplication(int $userId, int $applicationId, array $unitIds = [])
    {
        $a = $this->assignmentsApplications->firstOrCreate([
            'user_id'        => $userId,
            'application_id' => $applicationId
        ]);

        $a->units()->attach($unitIds);
    }

    /**
     * @param int $userId
     * @param int $deviceId
     * @param array $unitIds
     */
    public function assignDevices(int $userId, int $deviceId, array $unitIds = [])
    {
        $a = $this->assignmentsDevices->firstOrCreate([
            'user_id'   => $userId,
            'device_id' => $deviceId
        ]);

        $a->units()->attach($unitIds);
    }

    /**
     * @param int $userId
     * @param array $unitIds
     * @return \Illuminate\Database\Eloquent\Collection[]
     */
    public function getProcessingActivities(int $userId, array $unitIds = [])
    {
        return $this->assignmentsPa
            ->findAssignment($userId)
            ->filterUnit($unitIds)
            ->get();
    }

    /**
     * @param int $userId
     * @param array $unitIds
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getApplications(int $userId, array $unitIds = [])
    {
        return $this->assignmentsApplications
            ->findAssignment($userId)
            ->filterUnit($unitIds)
            ->get();
    }

    /**
     * @param int $userId
     * @param array $unitIds
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDevices(int $userId, array $unitIds = [])
    {
        return  $query = $this->assignmentsDevices
            ->findAssignment($userId)
            ->filterUnit($unitIds)
            ->get();
    }

    /**
     * @param int $assignmentId
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteProcessingActivity(int $assignmentId)
    {
        return $this->assignmentsPa->find($assignmentId)->delete();
    }

    /**
     * @param int $assignmentId
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteApplication(int $assignmentId)
    {
        return $this->assignmentsApplications->find($assignmentId)->delete();
    }

    /**
     * @param int $assignmentId
     * @return bool|null
     * @throws \Exception
     */
    public function deleteDevice(int $assignmentId)
    {
        return $this->assignmentsDevices->find($assignmentId)->delete();
    }

    public function getAvailablePa(int $userId, array $directedUnitIds)
    {
        $model = new ProcessingActivity();

        $currentAssignedPaIds = $this->getProcessingActivities($userId)
            ->pluck('pa_id')
            ->toArray();

        $allPas = $model->whereNotIn('id', $currentAssignedPaIds);

        if (count($directedUnitIds) > 0)
            $allPas->withUnits($directedUnitIds);

        $allPas->with(['dataCategories.dataTypes', 'processTypes', 'units']);

        return $allPas->get()->toArray();
    }

    public function getAvailableApplications(int $userId)
    {
        $model = new App\Models\Application();

        $currentAssignedApplicationIds = $this->getApplications($userId)
            ->pluck('application_id')
            ->toArray();

        return $model
            ->whereNotIn('id', $currentAssignedApplicationIds)
            ->get()
            ->toArray();
    }

    public function getAvailableDevices(int $userId)
    {
        $model = App\Models\Device::with('deviceType');

        $currentAssignedIds = $this->getDevices($userId)
            ->pluck('device_id')
            ->toArray();

        return $model->whereNotIn('id', $currentAssignedIds)
            ->get()
            ->toArray();
    }

    /**
     * @param int $assignmentId
     * @param array $unitIds
     *
     * @return array
     */
    public function syncProcessingActivityUnits(int $assignmentId, array $unitIds)
    {
        return $this->assignmentsPa
            ->find($assignmentId)
            ->units()
            ->sync($unitIds);
    }

    /**
     * @param int $assignmentId
     * @param array $unitIds
     *
     * @return array
     */
    public function syncApplicationUnits(int $assignmentId, array $unitIds)
    {
        return $this->assignmentsApplications
            ->find($assignmentId)
            ->units()
            ->sync($unitIds);
    }

    /**
     * @param int $assignmentId
     * @param array $unitIds
     *
     * @return array
     */
    public function syncDeviceUnits(int $assignmentId, array $unitIds)
    {
        return $this->assignmentsDevices
            ->find($assignmentId)
            ->units()
            ->sync($unitIds);
    }

}