<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use App\Models\ProcessingActivity;
use App\Models\SecurityMeasure;
use App\Models\SecurityMeasureCategory;
use Illuminate\Database\Eloquent\Builder;

class SecurityMeasuresRepository extends BaseTableRepository
{
    private $family = '';

    /**
     * @return SecurityMeasureCategory
     */
    protected function getDataModel()
    {
        return new SecurityMeasureCategory();
    }

    /**
     * @param string $family
     */
    public function setFamily(string $family)
    {
        $this->family = $family;
    }

    /**
     * @return Builder
     */
    protected function initQuery(): Builder
    {
        return $this->getDataModel()
            ->where('family', '=', $this->family)
            ->select('*');
    }

    public function storeCategory(string $family, array $data)
    {
        $this->getDataModel()->create(array_merge(
                $data,
                ['family' => $family]
            )
        );
    }

    public function getSecurityMeasure($categoryId)
    {
        return $this->getDataModel()
            ->find($categoryId)
            ->minSecurityMeasures()
            ->withTrashed()
            ->get();
    }

    /**
     * @param $categoryId
     * @param array $securityMeasureData
     */
    public function createSecurityMeasure(int $categoryId, array $securityMeasureData)
    {
        $minSecurityMeasure = new SecurityMeasure();
        $minSecurityMeasure->fill($securityMeasureData);

        $this->getDataModel()
            ->find($categoryId)
            ->minSecurityMeasures()
            ->save($minSecurityMeasure);
    }


    /**
     * @param int $securityMeasureId
     * @param array $securityMeasureData
     */
    public function updateSecurityMeasure(int $securityMeasureId, array $securityMeasureData)
    {
        SecurityMeasure::findOrFail($securityMeasureId)->update($securityMeasureData);
    }

    /**
     * @param int $securityMeasureId
     * @throws \Exception
     */
    public function deleteSecurityMeasure(int $securityMeasureId)
    {
        SecurityMeasure::withTrashed()->findOrFail($securityMeasureId)->delete();
    }

    /**
     * @param int $securityMeasureId
     */
    public function restoreSecurityMeasure(int $securityMeasureId)
    {
        SecurityMeasure::withTrashed()->findOrFail($securityMeasureId)->restore();
    }

}
