<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use App\Models\DPIAProject;
use Illuminate\Database\Eloquent\Builder;

class DPIARepository extends BaseTableRepository
{
    protected $properties = ['reporter', 'processingActivity', 'preAssessmentAnswers', 'principlesAnswers'];

    /**
     * @param $id
     * @param array $answers
     */
    public function storePreAssessment($id, array $answers)
    {
        $toSync = [];
        foreach ($answers as $answer) {
            $toSync[$answer['question_id']] = ['answer' => $answer['answer']];
        }

        $this->findDPIAById($id)->preAssessmentAnswers()->sync($toSync);
    }

    /**
     * @param $id
     * @param array $answers
     */
    public function storePrinciples($id, array $answers)
    {
        $toSync = [];
        foreach ($answers as $answer) {
            $toSync[$answer['question_id']] = [
                'answer'      => $answer['answer'],
                'description' => $answer['description']
            ];
        }

        $this->findDPIAById($id)->principlesAnswers()->sync($toSync);
    }


    /**
     * @return \Eloquent
     */
    protected function getDataModel()
    {
        return new DPIAProject();
    }

    /**
     * @param int $id
     * @return DPIAProject
     */
    public function findDPIAById(int $id)
    {
        return $this->find($id)->with($this->properties)->first();
    }

    /**
     * @param int $projectId
     * @param int $protectionCategoryId
     * @return Builder
     */
    public function getRisksByProtectionCategory(int $projectId, int $protectionCategoryId)
    {
        return $this->findDPIAById($projectId)->risks()->where('protection_category_id', '=', $protectionCategoryId);
    }
}
