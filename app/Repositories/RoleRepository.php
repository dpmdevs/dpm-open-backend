<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App\Permission;
use App\Role;
use DB;

class RoleRepository extends BaseTableRepository
{
    /**
     * @param int $limit
     * @return array
     */
    public function getUsersPerRole(int $limit)
    {
        $usersPerRole = DB::table('role_user')
            ->select(DB::raw('count(user_id) as users, role_id, roles.display_name as role'))
            ->join('roles', 'role_id', '=', 'id')
            ->groupBy(['role_id', 'display_name'])
            ->orderBy('users', 'desc')
            ->take($limit)
            ->get();

        return $usersPerRole;
    }


    /**
     * @return Role
     */
    protected function getDataModel()
    {
        return new Role();
    }

    public function usersCount()
    {
        return $this->getDataModel()
            ->users()
            ->selectRaw('role_user.role_id, count(*) as aggregate')
            ->groupBy('role_user.role_id');
    }

    /**
     * @param Role $role
     * @param Permission $permission
     * @return array
     */
    public function togglePermission(Role $role, Permission $permission)
    {
        return $role->perms()->toggle($permission->getKey());
    }

}