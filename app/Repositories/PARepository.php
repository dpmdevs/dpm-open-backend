<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App;
use App\Models\DisclosureSubjects;
use App\Models\PACompletion;
use App\Models\ProcessingActivity;
use App\Models\SecurityMeasure;
use Illuminate\Database\Eloquent\Builder;

class PARepository extends BaseTableRepository
{
    const PA_STATE_STEPS = 5;

    protected $properties = [
        'processTypes',
        'dataCategories.dataTypes',
        'dataTypes:id',
        'dataSubjects',
        'dataSources',
        'dataSourceTypes',
        'legalBasis',
        'controllers',
        'internalProcessors',
        'externalProcessors',
        'dpos',
        'completionState',
        'applications',
        'retentions',
        'securityMeasures',
        'units'
    ];

    public function __construct()
    {
        $this->properties['disclosureSubjects.answers'] = function ($query) {
            $query
                ->select('answer', 'question', 'question_id', 'questionnaires.code', 'questionnaires.name')
                ->join('questionnaires', 'questionnaire_id', '=', 'questionnaires.id');
        };
    }


    /**
     * @return \Eloquent
     */
    protected function getDataModel()
    {
        return new ProcessingActivity();
    }

    /**
     * @param string $sortField
     * @param string $sortDirection
     * @param int $start
     * @param int $limit
     * @param bool $trashed
     * @param string $searchParams
     * @param string $searchOrParams
     *
     * @return TableResult
     */
    public function query(string $sortField, string $sortDirection, int $start, int $limit, bool $trashed,
                          string $searchParams, string $searchOrParams): TableResult
    {
        $query = $this->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);

        $user = \Auth::user();

        if ($user->isUnitDirector()) {

            if (App\Models\Config::find(1)->isInternalProcessorActive()) {
                $query->withInternalProcessor($user->getKey());
            } else {
                $directedUnits = $user->directedUnits()->get()->pluck('id')->toArray();
                $query->withUnits($directedUnits);
                $query->totalCount = $query->count();
            }
        }

        return new TableResult($query->get()->toArray(), $query->totalCount);
    }

    /**
     * @param array $entityData
     * @return ProcessingActivity
     */
    public function store(array $entityData)
    {
        $pa = parent::store($entityData);

        $pa->update(['identification_code' => $pa->getKey()]);
        $this->setProcessTypes($pa, $entityData['process_types']);
        $this->setDataCategories($pa, $entityData['data_categories']);
        $this->setDataTypes($pa, $entityData['data_types']);
        $this->setLegalBasis($pa, $entityData['legal_basis']);
        $this->initPACompletionState($pa->getAttribute('id'));
        $this->setDataSources($pa, $entityData['data_sources']);
        $this->setDataSourceTypes($pa, $entityData['data_source_types']);

        $user = \Auth::user();

        if ($user->isUnitDirector()) {
            $directedUnits = $user->directedUnits()->get()->pluck('id')->toArray();
            $pa->units()->attach($directedUnits);
        }

        if (!$entityData['is_active']) {
            $pa->delete();
        }

        return $pa;
    }

    /**
     * @param int $id
     * @param array $entityData
     *
     * @return bool
     * @throws \Exception
     */
    public function update(int $id, array $entityData)
    {
        $response = parent::update($id, $entityData);

        $pa = $this->findProcessingActivity($id);

        $this->setProcessTypes($pa, $entityData['process_types']);
        $this->setDataCategories($pa, $entityData['data_categories']);
        $this->setDataTypes($pa, $entityData['data_types']);
        $this->setLegalBasis($pa, $entityData['legal_basis']);
        $this->setDataSources($pa, $entityData['data_sources']);
        $this->setDataSourceTypes($pa, $entityData['data_source_types']);

        $this->setIsActive($pa, $entityData['is_active']);

        return $response;
    }

    /**
     * @param int $pa_id
     * @return ProcessingActivity
     */
    public function getPABasicData(int $pa_id)
    {
        return $this->find($pa_id)->first();
    }

    /**
     * @param int $pa_id
     * @return Builder
     */
    public function getPAFullData(int $pa_id): Builder
    {
        return $this->find($pa_id)->with($this->properties);
    }

    /**
     * @param $pa_id
     *
     * @return Builder
     */
    public function getPACompletionStates(int $pa_id)
    {
        return PACompletion::where('pa_id', '=', $pa_id)->orderBy('step_id', 'asc');
    }

    /**
     * @param $pa_id
     * \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @return Builder
     */
    public function getPADataCategories(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->dataCategories();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPADataTypes(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->dataTypes();
    }

    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getLegalBasis(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->legalBasis();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPADataSubjects(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->dataSubjects();
    }

    /**
     * @param int $pa_id
     * @param $step_id
     *
     * @return Builder
     */
    public function getPACompletionState(int $pa_id, $step_id)
    {
        return $this->getPACompletionStates($pa_id)
            ->where('step_id', '=', $step_id);
    }

    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPAControllers(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->controllers();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPAInternalProcessors(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->internalProcessors();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPAExternalProcessors(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->externalProcessors();
    }

    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getPADisclosures(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->disclosureSubjects();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getPADPOs(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->DPOs();
    }


    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getApplications(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->applications();
    }

    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getRetentions(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->retentions();
    }

    /**
     * @param int $pa_id
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getSecurityMeasures(int $pa_id)
    {
        return $this->findProcessingActivity($pa_id)->securityMeasures();
    }


    /**
     * @param int $pa_id
     */
    public function initPACompletionState(int $pa_id)
    {
        PACompletion::create([
            'pa_id'     => $pa_id,
            'step_id'   => 0,
            'visited'   => true,
            'completed' => false
        ]);

        for ($step = 1; $step < self::PA_STATE_STEPS; ++$step) {
            PACompletion::create([
                'pa_id'     => $pa_id,
                'step_id'   => "" . $step,
                'visited'   => false,
                'completed' => false
            ]);
        }
    }

    /**
     * @param int $pa_id
     * @param int $step_id
     */
    public function setStepVisited(int $pa_id, int $step_id)
    {
        $this->getPACompletionState($pa_id, $step_id)
            ->update(['visited' => true]);
    }

    /**
     * @param int $pa_id
     * @param int $step_id
     */
    public function setStepComplete(int $pa_id, int $step_id)
    {
        $this->getPACompletionState($pa_id, $step_id)
            ->update(['completed' => true]);
    }


    public function setStepIncomplete(int $pa_id, int $step_id)
    {
        $this->getPACompletionState($pa_id, $step_id)
            ->update(['completed' => false]);
    }

    /**
     * @param ProcessingActivity $pa
     * @param $processTypes
     */
    private function setProcessTypes(ProcessingActivity $pa, array $processTypes)
    {
        $pa->processTypes()->sync($processTypes);
    }

    /**
     * @param ProcessingActivity $pa
     * @param $dataCategories
     */
    private function setDataCategories(ProcessingActivity $pa, array $dataCategories)
    {
        $pa->dataCategories()->sync($dataCategories);
    }

    /**
     * @param ProcessingActivity $pa
     * @param $dataTypes
     */
    private function setDataTypes(ProcessingActivity $pa, array $dataTypes)
    {
        $pa->dataTypes()->sync($dataTypes);
    }

    /**
     * @param ProcessingActivity $pa
     * @param array $legalBasis
     */
    private function setLegalBasis(ProcessingActivity $pa, array $legalBasis)
    {
        $pa->legalBasis()->sync($legalBasis);
    }

    /**
     * @param ProcessingActivity $pa
     * @param array $dataSources
     */
    private function setDataSources(ProcessingActivity $pa, array $dataSources)
    {
        $pa->dataSources()->sync($dataSources);
    }

    /**
     * @param ProcessingActivity $pa
     * @param array $dataSourceTypes
     */
    private function setDataSourceTypes(ProcessingActivity $pa, array $dataSourceTypes)
    {
        $pa->dataSourceTypes()->sync($dataSourceTypes);
    }

    /**
     * @param int $processingActivityId
     * @param array $units
     */
    public function toggleUnits(int $processingActivityId, array $units)
    {
        $pa = $this->findProcessingActivity($processingActivityId);

        $pa->units()->toggle($units);
    }

    /**
     * @param $processingActivityId
     * @param array $subjectsData
     *
     * @return mixed
     */
    public function updateSubjects(int $processingActivityId, array $subjectsData)
    {
        $processingActivity = $this->findProcessingActivity($processingActivityId);

        if (isset($subjectsData['data_subjects']) > 0) {
            $processingActivity
                ->dataSubjects()
                ->sync($subjectsData['data_subjects']);
        }

        if (isset($subjectsData['controllers']) > 0) {
            $processingActivity
                ->controllers()
                ->sync($subjectsData['controllers']);
        }

        if (isset($subjectsData['internal_processors']) > 0) {
            $processingActivity
                ->internalProcessors()
                ->sync($subjectsData['internal_processors']);
        }

        if (isset($subjectsData['external_processors']) > 0) {
            $processingActivity
                ->externalProcessors()
                ->sync($subjectsData['external_processors']);
        }

        if (isset($subjectsData['dpos']) > 0) {
            $processingActivity
                ->DPOs()
                ->sync($subjectsData['dpos']);
        }

        return $processingActivity;
    }

    /**
     * @param $processingActivityId
     * @param array $disclosureData
     *
     * @return integer
     */
    public function updateDisclosures(int $processingActivityId, array $disclosureData)
    {
        $processingActivity = $this->findProcessingActivity($processingActivityId);

        if (isset($disclosureData['id']) && $disclosureData['id'] > 0) {

            $ds = DisclosureSubjects::findOrFail($disclosureData['id']);
            $ds->update($disclosureData);

        } else {
            $ds = $processingActivity
                ->disclosureSubjects()
                ->create($disclosureData);
        }

        if ($disclosureData['extra_ue'] && count($disclosureData['answers']) > 0) {

            $answers = $disclosureData['answers'];
            $toSync = [];
            foreach ($answers as $answer) {
                $toSync[$answer['question_id']] = ['answer' => $answer['answer']];
            }

            $ds->answers()->sync($toSync);
        }

        return $ds->getAttribute('id');
    }


    /**
     * @param $disclosureId
     * @throws \Exception
     */
    public function deleteDisclosures($disclosureId)
    {
        DisclosureSubjects::findOrFail($disclosureId)->forceDelete();
    }

    /**
     * @param $processingActivityId
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function updateOtherData(int $processingActivityId, array $data)
    {
        $processingActivity = $this->findProcessingActivity($processingActivityId);

        if (count($data['retentions']) > 0) {
            $processingActivity
                ->retentions()
                ->sync($data['retentions']);
        }

        if (count($data['applications']) > 0) {
            $processingActivity
                ->applications()
                ->sync($data['applications']);
        }

        $processingActivity->update([
            'is_process_automated'          => $data['is_process_automated'],
            'has_profiling_activity'        => $data['has_profiling_activity'],
            'automated_process_description' => $data['automated_process_description'],
            'risk_estimate'                 => $data['risk_estimate'],
            'notes'                         => $data['notes'],
            'retention_description'         => $data['retention_description']
        ]);

        return $processingActivity;
    }

    /**
     * @param $processingActivityId
     * @param array $securityMeasures
     */
    public function updateSecurityMeasures(int $processingActivityId, array $securityMeasures = [])
    {
        $this->findProcessingActivity($processingActivityId)
            ->securityMeasures()
            ->toggle($securityMeasures);
    }

    public function getPersonal()
    {
        return $this
            ->getDataModel()
            ->select(['processing_activities.id', 'data_categories.id'])
            ->join('pa_data_categories', 'pa_id', '=', 'processing_activities.id')
            ->join('data_categories', 'data_categories.id', '=', 'pa_data_categories.data_category_id')
            ->where('data_categories.family', '=', 'personal')
            ->where('data_categories.name', '<>', 'Sanitari super sensibili')
            ->groupBy(['data_categories.id'])
            ->count();
    }

    public function getSensitive()
    {
        return $this
            ->getDataModel()
            ->select(['processing_activities.id', 'data_categories.id'])
            ->join('pa_data_categories', 'pa_id', '=', 'processing_activities.id')
            ->join('data_categories', 'data_categories.id', '=', 'pa_data_categories.data_category_id')
            ->where('data_categories.family', '=', 'sensitive')
            ->where('data_categories.name', '<>', 'Sanitari super sensibili')
            ->groupBy(['data_categories.id'])
            ->count();
    }

    public function getSuperSensitive()
    {
        return $this
            ->getDataModel()
            ->select(['processing_activities.id', 'data_categories.id'])
            ->join('pa_data_categories', 'pa_id', '=', 'processing_activities.id')
            ->join('data_categories', 'data_categories.id', '=', 'pa_data_categories.data_category_id')
            ->where('data_categories.name', '=', 'Sanitari super sensibili')
            ->groupBy(['data_categories.id'])
            ->count();
    }


    /**
     * @param $processingActivityId
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getUnits($processingActivityId)
    {
        return $this->findProcessingActivity($processingActivityId)->units();
    }

    /**
     * @param $processingActivityId
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getDataSources($processingActivityId)
    {
        return $this->findProcessingActivity($processingActivityId)->dataSources();
    }

    /**
     *  get all processing activities by unit
     * @param array $unitsIds
     * @return Builder
     */
    public function getPAByUnits(array $unitsIds)
    {
        return $this->getDataModel()->withUnits($unitsIds);
    }

    /**
     *  get all processing activities by internal processor
     * @param int $processorId
     * @return Builder
     */
    public function getPAByInternalProcessor(int $processorId)
    {
        return $this->getDataModel()->withInternalProcessor($processorId);
    }

    /**
     * @param ProcessingActivity $pa
     */
    public function setAdoptedSecurityMeasures(ProcessingActivity $pa)
    {
        $adoptedSecurityMeasures = SecurityMeasure::isAdopted()
            ->withCategoryFamily('org')
            ->get()
            ->pluck('id')
            ->toArray();

        $pa->attachSecurityMeasures($adoptedSecurityMeasures);
    }

    public function setAdoptedSecurityMeasuresToAll()
    {
        foreach ($this->getDataModel()->all() as $pa)
            $this->setAdoptedSecurityMeasures($pa);
    }

    /**
     * @param int $processingActivityId
     * @return ProcessingActivity
     */
    private function findProcessingActivity(int $processingActivityId)
    {
        return $this->find($processingActivityId)->first();
    }

    /**
     * @param $pa
     * @param bool $isActive
     * @throws \Exception
     */
    private function setIsActive(ProcessingActivity $pa, bool $isActive = true): void
    {
        if ($isActive) $pa->restore();
        else $pa->delete();
    }
}
