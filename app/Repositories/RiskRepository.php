<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;


use App\Models\ResidualRisk;
use App\Models\Risk;
use DB;
use Illuminate\Database\Query\Builder;

class RiskRepository extends BaseTableRepository
{
    /**
     * RiskRepository constructor.
     */
    public function __construct()
    {
        $this->makeTagHandlers('riskControls');
    }
    /**
     * @var array
     * Related eager loaded Models
     */
    protected $properties = [
        'riskSources',
        'residualRisk',
        'protectionCategory',
    ];

    /**
     * @return Risk
     */
    protected function getDataModel()
    {
        return new Risk();
    }

    /**
     * @param $projectId
     * @param $protectionCategoryId
     * @return Builder
     */
    public function getRisksByProtectionCategory($projectId, $protectionCategoryId)
    {
        return $this->initQuery()
            ->DPIAProject($projectId)
            ->protectionCategory($protectionCategoryId)
            ->orderBy('risk_level', 'desc');
    }

    public function getTotalRiskLevel()
    {
        return $this->getDataModel()
            ->select(DB::raw('SUM(risk_level - 1) as tot'))
            ->first()->tot;
    }

    public function getTotalResidualRisk()
    {
        return ResidualRisk::select(DB::raw('SUM(dpia_residual_risks.risk_level - 1) as tot'))
            ->first()->tot;
    }

    public function getProjectRiskLevel(int $projectId)
    {
        return intval(
            $this->getDataModel()
                ->DPIAProject($projectId)
                ->select(DB::raw('SUM(risk_level - 1) as tot'))
                ->first()->tot
        );
    }

    public function getProjectResidualRisk(int $projectId)
    {
        return ResidualRisk::select(DB::raw('SUM(dpia_residual_risks.risk_level - 1) as tot'))
            ->join('dpia_risks', 'dpia_residual_risks.risk_id', '=', 'dpia_risks.id')
            ->where('dpia_risks.dpia_project_id', '=', $projectId)
            ->first()->tot;
    }

    /**
     * @param int $id
     * @return Risk
     */
    public function findRiskById(int $id)
    {
        return $this->find($id)->first();
    }

    /**
     * @param int $riskId
     * @param int $controlId
     * @param array $attributes
     */
    public function attachControl(int $riskId, int $controlId, array $attributes)
    {
        $this
            ->findRiskById($riskId)
            ->attachControl($controlId, $attributes);
    }

    /**
     * @param int $riskId
     * @param int $controlId
     * @param array $attributes
     */
    public function updateControl(int $riskId, int $controlId, array $attributes)
    {
        $this->deleteControl($riskId, $controlId);

        $this->attachControl($riskId, $controlId, $attributes);
    }

    /**
     * @param int $riskId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getRiskControls(int $riskId)
    {
        return $this->findRiskById($riskId)->controls()->get();
    }

    /**
     * @param int $riskId
     * @param int $controlId
     */
    public function deleteControl(int $riskId, int $controlId)
    {
        $this->findRiskById($riskId)->detachControl($controlId);
    }
}
