<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App\Models\User;
use App\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class UsersRepository extends BaseTableRepository
{
    protected $properties = [
        'workingUnits',
        'delegatedUnits',
        'inChargeUnits',
        'qualifications',
        'directedUnits'
    ];

    /**
     * UsersRepository constructor.
     */
    public function __construct()
    {
        $this->makeTagHandlers('qualifications');
    }


    /**
     * @return Model
     */
    protected function getDataModel(): Model
    {
        return new User();
    }

    /**
     * @param int $userId
     * @return BelongsToMany
     */
    public function getWorkingUnit(int $userId)
    {
        return $this
            ->findUser($userId)
            ->workingUnits();
    }

    /**
     * @param int $userId
     * @return BelongsToMany
     */
    public function getInChargeUnits(int $userId)
    {
        return $this
            ->findUser($userId)
            ->inChargeUnits();
    }


    /**
     * @param int $userId
     * @return BelongsToMany
     */
    public function getDirectedUnits(int $userId)
    {
        return $this
            ->findUser($userId)
            ->directedUnits();
    }

    /**
     * @param int $userId
     * @param array $units
     */
    public function toggleWorkingUnits(int $userId, array $units)
    {
        $user = $this->findUser($userId);

        $result = $user->toggleWorkingUnits($units);

        $userInChargeRole = Role::getRoleByCode(Role::USER_IN_CHARGE);

        if (count($result['attached']) > 0 && !$user->hasRole(Role::USER_IN_CHARGE) && $userInChargeRole)
            $user->attachRole($userInChargeRole);

        if ($user->units()->count() === 0 && $userInChargeRole)
            $user->detachRole(Role::getRoleByCode(Role::USER_IN_CHARGE));
    }

    /**
     * @param int $userId
     * @param array $units
     * @throws \Exception
     */
    public function attachInChargeUnits(int $userId, array $units)
    {
        $user = $this->findUser($userId);

        foreach ($units as $u) $user->attachInChargeUnits($u);

        $user->attachRole(Role::getRoleByCode(Role::USER_IN_CHARGE));
    }

    /**
     * @param int $userId
     * @param int $unitId
     */
    public function detachInChargeUnit(int $userId, int $unitId)
    {
        $user = $this
            ->findUser($userId)
            ->detachInChargeUnits($unitId);

        $user->detachRole(Role::getRoleByCode(Role::USER_IN_CHARGE));
    }

    /**
     * @param $userId
     * @return User
     */
    public function findUser(int $userId): User
    {
        return $this->find($userId)->first();
    }

    /**
     * @param $userId
     * @param $roleId
     * @return array
     */
    public function toggleRole($userId, $roleId)
    {
        return $this->findUser($userId)->roles()->toggle($roleId);
    }

    /**
     * @param string $sortField
     * @param string $sortDirection
     * @param int $start
     * @param int $limit
     * @param bool $trashed
     * @param string $searchParams
     * @param string $searchOrParams
     *
     * @return TableResult
     */
    public function query(string $sortField, string $sortDirection, int $start, int $limit, bool $trashed,
                          string $searchParams, string $searchOrParams): TableResult
    {
        $user = \Auth::user();
        if (!$user) return new TableResult([], 0);


        if ($user->isDelegate()) {
            $query = $this->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);
            $delegatedUnits = $user->delegatedUnits()->get()->pluck('id')->toArray();

            $query->directorUnits($delegatedUnits)->withoutAdmin();

            return new TableResult($query->get()->toArray(), $query->totalCount);
        }

        if ($user->isUnitDirector()) {
            $query = $this->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);
            $directedUnits = $user->directedUnits()->get()->pluck('id')->toArray();

            $query->directorUnits($directedUnits)->withoutAdmin();

            return new TableResult($query->get()->toArray(), $query->totalCount);
        }

        if ($user->isAdmin()) {
            $query = $this->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);

            return new TableResult($query->get()->toArray(), $query->totalCount);
        }

        return new TableResult([], 0);
    }

    public function getUsersInCharge(array $unitIds = [], string $sortField = '', string $searchOrParams = '')
    {
        $query = $this->getDataModel()->withoutAdmin()->with($this->properties);
        $query = $this->addSearchOrParams($query, $searchOrParams);

        if (count($unitIds) > 0) {
            $query->whereHas('units', function (Builder $query) use ($unitIds) {
                $query->whereIn('unit_id', $unitIds);
            });
        }

        $query->orderBy($sortField, 'asc')->limit(200);

        return $query->get();
    }

    public function getAvailableUsersInCharge(array $unitIds = [], string $sortField = '', string $sortDirection = 'asc', string $searchOrParams = '')
    {
        $query = $this->initQuery();
        $query = $this->addSearchOrParams($query, $searchOrParams);

        if (count($unitIds) > 0) {
            $query->whereHas('units', function (Builder $query) use ($unitIds) {
                $query->whereNotIn('unit_id', $unitIds);
            });
        }

        $query->orderBy($sortField, $sortDirection);

        return $query->get();
    }

    /**
     * @param int $userId
     * @param int $unitId
     * @return int
     * @throws \Exception
     */
    public function toggleDelegateUnit(int $userId, int $unitId)
    {
        return $this->findUser($userId)->toggleDelegateToUnit($unitId);
    }

    /**
     * @param string $searchOrParams
     * @return Builder
     */
    public function getDirectors(string $searchOrParams)
    {
        $sortField = 'lastname';
        $sortDirection = 'asc';
        $start = 0;
        $limit = -1;
        $trashed = false;
        $searchParams = '';

        return $this
            ->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams)
            ->withRole(Role::UNIT_DIRECTOR);
    }

    /**
     * @param Builder $query
     * @param string $aggregateColumnName
     * @param string $keyword
     *
     * @return Builder
     */
    protected function addAggregateFilter(Builder $query, $aggregateColumnName, $keyword)
    {
        list($table, $columnName) = explode('.', $aggregateColumnName);

        $query->whereHas($table, function ($query) use ($table, $columnName, $keyword) {
            if ($table === 'units')
                $query->where('is_working_unit', '=', true);

            $query->where($columnName, 'LIKE', "%$keyword%");
        });

        return $query;
    }
}
