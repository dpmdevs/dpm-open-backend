<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repositories;

use App\Models\ThirdParty;

class ThirdPartiesRepository extends BaseTableRepository
{
    /**
     * ApplicationRepository constructor.
     */
    public function __construct()
    {
        $this->makeTagHandlers('applications');
    }

    /**
     * @return ThirdParty
     */
    protected function getDataModel()
    {
        return new ThirdParty();
    }

    /**
     * @param $searchOrCriteria
     *
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function getThirdPartiesExternalProcessors($searchOrCriteria)
    {
        $query = $this->getDataModel()
            ->with([
                'processingActivities',
                'processingActivities.dataSubjects',
                'processingActivities.dataCategories',
                'processingActivities.retentions',
            ])
            ->whereHas('processingActivities');

        return $this->addSearchOrParams($query, $searchOrCriteria);
    }
}