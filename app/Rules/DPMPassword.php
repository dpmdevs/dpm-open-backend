<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DPMPassword implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/', $value) === 1;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Il campo :attribute deve essere composto da 8 caratteri con minuscole, maiuscole, numeri e caratteri speciali.';
    }
}
