<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Instances;

use App\Logging\UserAccessLog;

abstract class DPMInstance
{
    /**
     * @return array
     */
    public function connectionData(): array
    {
        return [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => $this->databaseName(),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => true,
            'engine'    => null,
        ];
    }

    /**
     * @return array
     */
    public function storageDisk(): array
    {
        return [
            'driver' => 'local',
            'root'   => storage_path('app/customers/' . $this->connectionName()),
        ];
    }

    /**
     * @return array
     */
    public function logChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/' . $this->connectionName() . '/laravel.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
        ];
    }

    /**
     * @return array
     */
    public function accessLogChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/' . $this->connectionName() . '/user-access.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
            'via'    => UserAccessLog::class,
        ];
    }

    /**
     * @return array
     */
    public function queryLogChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/' . $this->connectionName() . '/queries.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
        ];
    }

    /**
     * Customer database name
     *
     * @return string
     */
    public abstract function databaseName(): string;

    /**
     * Customer connection name, should equals to customer domain name in url
     *
     * @return string
     */
    public abstract function connectionName(): string;

}