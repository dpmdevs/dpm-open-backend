<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Instances;


use App\Logging\UserAccessLog;

class MainInstance extends DPMInstance
{
    /**
     * @return string
     */
    public function databaseName(): string
    {
        return env('DB_DATABASE', 'dpm-new');
    }

    /**
     * @return string
     */
    public function connectionName(): string
    {
        return env('DB_CONNECTION', 'dev-connection');
    }

    /**
     * @return array
     */
    public function logChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
        ];
    }

    public function accessLogChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/user-access.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
            'via'    => UserAccessLog::class,
        ];
    }

    /**
     * @return array
     */
    public function queryLogChannel()
    {
        return [
            'driver' => 'daily',
            'path'   => storage_path('logs/queries.log'),
            'level'  => 'debug',
            'days'   => env('LOG_RETENTION_DAYS', 365),
        ];
    }
}
