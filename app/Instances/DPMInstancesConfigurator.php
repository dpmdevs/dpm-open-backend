<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Instances;

class DPMInstancesConfigurator
{
    /**
     * @var array
     */
    private $instances;

    /**
     * DPMInstancesConfigurator constructor.
     * @param array $instances
     */
    public function __construct(array $instances)
    {
        $this->instances = $instances;
    }

    /**
     * @return array
     */
    public function makeConnectionsConfig(): array
    {
        $instancesConfig = [];

        foreach ($this->instances as $instanceClass) {

            $instance = $this->createInstance($instanceClass);

            $instancesConfig[$instance->connectionName()] = $instance->connectionData();
        }

        return $instancesConfig;
    }

    /**
     * @return array
     */
    public function makeStorageDisks(): array
    {
        $storageDisks = [];

        foreach ($this->instances as $instanceClass) {

            $instance = $this->createInstance($instanceClass);

            $storageDisks[$instance->connectionName()] = $instance->storageDisk();
        }

        return $storageDisks;
    }

    /**
     * @return array
     */
    public function makeLogChannels(): array
    {
        $logChannels = [];

        foreach ($this->instances as $instanceClass) {

            $instance = $this->createInstance($instanceClass);

            $logChannels[$instance->connectionName()] = $instance->logChannel();
            $logChannels['user-access-' . $instance->connectionName()] = $instance->accessLogChannel();
            $logChannels['query-log-' . $instance->connectionName()] = $instance->queryLogChannel();
        }

        return $logChannels;
    }

    /**
     * @param $instanceClass
     * @return mixed
     */
    private function createInstance($instanceClass)
    {
        $className = "\App\Instances\\" . $instanceClass;

        $instance = new $className();

        return $instance;
    }
}