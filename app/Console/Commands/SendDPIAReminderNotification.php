<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Mail\DPIAReviewReminder;
use App\Models\Config;
use App\Models\DPIAProject;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class SendDPIAReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dpia-reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify and send dpia review reminder notifications';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $dpias = DPIAProject::all();

        foreach ($dpias as $dpia) {
            $reviewDate = $dpia->getAttribute('review_date');

            if ($reviewDate !== null && $reviewDate->toDateString() === Carbon::now()->toDateString()) {

                $recipient = Config::first()->getAttribute('notification_recipient');

                $this->info("Sending reminder notification to $recipient");

                Mail::to($recipient)
                    ->send(new DPIAReviewReminder($dpia));
            }
        }
    }
}
