<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class ControllerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'         => 'required|max:255',
            'company_number'       => 'nullable|max:255',
            'address'              => 'nullable|max:255',
            'holder_firstname'     => 'nullable|max:255',
            'holder_lastname'      => 'nullable|max:255',
            'holder_title'         => 'nullable|max:255',
            'holder_qualification' => 'nullable|max:255',
            'city'                 => 'nullable|max:255',
            'zip_code'             => 'nullable|max:20',
            'phone'                => 'nullable|max:255',
            'fax'                  => 'nullable|max:255',
            'email'                => 'nullable|email|max:255',
            'web'                  => 'nullable|max:255',
        ];
    }
}
