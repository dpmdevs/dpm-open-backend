<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class PARequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                => 'required|min:3|max:255',
            'description'         => 'required|max:5000',
            'process_types'       => 'required|array', // ['it','paper']
            'data_categories'     => 'required|array', // list of ids
            'is_active'           => 'required|boolean',
            'purpose'             => 'nullable|max:5000',
            'legal_references'    => 'nullable|max:5000',
            'legal_basis'         => 'array', // list of ids
            'data_sources'        => 'array', // list of ids
            'data_source_types'   => 'array', // list of ids
            'data_types'          => 'array', // list of ids
            'identification_code' => 'nullable|max:255',
            'retention_description' => 'nullable|max:5000',
        ];
    }
}
