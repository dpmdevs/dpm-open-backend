<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Middleware;

use App\Libs\CustomerInstanceNameParser;
use Auth;
use Closure;
use DB;
use Illuminate\Http\Request;

class UserActionLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    const AUTH_LOGIN_PATH = 'api/auth/login';

    public function handle(Request $request, Closure $next)
    {
        $userLog = $this->createUserLogString();

        if ($request->getRealMethod() === 'OPTIONS') return $next($request);
        if ($request->path() === self::AUTH_LOGIN_PATH) return $next($request);

        $requestLog = $this->createRequestLogString($request);

        \Log::info($_SERVER['REMOTE_ADDR'] . ' - ' . $userLog . ' - ' . $requestLog);

        DB::listen(function ($query) use ($userLog) {

            $instanceName = env('DB_CONNECTION');

            $sqlLog = $_SERVER['REMOTE_ADDR']
                . ' - '
                . $userLog
                . ' - '
                . $query->sql
                . ' - '
                . json_encode($query->bindings);

            \Log::channel('query-log-' . $instanceName)->info($sqlLog);
        });

        return $next($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    private function createRequestLogString(Request $request): string
    {
        return $request->getRealMethod() . " - " . $request->fullUrl() . ' - ' . json_encode($request->toArray());
    }

    /**
     * @return string
     */
    private function createUserLogString(): string
    {
        $user = Auth::user();

        return $user ?
            "User: #{$user->getAttribute('id')} {$user->getAttribute('firstname')} {$user->getAttribute('lastname')}" :
            "Anonymous";
    }
}
