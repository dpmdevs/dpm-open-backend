<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;


use App\Http\Requests\TableQueryRequest;
use App\Repositories\BaseTableRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Validator;


abstract class BaseTableController extends Controller
{
    const DEFAULT_LIMIT = 50;
    const DEFAULT_SORT_FIELD = 'id';
    const DEFAULT_SORT_DIRECTION = 'DESC';

    /**
     * @var $repository BaseTableRepository
     */
    protected $repository;

    public function __construct()
    {
        $this->repository = $this->getRepository();
    }

    /**
     * @return BaseTableRepository
     */
    public abstract function getRepository(): BaseTableRepository;


    /**
     * @param TableQueryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function query(TableQueryRequest $request)
    {
        $sortField = $request->get('sortField', self::DEFAULT_SORT_FIELD);
        $sortDirection = $request->get('sortDirection', self::DEFAULT_SORT_DIRECTION);
        $start = $request->get('start', 0);
        $limit = $request->get('limit', self::DEFAULT_LIMIT);
        $searchParams = $request->get('searchCriteria', '');
        $searchOrParams = $request->get('searchOrCriteria', '');
        $trashed = $request->get('trashed', '');

        if ($searchParams === null) $searchParams = '';

        $data = $this->repository
            ->query($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams)
            ->pack();

        return response()->json($data, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($id)
    {
        if (intval($id) === 0) return response('{}', 404);

        $this->repository->delete(intval($id));

        return response('{}', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function forceDelete($id)
    {
        if (intval($id) === 0) return response('{}', 404);

        $this->repository->forceDelete(intval($id));

        return response('{}', 200);
    }


    /**
     * @param FormRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function create(FormRequest $request)
    {
        $this->repository->store($this->extractValidKeys($request));

        return response('{}', 201);
    }

    /**
     * @param $id
     * @param FormRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($id, FormRequest $request)
    {
        $this->repository->update(intval($id), $this->extractValidKeys($request));

        return response('{}', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore($id)
    {
        $this->repository->restore(intval($id));

        return response('{}', 200);
    }

    /**
     * @param string $attributeName
     * @param int $id
     * @param Request $request
     * @param array $validation
     *
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function updateAttribute(string $attributeName, int $id, Request $request, array $validation = [])
    {
        if ($validation === []) $validation = [$attributeName => ''];

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors(), 'message' => ''], 422);

        $this->repository->updateAttribute($id, $attributeName, $validator->valid()[$attributeName]);

        return response('{}', 200);
    }

    /**
     * @param FormRequest $request
     * @return array
     */
    protected function extractValidKeys(FormRequest $request): array
    {
        return $request->only(array_keys($request->rules($request)));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getCount()
    {
        return response()->json([
            'count' => $this->repository->getCount(),
        ]);
    }
}
