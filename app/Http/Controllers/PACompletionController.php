<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Repositories\PARepository;
use Route;

class PACompletionController extends Controller
{
    private $repository;

    public function __construct(PARepository $repository)
    {
        $this->repository = $repository;
    }

    static function routes()
    {
        Route::get('pa-completion/{pa_id}', 'PACompletionController@getPACompletionState');
    }

    /**
     * @param $pa_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPACompletionState($pa_id)
    {
        $completionData = $this->repository
            ->getPACompletionStates($pa_id)
            ->get()
            ->toArray();

        return response()->json($completionData, 200);
    }


}
