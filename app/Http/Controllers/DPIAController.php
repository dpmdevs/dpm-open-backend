<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\Http\Requests\DPIARequest;
use App\Repositories\BaseTableRepository;
use App\Repositories\DPIARepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DPIAController extends BaseTableController
{
    /**
     * @var DPIARepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new DPIARepository();
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function storePreAssessment($id, Request $request)
    {
        $this->repository->storePreAssessment($id, $request->only(['answers'])['answers']);

        return response('{}', 200);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function storePrinciples($id, Request $request)
    {
        $this->repository->storePrinciples($id, $request->get('answers', []));

        return response('{}', 200);
    }

    /**
     * @param DPIARequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(DPIARequest $request)
    {
        $createData = $this->extractValidKeys($request);
        $createData['status'] = 'in corso';
        $createData['user_id'] = \Auth::id();
        $createData['review_date'] = Carbon::now()->addYear()->toDateString();

        $dpiaProject = $this->repository->store($createData);

        return response()->json($dpiaProject->getAttribute('id'), 201);
    }

    /**
     * @param $id
     * @param DPIARequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, DPIARequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDPIADetails($id)
    {
        return response()->json($this->repository->findDPIAById($id), 200);
    }

    /**
     * @param $projectId
     * @param $protectionGoalId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRisksByProtectionCategory($projectId, $protectionGoalId)
    {
        $riskRepository = App::make(App\Repositories\RiskRepository::class);

        return response()->json($riskRepository->getRisksByProtectionCategory($projectId, $protectionGoalId)->get());
    }

    public function getDPIAStatistics()
    {
        $riskRepository = App::make(App\Repositories\RiskRepository::class);

        return response()->json([
            'risk_level'    => $riskRepository->getTotalRiskLevel(),
            'residual_risk' => $riskRepository->getTotalResidualRisk()
        ]);
    }

    public function getDPIAProjectStatistics($projectId)
    {
        $riskRepository = App::make(App\Repositories\RiskRepository::class);

        return response()->json([
            'risk_level'    => $riskRepository->getProjectRiskLevel($projectId),
            'residual_risk' => $riskRepository->getProjectResidualRisk($projectId)
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateProportionalityEvaluation($id, Request $request)
    {
        return $this->updateAttribute('proportionality_evaluation', $id, $request,
            ['proportionality_evaluation' => 'required|boolean']);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateProportionalityReason($id, Request $request)
    {
        return $this->updateAttribute('proportionality_reason', $id, $request,
            ['proportionality_reason' => 'required|max:255']);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateReviewDate($id, Request $request)
    {
        return $this->updateAttribute('review_date', $id, $request,
            ['review_date' => 'required|date|date_format:Y-m-d']);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStatus($id, Request $request)
    {
        return $this->updateAttribute('status', $id, $request,
            ['status' => 'required|max:50']);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateNotes($id, Request $request)
    {
        return $this->updateAttribute('notes', $id, $request, ['notes' => 'max:512']);
    }
}
