<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class IndexController extends Controller
{
    /**
     * Show the DPM index html page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dpmIndexContent = 'Development environment';

        if (App::environment() === 'production') {
            try {
                $dpmIndexContent = \File::get(public_path('dist/index.html'));
            } catch (FileNotFoundException $ex) {
                $dpmIndexContent = 'Missing production DPM-UI index file.. the application is building or should to be built';
            }
        }

        return response($dpmIndexContent, 200);
    }
}
