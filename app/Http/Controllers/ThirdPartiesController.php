<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;


use App\Http\Requests\TableQueryRequest;
use App\Http\Requests\ThirdPartiesRequest;
use App\Http\Requests\ToggleApplicationsRequest;
use App\Models\Application;
use App\Repositories\BaseTableRepository;
use App\Repositories\ThirdPartiesRepository;

class ThirdPartiesController extends BaseTableController
{
    /**
     * @return ThirdPartiesRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new ThirdPartiesRepository();
    }

    /** @param ThirdPartiesRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(ThirdPartiesRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param ThirdPartiesRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, ThirdPartiesRequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $thirdPartyId
     * @param ToggleApplicationsRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleApplications($thirdPartyId, ToggleApplicationsRequest $request)
    {
        $changes = $this->repository->toggleTag('applications', $thirdPartyId, $request->get('applications'));

        return response($changes, 200);
    }

    /**
     * @param $thirdPartyId
     * @return mixed
     */
    public function getApplications($thirdPartyId)
    {
        return $this->repository->getAssociatedTags('applications', $thirdPartyId);
    }

    /**
     * @param $thirdPartyId
     * @param TableQueryRequest $request
     * @return mixed
     */
    public function getAvailableApplications($thirdPartyId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'applications',
            $thirdPartyId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new Application(),
            'third_party_id',
            'application_id',
            'app_third_parties',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }

    /**
     * @param TableQueryRequest $request
     * @return mixed
     */
    public function getThirdPartiesExternalProcessors(TableQueryRequest $request)
    {
        $searchOrCriteria = $request->get('searchOrCriteria', '');

        return $this->repository->getThirdPartiesExternalProcessors($searchOrCriteria)->get();
    }
}
