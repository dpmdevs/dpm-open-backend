<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\RetentionRequest;
use App\Repositories\BaseTableRepository;
use App\Repositories\RetentionsRepository;

class RetentionsController extends BaseTableController
{
    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new RetentionsRepository();
    }

    /**
     * @param RetentionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(RetentionRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param RetentionRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, RetentionRequest $request)
    {
        return $this->update($id, $request);
    }
}
