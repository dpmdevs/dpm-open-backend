<?php
/**
 *  Data Protection Manager (DPM) - GDPR Compliance
 *     Copyright (C)  2018 - All rights reserved - Studio Storti Srl
 */

namespace App\Http\Controllers;

use App\Http\Requests\ConfigRequest;
use App\Models\Config;
use App\Repositories\ControllerRepository;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * @param ConfigRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ConfigRequest $request)
    {
        $update = Config::findOrFail(1)->update($request->only([
            'notification_recipient',
            'internal_processor_active',
            'devices_active',
            'controller_id',
            'dpo_id'
        ]));

        return response()->json($update);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleController(Request $request)
    {
        Config::findOrFail(1)->update(['controller_id' => $request->get('controller_id')]);

        return response('{}', 200);
    }

    /**
     * @return array
     */
    public function getController()
    {
        return response(\App\Models\Controller::all(), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getAvailableControllers(Request $request)
    {
        $sortField = $request->get('sortField', 'company_name');
        $sortDirection = $request->get('sortDirection', 'asc');
        $start = $request->get('start', 0);
        $limit = $request->get('limit', 0);
        $searchParams = $request->get('searchCriteria', '');
        $searchOrParams = $request->get('searchOrCriteria', '');
        $trashed = $request->get('trashed', false);

        $controller = \App::make(ControllerRepository::class);

        $currentControllerId = Config::findOrFail(1)->getAttribute('controller_id');

        $query = $controller->makeQuery($sortField, $sortDirection, $start, $limit, $trashed, $searchParams, $searchOrParams);

        if($currentControllerId)
            $query->whereNotIn('id', [$currentControllerId]);

        $data = $query->orderBy($sortField, $sortDirection)->get();

        return response($data, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleDPO(Request $request)
    {
        Config::findOrFail(1)->update(['dpo_id' => $request->get('dpo_id')]);

        return response('{}', 200);
    }

    /**
     * @return array
     */
    public function getDPO()
    {
        return response(\App\Models\Dpo::all(), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableDPOs(Request $request)
    {
        $sortField = $request->get('sortField', 'id');
        $sortDirection = $request->get('sortDirection', 'desc');

        $currentDPOId = Config::findOrFail(1)->getAttribute('dpo_id');

        $query = \App\Models\Dpo::query();

        if($currentDPOId)
            $query->whereNotIn('id', [$currentDPOId]);

        $data = $query->orderBy($sortField, $sortDirection)->get();

        return response($data, 200);
    }
}
