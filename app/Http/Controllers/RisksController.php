<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\Http\Requests\RiskControlRequest;
use App\Http\Requests\RiskRequest;
use App\Http\Requests\TableQueryRequest;
use App\Repositories\BaseTableRepository;
use App\Repositories\RiskRepository;
use Illuminate\Http\Request;

class RisksController extends BaseTableController
{
    /**
     * @var RiskRepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new RiskRepository();
    }

    /**
     * @param $riskId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRiskControls($riskId)
    {
        $controls = $this->repository->getRiskControls($riskId);

        return response()->json($controls);
    }

    /**
     * @param $riskId
     * @param TableQueryRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableRiskControls($riskId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'riskControls',
            $riskId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new App\Models\Control(),
            'risk_id',
            'control_id',
            'dpia_risk_controls',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAcceptanceMotivation($id, Request $request)
    {
        $this->repository->updateAttribute($id, 'acceptance_motivation', $request->get('acceptance_motivation', ''));

        return response('{}', 200);
    }

    /**
     * @param RiskRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(RiskRequest $request)
    {
        $risk = $this->repository->store($this->extractValidKeys($request));
        $risk->riskSources()->attach($request->get('risk_sources'));

        return response('{}', 201);
    }

    /**
     * @param $id
     * @param RiskRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, RiskRequest $request)
    {
        $this->repository->update(intval($id), $this->extractValidKeys($request));
        $this->repository->findRiskById($id)->riskSources()->sync($request->get('risk_sources'));

        return response('{}', 200);
    }

    /**
     * @param $riskId
     * @param RiskControlRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createControl($riskId, RiskControlRequest $request)
    {
        $this->repository->attachControl($riskId, $request->get('control_id'), $request->only([
            'identifiability',
            'prejudicial_effect',
            'vulnerability',
            'risk_sources_capabilities'
        ]));

        return response('{}', 200);
    }

    /**
     * @param $riskId
     * @param $controlId
     * @param RiskControlRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateControl($riskId, $controlId, RiskControlRequest $request)
    {
        $this->repository->updateControl($riskId, $controlId, $request->only([
            'identifiability',
            'prejudicial_effect',
            'vulnerability',
            'risk_sources_capabilities'
        ]));

        return response('{}', 200);
    }

    /**
     * @param $riskId
     * @param $controlId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteControl($riskId, $controlId)
    {
        $this->repository->deleteControl($riskId, $controlId);

        return response('{}', 200);
    }
}
