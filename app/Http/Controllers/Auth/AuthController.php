<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libs\CustomerInstanceNameParser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return mixed | \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {

            $this->fireLockoutEvent($request);
            $this->sendLockoutResponse($request);

            return null;
        }

        $token = $this->attemptLogin($this->credentials($request));

        return $token ? $this->doSuccessLoginActions($request, $token) : $this->doFailedLoginActions($request);
    }


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return response()->json(['error' => 'invalid_credentials'], 401);
    }


    /**
     * Attempt to log the user into the application.
     *
     * @param array $credentials
     * @return bool|string
     */
    protected function attemptLogin(array $credentials)
    {
        return auth()->attempt($credentials);
    }


    /**
     * Send the response after the user was authenticated.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse($token)
    {
        return response()->json([
            'token' => $token
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUserAuthentication()
    {
        return response()->json([
            'is_user_authenticated' => auth()->check()
        ], 200);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $this->logUserLoggedOut();

        auth()->logout();

        return response()->json(auth()->guest(), 200);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'password'        => 'required',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    private function doFailedLoginActions(Request $request)
    {
// something went wrong whilst attempting to encode the token

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        $this->logUserFailedAccess($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @param Request $request
     * @param string $token
     * @return \Illuminate\Http\Response
     */
    private function doSuccessLoginActions(Request $request, $token)
    {
        $this->clearLoginAttempts($request);

        $this->logUserAccess();

        return $this->sendLoginResponse($token);
    }

    private function logUserAccess()
    {
        $user = \Auth::user();

        $isAdmin = $user->isAdmin() ? 'Admin' : 'Standard';

        \Log::channel($this->getAccessLogChannelName())->info(
            $_SERVER['REMOTE_ADDR']
            . ' - '
            . $isAdmin
            . ' User logged in: #'
            . $user->getKey()
            . ' '
            . $user->getAttribute('username')
            . ' '
            . $user->getAttribute('firstname')
            . ' '
            . $user->getAttribute('lastname')
            . ' '
        );
    }

    private function logUserFailedAccess(Request $request)
    {
        \Log::debug($this->getAccessLogChannelName());

        \Log::channel($this->getAccessLogChannelName())->notice(
            $_SERVER['REMOTE_ADDR']
            . ' - '
            . 'User Failed to logged in:'
            . ' '
            . $request->get('username')
        );
    }

    private function logUserLoggedOut()
    {
        $user = \Auth::user();

        $isAdmin = $user->isAdmin() ? 'Admin' : 'Standard';

        \Log::channel($this->getAccessLogChannelName())->info(
            $_SERVER['REMOTE_ADDR']
            . ' - '
            . $isAdmin
            . ' User logged out: #'
            . $user->getKey()
            . ' '
            . $user->getAttribute('username')
            . ' '
            . $user->getAttribute('firstname')
            . ' '
            . $user->getAttribute('lastname')
            . ' '
        );
    }

    private function getAccessLogChannelName()
    {
        return (env('APP_IS_MULTI_INSTANCE'))
            ? 'user-access-' . app(CustomerInstanceNameParser::class)->instanceName()
            : 'user-access-' . env('DB_CONNECTION');
    }
}
