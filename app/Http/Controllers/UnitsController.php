<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\Http\Requests\TableQueryRequest;
use App\Http\Requests\UnitRequest;
use App\Models\User;
use App\Repositories\BaseTableRepository;
use App\Repositories\UnitRepository;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;

class UnitsController extends BaseTableController
{
    /**
     * @var UnitRepository
     */
    protected $repository;


    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new UnitRepository();
    }

    public function createHandler(UnitRequest $request)
    {
        return parent::create($request);
    }

    public function updateHandler($id, UnitRequest $request)
    {
        return parent::update($id, $request);
    }

    public function getTree()
    {
        return $this->repository->getTree()->pack();
    }

    /**
     * @param $unitId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toggleDirectors($unitId, Request $request)
    {
        $unit = $this->repository->findUnit($unitId);
        $unitDirectors = $unit->directors();
        $newDirectorIds = $request->get('directors');

        $currentDirector = $unitDirectors
            ->whereNotIn('unit_directors.user_id', $newDirectorIds)
            ->first();

        if ($currentDirector) {

            $idToRemove = $currentDirector->getKey();

            $this->unsetDirectorRole([$idToRemove], $unitId);
            $unitDirectors->detach($idToRemove);
        }

        $changes = $this->repository->toggleTag('directors', $unitId, $request->get('directors'));

        if (count($changes['attached']) > 0) {
            $this->setDirectorRole($changes['attached']);

            foreach ($unit->getDescendants() as $descendantUnit) {

                $currentDescendantDirector = $descendantUnit->directors()->first();

                if (
                    !$currentDescendantDirector ||
                    ($currentDirector && $currentDescendantDirector->getKey() === $currentDirector->getKey())
                )
                    $descendantUnit->directors()->sync($newDirectorIds);
            }
        }

        if (count($changes['detached']) > 0) $this->unsetDirectorRole($changes['detached'], $unitId);

        return response($changes, 200);
    }

    /**
     * @param $unitId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDirectors($unitId)
    {
        return $this->repository->getAssociatedTags('directors', $unitId);
    }

    /**
     * @param $unitId
     * @param TableQueryRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableDirectors($unitId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'directors',
            $unitId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new User(),
            'unit_id',
            'user_id',
            'unit_directors',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }

    public function getUnitRelatedProcessingActivities($unitId)
    {
        return $this->repository->getUnitRelatedProcessingActivities($unitId)->get();
    }

    private function setDirectorRole(array $attachedUserIds)
    {
        $userRepo = App::make(UsersRepository::class);

        foreach ($attachedUserIds as $userId) {
            $userRepo
                ->findUser($userId)
                ->setDirectorRole();
        }
    }

    private function unsetDirectorRole(array $detachedUserIds, int $unitId)
    {
        $userRepo = App::make(UsersRepository::class);

        foreach ($detachedUserIds as $userId) {
            if ($userRepo
                    ->findUser($userId)
                    ->directedUnits()
                    ->whereNotIn('unit_directors.unit_id', [$unitId])
                    ->count()
                === 0)
                $userRepo
                    ->findUser($userId)
                    ->unsetDirectorRole()
                    ->setUserInChargeRole();
        }
    }
}
