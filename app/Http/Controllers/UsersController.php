<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\Http\Requests\TableQueryRequest;
use App\Http\Requests\ToggleQualificationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUnitsRequest;
use App\Models\Config;
use App\Models\User;
use App\Repositories\BaseTableRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UsersRepository;
use App\Role;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Log;

class UsersController extends BaseTableController
{
    /**
     * @var UsersRepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new UsersRepository();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLoggedUser()
    {
        $user = Auth::user();
        $userData = $user->toArray();

        $userData['roles'] = $user->roles()->get(['display_name', 'description', 'name'])->toArray();
        $userData['working_units'] = $user->workingUnits()->get()->toArray();
        $userData['in_charge_units'] = $user->inChargeUnits()->get()->toArray();
        $userData['delegated_units'] = $user->delegatedUnits()->get()->toArray();
        $userData['qualifications'] = $user->qualifications()->get()->toArray();
        $userData['directed_units'] = $user->directedUnits()->get()->toArray();

        $userData['homepage'] = $user->isSuperAdmin() ? 'dashboard' : 'user';

        return response()->json($userData, 200);
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(UserRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param UpdateUserRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, UpdateUserRequest $request)
    {
        $errorMessageResponse = ['message' => 'Errore di validazione', 'errors' => ''];

        $u = User::find($id);
        if ($u === null) return \response('Utente non trovato', 404);

        $password = $request->get('password');

        if ($password && $this->checkPassword($password, $u->getAttribute('password'))) {

            $errorMessageResponse['errors'] = ['password' => 'Non è consentito riutilizzare la stessa password'];

            return \response($errorMessageResponse, 422);
        }

        $this->repository->update(intval($id), $this->extractValidKeys($request));

        return response('{}', 200);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserConfig()
    {
        $roles = Auth::user()->roles()->get();

        $permissions = [];

        foreach ($roles as $role) {
            $permissions = array_merge($permissions, $role->perms()->orderBy('display_order', 'asc')->get()->toArray());
        }

        $sections = [];

        foreach ($permissions as $permission) {
            $sections = $this->addPermissionToSections($sections, $permission);
        }

        $instanceConfig = $this->loadConfig();

        $config = [
            'sections'       => $sections,
            'instanceConfig' => $instanceConfig->toArray()
        ];

        return response()->json($config, 200);
    }

    /**
     * @param array $sections
     * @param array $permission
     *
     * @return array
     */
    public function addPermissionToSections(array $sections = [], array $permission = [])
    {
        if ($permission === []) return $sections;

        $section = [
            'name'      => $permission['display_name'],
            'path'      => $permission['name'],
            'iconClass' => $permission['iconClass']
        ];

        if (strstr($permission['name'], '/')) {

            $subPermissions = explode('/', $permission['name']);

            $permissionPath = $subPermissions[0];

            $sectionIndex = array_search($permissionPath, array_column($sections, 'path'));

            if (!isset($sections[$sectionIndex]['children']))
                $sections[$sectionIndex]['children'][] = $section;
            else if (!$this->findSection($sections[$sectionIndex]['children'], $section))
                $sections[$sectionIndex]['children'][] = $section;

        } else {

            if (!$this->findSection($sections, $section))
                $sections[] = $section;

        }

        return $sections;
    }

    /**
     * @param $userId
     * @param $roleId
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleRole($userId, $roleId)
    {
        $notify = false;
        $user = User::find($userId);

        if ($user->isAdmin()) $notify = true;
        $changes = $this->repository->toggleRole($userId, $roleId);
        if ($user->fresh()->isAdmin()) $notify = true;

        if ($notify) \Event::fire(new App\Events\AddedAdminRole($user));

        return response()->json($changes);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserRoles($userId)
    {
        return User::withTrashed()->findOrFail($userId)->roles()->get();
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getUserAvailableRoles($userId, Request $request)
    {
        $currentUser = auth()->user();

        $sortField = $request->get('sortField', 'id');
        $sortDirection = $request->get('sortDirection', 'desc');
        $query = $this->repository
            ->addSearchParams(Role::query(), $request->get('searchCriteria', ''))
            ->whereNotIn('id', function ($query) use ($userId) {
                return $query->select('role_id')
                    ->from('role_user')
                    ->where('user_id', '=', $userId);
            });

        if(!$currentUser->isAdmin())
            $query->whereNotIn('id', function ($query) {
                return $query->select('id')->from('roles')->whereName(Role::SUPERADMIN);
            });

        return $query->orderBy($sortField, $sortDirection)->get();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistics()
    {
        $roleRepo = new RoleRepository();

        $userStatistics = [
            'count'        => User::count(),
            'usersPerRole' => $roleRepo->getUsersPerRole(5)
        ];

        return response()->json($userStatistics);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function checkPermission(Request $request): Response
    {
        $route = $request->get('route');

        if ($route == '/') return response(['canActivate' => true], 200);

        $user = \Auth::user();
        $response = [
            'route'       => $route,
            'canActivate' => false
        ];

        $userLog = $user ?
            "User: #{$user->getAttribute('id')} {$user->getAttribute('firstname')} {$user->getAttribute('lastname')}" :
            "Anonymous";

        Log::info(
            $_SERVER['REMOTE_ADDR']
            . ' - '
            . $userLog
            . ' - '
            . 'Asked permission for route '
            . $route
        );

        if ($user) $response['canActivate'] = $user->can($route) === true;

        return response($response, 200);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWorkingUnit($userId)
    {
        $units = $this->repository->getWorkingUnit($userId)->get();

        return \response()->json($units);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInChargeUnits($userId)
    {
        $units = $this->repository->getInChargeUnits($userId)->get();

        return \response()->json($units);
    }


    /**
     * @param $userId
     * @param UserUnitsRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleWorkingUnits($userId, UserUnitsRequest $request)
    {
        $this->repository->toggleWorkingUnits($userId, $request->get('unitIds', []));

        return \response('{}', 200);
    }

    /**
     * @param $userId
     * @param UserUnitsRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function attachInChargeUnits($userId, UserUnitsRequest $request)
    {
        $units = $request->get('unitIds', []);

        if ($units === []) $units = Auth::user()->directedUnits()->pluck('id')->toArray();

        try {
            $this->repository->attachInChargeUnits($userId, $units);
        } catch (\Exception $exception) {
            return \response()->json(['message' => $exception->getMessage()], 422);
        }

        return \response('{}', 200);
    }

    /**
     * @param $userId
     * @param $unitId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function detachInChargeUnits($userId, $unitId)
    {
        $this->repository->detachInChargeUnit($userId, $unitId);

        return \response('{}', 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirectedUnits()
    {
        $unitRepository = App::make(App\Repositories\UnitRepository::class);

        return Auth::user()->isSuperAdmin()
            ? $unitRepository->getTree(false)->pack()
            : $unitRepository->getDirectorUnitTree(Auth::user()->getKey())->pack();
    }

    public function getUsersInCharge(Request $request)
    {
        $sortField = $request->get('sortField', 'id');
        $searchOrParams = $request->get('searchOrCriteria', '');
        $unitId = $request->get('unitId', null);

        $currentUser = Auth::user();
        $unitIds = $this->getUnitIdsFilter($unitId, $currentUser);

        $data = $this->repository->getUsersInCharge($unitIds, $sortField, $searchOrParams);

        return response()->json($data, 200);
    }

    public function getAvailableUsersInCharge(Request $request)
    {
        $sortField = $request->get('sortField', 'id');
        $sortDirection = $request->get('sortDirection', 'desc');
        $searchOrParams = $request->get('searchOrCriteria', '');
        $unitId = $request->get('unitId', null);

        $currentUser = Auth::user();
        $unitIds = $this->getUnitIdsFilter($unitId, $currentUser);

        $data = $this->repository->getAvailableUsersInCharge($unitIds, $sortField, $sortDirection, $searchOrParams);

        return response()->json($data, 200);
    }

    /**
     * @return Config
     */
    private function loadConfig()
    {
        return Config::with(['controller', 'dpo'])->first();
    }

    /**
     * @param $userId
     * @param $unitId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function toggleDelegateUnit($userId, $unitId)
    {
        $result = $this->repository->toggleDelegateUnit($userId, $unitId);

        return response()->json($result, 200);
    }

    /**
     * @param $unitId
     * @param User $currentUser
     * @return array
     */
    private function getUnitIdsFilter($unitId, User $currentUser): array
    {
        $unitIds = [];

        if ($unitId === null) {
            if ($currentUser->isDelegate()) $unitIds = $currentUser->delegatedUnits()->pluck('id')->toArray();
            if ($currentUser->isUnitDirector()) $unitIds = $currentUser->directedUnits()->pluck('id')->toArray();
            if ($currentUser->isSuperAdmin()) $unitIds = [];
        } else {
            $unitIds = [$unitId];
        }

        return $unitIds;
    }

    /**
     * @param $entityId
     * @param ToggleQualificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleQualifications($entityId, ToggleQualificationRequest $request)
    {
        $changes = $this->repository->toggleTag('qualifications', $entityId, $request->get('qualificationIds', []));

        return response()->json($changes);
    }

    /**
     * @param $entityId
     * @return mixed
     */
    public function getQualifications($entityId)
    {
        return $this->repository->getAssociatedTags('qualifications', $entityId);
    }

    /**
     * @param $entityId
     * @param TableQueryRequest $request
     * @return mixed
     */
    public function getAvailableQualifications($entityId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'qualifications',
            $entityId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new App\Models\Qualification(),
            'user_id',
            'qualification_id',
            'users_qualifications',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirectors(Request $request)
    {
        return response()->json($this->repository->getDirectors($request->get('searchOrCriteria', ''))->get());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSysAdmins(Request $request)
    {
        return response()
            ->json($this->repository->getSysAdmins($request->get('searchOrCriteria', ''))->get());
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePassword($id, Request $request)
    {
        $currentHash = User::findOrFail($id)->getAttribute('password');
        $currentPassword = $request->get('current_password', '');
        $newPassword = $request->get('password', '');

        if (!$this->checkPassword($currentPassword, $currentHash))
            return \response('Autenticazione non riuscita', 422);

        if ($this->checkPassword($newPassword, $currentHash))
            return \response('Non è consentito riutilizzare la stessa password', 422);

        return $this->updateAttribute('password', $id, $request, [
            'current_password' => ['required'],
            'password'         => ['required', new App\Rules\DPMPassword]
        ]);
    }

    private function findSection(array $sections, array $section)
    {
        foreach ($sections as $s)
            if ($s['path'] === $section['path']) return true;

        return false;
    }

    /**
     * @param string $password
     * @param string $currentHash
     * @return bool
     */
    private function checkPassword(string $password, string $currentHash): bool
    {
        return \Hash::check($password, $currentHash);
    }
}
