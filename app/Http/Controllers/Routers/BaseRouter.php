<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

abstract class BaseRouter
{
    abstract public function makeRoutes();

    /**
     * @return string
     */
    abstract protected function getApiName(): string;

    /**
     * @return string
     */
    abstract protected function getControllerName(): string;

    /**
     * Creates Routes for tag handlers (to be implemented):
     *  - toggleEntity($entityId, Request $request)
     *  - getEntity($entityId)
     *  - getAvailableEntity($entityId, TableQueryRequest $request)
     *
     * @param string $relation
     */
    public function makeTagRoutes(string $relation)
    {
        $apiTarget = $this->fromCamelToKebab($relation);

        Route::put($this->getApiName() . "/{id}/$apiTarget",
            $this->getControllerName() . "@toggle" . ucfirst($relation));

        Route::get($this->getApiName() . "/{id}/$apiTarget",
            $this->getControllerName() . "@get" . ucfirst($relation));

        Route::get($this->getApiName() . "/{id}/available-$apiTarget",
            $this->getControllerName() . "@getAvailable" . ucfirst($relation)
        );
    }

    private function fromCamelToKebab(string $input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }

        return implode('-', $ret);
    }

    protected function makeCRUDRoutes()
    {
        Route::get($this->getApiName(), $this->getControllerName() . '@query');
        Route::post($this->getApiName(), $this->getControllerName() . '@createHandler');
        Route::put("{$this->getApiName()}/{id}", $this->getControllerName() . '@updateHandler');
        Route::delete("{$this->getApiName()}/{id}/force", $this->getControllerName() . '@forceDelete');
        Route::delete("{$this->getApiName()}/{id}", $this->getControllerName() . '@delete');
        Route::put("{$this->getApiName()}/{id}/restore", $this->getControllerName() . '@restore');
    }


    /**
     * @return \Illuminate\Routing\Route
     */
    protected function makeCountRoute()
    {
        return Route::get($this->getApiName() . '/count', $this->getControllerName() . '@getCount');
    }
}