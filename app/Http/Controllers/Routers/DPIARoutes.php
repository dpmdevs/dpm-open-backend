<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

class DPIARoutes extends BaseRouter
{

    public function makeRoutes()
    {
        $this->makeCRUDRoutes();

        Route::get($this->getApiName() . '/statistics', $this->getControllerName() . '@getDPIAStatistics');
        Route::get($this->getApiName() . '/{id}', $this->getControllerName() . '@getDPIADetails');
        Route::get($this->getApiName() . '/{id}/statistics', $this->getControllerName() . '@getDPIAProjectStatistics');
        Route::get($this->getApiName() . '/{id}/risks/{protectionCategory}', $this->getControllerName() . '@getRisksByProtectionCategory');

        Route::put($this->getApiName() . '/{id}/pre-assessment', $this->getControllerName() . '@storePreAssessment');
        Route::put($this->getApiName() . '/{id}/proportionality-evaluation', $this->getControllerName() . '@updateProportionalityEvaluation');
        Route::put($this->getApiName() . '/{id}/proportionality-reason', $this->getControllerName() . '@updateProportionalityReason');
        Route::put($this->getApiName() . '/{id}/principles', $this->getControllerName() . '@storePrinciples');
        Route::put($this->getApiName() . '/{id}/review-date', $this->getControllerName() . '@updateReviewDate');
        Route::put($this->getApiName() . '/{id}/status', $this->getControllerName() . '@updateStatus');
        Route::put($this->getApiName() . '/{id}/notes', $this->getControllerName() . '@updateNotes');
    }

    /**
     * @return string
     */
    protected function getApiName(): string
    {
        return 'dpia';
    }

    /**
     * @return string
     */
    protected function getControllerName(): string
    {
        return 'DPIAController';
    }
}
