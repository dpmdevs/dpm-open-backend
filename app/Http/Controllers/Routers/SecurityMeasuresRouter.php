<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

class SecurityMeasuresRouter extends BaseRouter
{

    public function makeRoutes()
    {
        Route::post($this->getApiName() . '/apply-adopted-security-measures', $this->getControllerName() . '@applyAdoptedSecurityMeasures');

        Route::get($this->getApiName() . '/{family}', $this->getControllerName() . '@queryWithFamily'); // org | tec
        Route::post($this->getApiName() . '/{family}', $this->getControllerName() . '@createHandler');
        Route::put($this->getApiName() . '/{id}', $this->getControllerName() . '@updateHandler');
        Route::delete($this->getApiName() . '/{id}', $this->getControllerName() . '@delete');

        /*
         * Min security measures
         */
        Route::get($this->getApiName() . '/{id}/sec-measures', $this->getControllerName() . '@getSecurityMeasure');
        Route::post($this->getApiName() . '/{id}/sec-measures', $this->getControllerName() . '@createSecurityMeasure');
        Route::put($this->getApiName() . '/sec-measures/{id}', $this->getControllerName() . '@updateSecurityMeasure');
        Route::delete($this->getApiName() . '/sec-measures/{id}', $this->getControllerName() . '@deleteSecurityMeasure');
        Route::put($this->getApiName() . '/sec-measures/{id}/restore', $this->getControllerName() . '@restoreSecurityMeasure');
    }

    /**
     * @return string
     */
    protected function getApiName(): string
    {
        return 'sec-measures-cat';
    }

    /**
     * @return string
     */
    protected function getControllerName(): string
    {
        return 'SecurityMeasuresController';
    }
}
