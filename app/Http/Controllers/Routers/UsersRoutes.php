<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

class UsersRoutes extends BaseRouter
{

    public function makeRoutes()
    {
        Route::get('user', $this->getControllerName() . '@getLoggedUser');
        Route::get('user/config', $this->getControllerName() . '@getUserConfig');

        $this->makeCRUDRoutes();

        Route::get($this->getApiName() . '/statistics', $this->getControllerName() . '@getStatistics');
        Route::post($this->getApiName() . '/{id}/change-pwd', $this->getControllerName() . '@changePassword');
        Route::post($this->getApiName() . '/check-permission', $this->getControllerName() . '@checkPermission');

        // User -> roles
        Route::put($this->getApiName() . '/{user}/role/{role}', $this->getControllerName() . '@toggleRole');
        Route::get($this->getApiName() . '/{id}/roles', $this->getControllerName() . '@getUserRoles');
        Route::get($this->getApiName() . '/{id}/available-roles', $this->getControllerName() . '@getUserAvailableRoles');

        // Units
        Route::get($this->getApiName() . '/directed-units', $this->getControllerName() . '@getDirectedUnits');
        Route::get($this->getApiName() . '/{id}/working-units', $this->getControllerName() . '@getWorkingUnit');
        Route::get($this->getApiName() . '/{id}/in-charge-units', $this->getControllerName() . '@getInChargeUnits');
        Route::put($this->getApiName() . '/{id}/working-units', $this->getControllerName() . '@toggleWorkingUnits');
        Route::put($this->getApiName() . '/{id}/in-charge-units', $this->getControllerName() . '@attachInChargeUnits');
        Route::delete($this->getApiName() . '/{id}/in-charge-units/{unitId}', $this->getControllerName() . '@detachInChargeUnits');

        // Users in charge
        Route::get($this->getApiName() . '/users-in-charge', $this->getControllerName() . '@getUsersInCharge');
        Route::get($this->getApiName() . '/available-users-in-charge', $this->getControllerName() . '@getAvailableUsersInCharge');

        // Director delegate
        Route::put($this->getApiName() . '/{id}/delegate-unit/{unitId}', $this->getControllerName() . '@toggleDelegateUnit');

        // Directors
        Route::get($this->getApiName() . '/directors', $this->getControllerName() . '@getDirectors');

        // Qualifications
        $this->makeTagRoutes('qualifications');
    }

    /**
     * @return string
     */
    protected function getControllerName(): string
    {
        return 'UsersController';
    }

    /**
     * @return string
     */
    protected function getApiName(): string
    {
        return 'users';
    }
}