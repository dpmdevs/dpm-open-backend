<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

class RisksRoutes extends BaseRouter
{

    public function makeRoutes()
    {
        $this->makeCRUDRoutes();

        Route::put($this->getApiName() . "/{id}/acceptance-motivation", $this->getControllerName() . '@updateAcceptanceMotivation');

        Route::get($this->getApiName() . "/{id}/controls", $this->getControllerName() . '@getRiskControls');
        Route::post($this->getApiName() . "/{id}/controls", $this->getControllerName() . '@createControl');
        Route::put($this->getApiName() . "/{id}/controls/{controlId}", $this->getControllerName() . '@updateControl');
        Route::delete($this->getApiName() . "/{id}/controls/{controlId}", $this->getControllerName() . '@deleteControl');

        $this->makeTagRoutes('riskControls');
    }

    /**
     * @return string
     */
    protected function getApiName(): string
    {
        return 'risks';
    }

    /**
     * @return string
     */
    protected function getControllerName(): string
    {
        return 'RisksController';
    }
}