<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Routers;


use Route;

class ProcessingActivityRouter extends BaseRouter
{

    public function makeRoutes()
    {
        Route::get($this->getApiName(), $this->getControllerName() . '@query');
        Route::get($this->getApiName() . '/statistics', $this->getControllerName() . '@getPAStatistics');
        Route::get($this->getApiName() . '/{id}/units', $this->getControllerName() . '@getUnits');
        Route::get($this->getApiName() . '/{id}/security-measures', $this->getControllerName() . '@getSecurityMeasures');
        Route::get($this->getApiName() . '/{id}', $this->getControllerName() . '@getFullProcessingActivityData');
        Route::get($this->getApiName() . '/{id}/data-sources', $this->getControllerName() . '@getDataSources');

        Route::post($this->getApiName() . '', $this->getControllerName() . '@createHandler');

        Route::put($this->getApiName() . '/{id}', $this->getControllerName() . '@updateHandler');
        Route::put($this->getApiName() . '/{id}/subjects', $this->getControllerName() . '@createSubjectsHandler');
        Route::put($this->getApiName() . '/{id}/disclosure', $this->getControllerName() . '@syncDisclosure');
        Route::put($this->getApiName() . '/{id}/other-data', $this->getControllerName() . '@syncOtherDataHandler');
        Route::put($this->getApiName() . '/{id}/security-measures', $this->getControllerName() . '@toggleSecurityMeasures');
        Route::put($this->getApiName() . '/{id}/units', $this->getControllerName() . '@toggleUnits');
        Route::put($this->getApiName() . '/{id}/restore', $this->getControllerName() . '@restore');

        Route::delete($this->getApiName() . '/{id}/force', $this->getControllerName() . '@forceDelete');
        Route::delete($this->getApiName() . '/{id}', $this->getControllerName() . '@delete');
        Route::delete($this->getApiName() . '/disclosure/{id}', $this->getControllerName() . '@deleteDisclosure');

        Route::post($this->getApiName() . '/record-of-pa/{format}', 'ProcessingActivitiesController@generateReport');
        Route::post($this->getApiName() . '/processor-record-of-pa/{format}', 'ProcessingActivitiesController@generateReportProcessor');
    }

    /**
     * @return string
     */
    protected function getApiName(): string
    {
        return 'processing-activities';
    }

    /**
     * @return string
     */
    protected function getControllerName(): string
    {
        return 'ProcessingActivitiesController';
    }
}
