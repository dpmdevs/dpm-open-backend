<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\ServerRequest;
use App\Http\Requests\TableQueryRequest;
use App\Models\Application;
use App\Models\Server;
use App\Repositories\BaseTableRepository;
use App\Repositories\ServerRepository;
use Illuminate\Http\Request;

class ServersController extends BaseTableController
{
    /**
     * @return ServerRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new ServerRepository();
    }

    /**
     * @param ServerRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(ServerRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param ServerRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, ServerRequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $serverId
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleApplications($serverId, Request $request)
    {
        $changes = $this->repository->toggleTag('applications', $serverId, $request->get('applications'));

        return response($changes, 200);
    }

    /**
     * @param $serverId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getApplications($serverId)
    {
        return $this->repository->getAssociatedTags('applications', $serverId);
    }

    /**
     * @param $serverId
     * @param TableQueryRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableApplications($serverId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'applications',
            $serverId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new Application(),
            'server_id',
            'application_id',
            'app_servers',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }
}
