<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Http\Requests\ApplicationSecMeasuresRequest;
use App\Http\Requests\TableQueryRequest;
use App\Models\Server;
use App\Models\ThirdParty;
use App\Repositories\ApplicationRepository;
use App\Repositories\BaseTableRepository;
use Illuminate\Http\Request;


class ApplicationsController extends BaseTableController
{
    /**
     * @var $repository ApplicationRepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new ApplicationRepository();
    }

    /**
     * @param ApplicationRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(ApplicationRequest $request)
    {
        $application = $this->repository->store($this->extractValidKeys($request));

        $application->thirdParties()->attach($request->get('third_parties', []));
        $application->servers()->attach($request->get('servers', []));

        $this->repository->setAdoptedSecurityMeasures($application);

        return response('{}', 201);
    }

    /**
     * @param $id
     * @param ApplicationRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, ApplicationRequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $id
     * @param ApplicationSecMeasuresRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleSecurityMeasures($id, ApplicationSecMeasuresRequest $request)
    {
        $this->repository->toggleSecurityMeasures($id, $request->get('security_measures'));

        return response('{}', 200);
    }

    /**
     * @param $applicationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSecurityMeasures($applicationId)
    {
        $securityMeasures = $this->repository
            ->getSecurityMeasures($applicationId)
            ->get();

        return response()->json($securityMeasures);
    }

    /**
     * @param $applicationId
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleThirdParties($applicationId, Request $request)
    {
        $changes = $this->repository->toggleTag('thirdParties', $applicationId, $request->get('thirdParties'));

        return response($changes, 200);
    }

    /**
     * @param $applicationId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getThirdParties($applicationId)
    {
        return $this->repository->getAssociatedTags('thirdParties', $applicationId);
    }

    /**
     * @param $applicationId
     * @param TableQueryRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableThirdParties($applicationId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'thirdParties',
            $applicationId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new ThirdParty(),
            'application_id',
            'third_party_id',
            'app_third_parties',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }

    /**
     * @param $applicationId
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleServers($applicationId, Request $request)
    {
        $changes = $this->repository->toggleTag('servers', $applicationId, $request->get('servers'));

        return response($changes, 200);
    }

    /**
     * @param $applicationId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getServers($applicationId)
    {
        return $this->repository->getAssociatedTags('servers', $applicationId);
    }

    /**
     * @param $applicationId
     * @param TableQueryRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailableServers($applicationId, TableQueryRequest $request)
    {
        return $this->repository->getAvailableTag(
            'servers',
            $applicationId,
            $request->get('searchCriteria', ''),
            $request->get('searchOrCriteria', ''),
            new Server(),
            'application_id',
            'server_id',
            'app_servers',
            $request->get('sortField', ''),
            $request->get('sortDirection', '')
        );
    }
}
