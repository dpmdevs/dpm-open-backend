<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Permission;
use App\Repositories\PermissionsRepository;
use App\Role;
use Request;

class PermissionsController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new PermissionsRepository();
    }

    public function attachPermission(Role $role, Permission $permission)
    {
        $role->attachPermission($permission);

        return response('', 200);
    }

    public function detachPermission(Role $role, Permission $permission)
    {
        $role->attachPermission($permission);

        return response('', 200);
    }

    /**
     * @param $roleId
     * @return mixed
     */
    public function getRolePermissions($roleId)
    {
        return Role::withTrashed()->findOrFail($roleId)->perms()->get();
    }

    /**
     * @param $roleId
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getRoleAvailablePermissions($roleId, Request $request)
    {
        return $this->repository
            ->addSearchParams(Permission::select('*'), $request->get('searchCriteria', ''))
            ->whereNotIn('id', function ($query) use ($roleId) {
                return $query->select('permission_id')
                    ->from('permissions_role')
                    ->where('role_id', '=', $roleId);
            })
            ->get();
    }
}
