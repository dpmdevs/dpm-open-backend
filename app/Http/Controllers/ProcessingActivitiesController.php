<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;


use App;
use App\Http\Requests\PADisclosureRequest;
use App\Http\Requests\PAOtherDataRequest;
use App\Http\Requests\PARequest;
use App\Http\Requests\PASecurityMeasuresRequest;
use App\Http\Requests\PASubjectsRequest;
use App\Http\Requests\PAUnitsRequest;
use App\Libs\DocGenerator;
use App\Models\Documents\PAReport;
use App\Models\ProcessingActivity;
use App\Repositories\BaseTableRepository;
use App\Repositories\PARepository;
use Illuminate\Http\Request;
use Storage;

class ProcessingActivitiesController extends BaseTableController
{
    const BASIC_DATA_STEP = 0;
    const SUBJECTS_STEP = 1;
    const DISCLOSURES_STEP = 2;
    const SECURITY_MEASURES_STEP = 3;
    const OTHER_DATA_STEP = 4;

    /**
     * @var PARepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new PARepository();
    }

    /**
     * @param PARequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(PARequest $request)
    {
        $pa = $this->repository->store($request->all());

        $this->repository->setStepVisited($pa->getKey(), self::BASIC_DATA_STEP);

        if ($this->isBasicDataStepComplete($request)) {
            $this->repository->setStepComplete($pa->getKey(), self::BASIC_DATA_STEP);
        }

        $this->repository->setAdoptedSecurityMeasures($pa);

        return response()->json($pa->getKey(), 201);
    }

    /**
     * @param $processingActivityId
     * @param PARequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function updateHandler($processingActivityId, PARequest $request)
    {
        $this->repository->update($processingActivityId, $request->all());

        if ($this->isBasicDataStepComplete($request)) {
            $this->repository->setStepComplete($processingActivityId, self::BASIC_DATA_STEP);
        } else {
            $this->repository->setStepIncomplete($processingActivityId, self::BASIC_DATA_STEP);
        }

        return response('{}', 200);
    }

    /**
     * @param $processingActivityId
     * @param PASubjectsRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createSubjectsHandler($processingActivityId, PASubjectsRequest $request)
    {
        $this->repository->updateSubjects($processingActivityId, $request->all());

        $this->repository->setStepVisited($processingActivityId, self::SUBJECTS_STEP);

        if ($this->isSubjectsStepComplete($request)) {
            $this->repository->setStepComplete($processingActivityId, self::SUBJECTS_STEP);
        } else {
            $this->repository->setStepIncomplete($processingActivityId, self::SUBJECTS_STEP);
        }

        return response('{}', 200);
    }

    /**
     * Checks only against NOT required fields
     *
     * @param PARequest $request
     * @return bool
     */
    public function isBasicDataStepComplete(PARequest $request)
    {
        if ($request->get('purpose', '') === '') return false;
        if ($request->get('data_source_types', '') === '') return false;

        return true;
    }

    /**
     * @param PASubjectsRequest $request
     * @return bool
     */
    public function isSubjectsStepComplete(PASubjectsRequest $request)
    {
        if ($request->get('controllers', []) === []) return false;
        if ($request->get('data_subjects', []) === []) return false;
        if ($request->get('internal_processors', []) === []) return false;
        if ($request->get('dpos', []) === []) return false;

        return true;
    }

    /**
     * @param $pa_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFullProcessingActivityData($pa_id)
    {
        $fullProcessingActivityData = $this->repository->getPAFullData($pa_id)->first();

        return response()->json($fullProcessingActivityData, 200);
    }

    /**
     * @param $processingActivityId
     * @param PADisclosureRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function syncDisclosure($processingActivityId, PADisclosureRequest $request)
    {
        $disclosureId = $this->repository->updateDisclosures($processingActivityId, $request->all());

        $this->repository->setStepVisited($processingActivityId, self::DISCLOSURES_STEP);
        $this->repository->setStepComplete($processingActivityId, self::DISCLOSURES_STEP);

        return response(['id' => $disclosureId], 200);
    }

    /**
     * @param $disclosureId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function deleteDisclosure($disclosureId)
    {
        $this->repository->deleteDisclosures($disclosureId);

        return response('{}', 200);
    }

    /**
     * @param $processingActivityId
     * @param PAOtherDataRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function syncOtherDataHandler($processingActivityId, PAOtherDataRequest $request)
    {
        $this->repository->updateOtherData($processingActivityId, $request->all());

        $this->repository->setStepVisited($processingActivityId, self::OTHER_DATA_STEP);

        if ($this->isOtherDataStepComplete($request)) {
            $this->repository->setStepComplete($processingActivityId, self::OTHER_DATA_STEP);
        } else {
            $this->repository->setStepIncomplete($processingActivityId, self::OTHER_DATA_STEP);
        }

        return response('{}', 200);
    }

    /**
     * @param PAOtherDataRequest $request
     * @return bool
     */
    public function isOtherDataStepComplete(PAOtherDataRequest $request)
    {
        if ($request->get('retentions', []) === []) return false;
        if ($request->get('applications', []) === []) return false;
        if ($request->get('risk_estimate', null) === null) return false; // TODO test null value!

        return true;
    }

    /**
     * @param $processingActivityId
     * @param PASecurityMeasuresRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toggleSecurityMeasures($processingActivityId, PASecurityMeasuresRequest $request)
    {
        $this->repository->updateSecurityMeasures($processingActivityId, $request->only('security_measures')['security_measures']);

        $this->repository->setStepVisited($processingActivityId, self::SECURITY_MEASURES_STEP);
        $this->repository->setStepComplete($processingActivityId, self::SECURITY_MEASURES_STEP);

        return response('{}', 200);
    }

    /**
     * @param $processingActivityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSecurityMeasures($processingActivityId)
    {
        return response()->json($this->repository->getSecurityMeasures($processingActivityId)->get()->toArray());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPAStatistics()
    {
        $statistics = [
            'count'          => ProcessingActivity::all()->count(),
            'personal'       => $this->repository->getPersonal(),
            'sensitive'      => $this->repository->getSensitive(),
            'superSensitive' => $this->repository->getSuperSensitive(),
        ];

        return response()->json($statistics);
    }

    /**
     * @param $processingActivityId
     * @param PAUnitsRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function toggleUnits($processingActivityId, PAUnitsRequest $request)
    {
        $this->repository->toggleUnits($processingActivityId, $request->only('units')['units']);

        return response('{}', 200);
    }


    /**
     * @param $processingActivityId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUnits($processingActivityId)
    {
        return $this->repository->getUnits($processingActivityId)->get();
    }

    /**
     * @param $processingActivityId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getDataSources($processingActivityId)
    {
        return $this->repository->getDataSources($processingActivityId)->get();
    }
}
