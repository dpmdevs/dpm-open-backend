<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Permission;
use App\Repositories\BaseTableRepository;
use App\Repositories\RoleRepository;
use App\Role;
use Illuminate\Http\Request;

class RolesController extends BaseTableController
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new RoleRepository();
    }

    /**
     * @param RoleRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(RoleRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param RoleRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, RoleRequest $request)
    {
        return $this->update($id, $request);
    }


    public function togglePermission(Role $role, Permission $permission)
    {
        $changes = $this->repository->togglePermission($role, $permission);

        return response()->json($changes);
    }

    /**
     * @param $roleId
     * @return mixed
     */
    public function getPermissions($roleId)
    {
        return Role::withTrashed()->findOrFail($roleId)->perms()->get();
    }

    /**
     * @param $roleId
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getAvailablePermissions($roleId, Request $request)
    {
        $sortField = $request->get('sortField', 'id');
        $sortDirection = $request->get('sortDirection', 'desc');

        return $this->repository
            ->addSearchParams(Permission::select('*'), $request->get('searchCriteria', ''))
            ->whereNotIn('id', function ($query) use ($roleId) {
                return $query->select('permission_id')
                    ->from('permission_role')
                    ->where('role_id', '=', $roleId);
            })
            ->orderBy($sortField, $sortDirection)
            ->get();
    }
}
