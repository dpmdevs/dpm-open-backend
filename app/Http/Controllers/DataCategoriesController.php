<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\DataCategoryRequest;
use App\Http\Requests\DataTypeRequest;
use App\Http\Requests\TableQueryRequest;
use App\Repositories\BaseTableRepository;
use App\Repositories\DataCategoriesRepository;

class DataCategoriesController extends BaseTableController
{
    /** @var DataCategoriesRepository */
    protected $repository;

    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new DataCategoriesRepository();
    }

    /**
     * @param $family
     * @param TableQueryRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function queryWithFamily($family, TableQueryRequest $request)
    {
        $this->repository->setFamily($family);

        return $this->query($request);
    }

    /**
     * @param DataCategoryRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createHandler(DataCategoryRequest $request)
    {
        return $this->create($request);
    }

    /**
     * @param $id
     * @param DataCategoryRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, DataCategoryRequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDataTypes($id)
    {
        return $this->repository->getDataTypes($id);
    }

    /**
     * @param $categoryId
     * @param DataTypeRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createDataType($categoryId, DataTypeRequest $request)
    {
        $this->repository->createDataType($categoryId, $request->all());

        return response('{}', 201);
    }

    /**
     * @param $dataTypeId
     * @param DataTypeRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateDataType($dataTypeId, DataTypeRequest $request)
    {
        $this->repository->updateDataType($dataTypeId, $request->all());

        return response('{}', 200);
    }

    /**
     * @param $dataTypeId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteDataType($dataTypeId)
    {
        $this->repository->deleteDataType($dataTypeId);

        return response('{}', 200);
    }

    /**
     * @param $dataTypeId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restoreDataType($dataTypeId)
    {
        $this->repository->restoreDataType($dataTypeId);

        return response('{}', 200);
    }
}
