<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Requests\SecurityMeasureCategoryRequest;
use App\Http\Requests\SecurityMeasureRequest;
use App\Http\Requests\TableQueryRequest;
use App\Repositories\ApplicationRepository;
use App\Repositories\BaseTableRepository;
use App\Repositories\PARepository;
use App\Repositories\SecurityMeasuresRepository;

class SecurityMeasuresController extends BaseTableController
{
    /** @var SecurityMeasuresRepository */
    protected $repository;


    /**
     * @return BaseTableRepository
     */
    public function getRepository(): BaseTableRepository
    {
        return new SecurityMeasuresRepository();
    }

    public function queryWithFamily($family, TableQueryRequest $request)
    {
        $this->repository->setFamily($family);

        return $this->query($request);
    }

    /**
     * @param $family
     * @param SecurityMeasureCategoryRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createHandler($family, SecurityMeasureCategoryRequest $request)
    {
        $this->repository->storeCategory($family, $request->all());

        return response('{}', 201);
    }

    /**
     * @param $id
     * @param SecurityMeasureCategoryRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateHandler($id, SecurityMeasureCategoryRequest $request)
    {
        return $this->update($id, $request);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSecurityMeasure($id)
    {
        return $this->repository->getSecurityMeasure($id);
    }


    public function createSecurityMeasure($categoryId, SecurityMeasureRequest $request)
    {
        $this->repository->createSecurityMeasure($categoryId, $request->all());

        return response('{}', 201);
    }

    /**
     * @param $securityMeasureId
     * @param SecurityMeasureRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateSecurityMeasure($securityMeasureId, SecurityMeasureRequest $request)
    {
        $this->repository->updateSecurityMeasure($securityMeasureId, $request->all());

        return response('{}', 200);
    }

    /**
     * @param $securityMeasureId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function deleteSecurityMeasure($securityMeasureId)
    {
        $this->repository->deleteSecurityMeasure($securityMeasureId);

        return response('{}', 200);
    }

    /**
     * @param $securityMeasureId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function restoreSecurityMeasure($securityMeasureId)
    {
        $this->repository->restoreSecurityMeasure($securityMeasureId);

        return response('{}', 200);
    }

    public function applyAdoptedSecurityMeasures()
    {
        \App::make(PARepository::class)->setAdoptedSecurityMeasuresToAll();
        \App::make(ApplicationRepository::class)->setAdoptedSecurityMeasuresToAll();

        return response('{}', 200);
    }
}
