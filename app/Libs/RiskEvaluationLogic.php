<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Libs;


class RiskEvaluationLogic
{
    const NEGLIGIBLE = 1;
    const LIMITED = 2;
    const SIGNIFICANT = 3;
    const MAXIMUM = 4;

    /**
     * @param int $severity
     * @param int $likelihood
     * @return int
     */
    public static function calculateRiskLevel(int $severity, int $likelihood): int
    {
        return $severity * $likelihood;
    }

    /**
     * @param int $identifiability
     * @param int $prejudicial_effect
     * @return int
     */
    public static function calculateSeverity(int $identifiability, int $prejudicial_effect)
    {
        return self::map($identifiability + $prejudicial_effect);
    }

    /**
     * @param int $vulnerability
     * @param int $risk_sources_capabilities
     * @return int
     */
    public static function calculateLikelihood(int $vulnerability, int $risk_sources_capabilities)
    {
        return self::map($vulnerability + $risk_sources_capabilities);
    }

    /**
     * @param int $value
     * @return int
     */
    public static function map(int $value): int
    {
        if ($value < 5) return self::NEGLIGIBLE;
        if ($value === 5) return self::LIMITED;
        if ($value === 6) return self::SIGNIFICANT;
        if ($value > 6) return self::MAXIMUM;

        return 0;
    }
}