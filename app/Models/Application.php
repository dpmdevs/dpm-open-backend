<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Application
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $supplier
 * @property string|null $brand
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SecurityMeasure[] $securityMeasures
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Server[] $servers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThirdParty[] $thirdParties
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereSupplier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Application whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Application withoutTrashed()
 * @mixin \Eloquent
 */
class Application extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'brand'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Get the security measures for the application.
     */
    public function securityMeasures()
    {
        return $this
            ->belongsToMany('App\Models\SecurityMeasure',
                'app_security_measures',
                'application_id',
                'security_measure_id')
            ->withTimestamps();
    }

    /**
     * Get third parties for the application.
     */
    public function thirdParties()
    {
        return $this
            ->belongsToMany('App\Models\ThirdParty',
                'app_third_parties',
                'application_id',
                'third_party_id')
            ->withTimestamps();
    }

    /**
     * Get third parties for the application.
     */
    public function servers()
    {
        return $this
            ->belongsToMany('App\Models\Server',
                'app_servers',
                'application_id',
                'server_id')
            ->withTimestamps();
    }
}
