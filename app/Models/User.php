<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $user_number
 * @property string $fiscal_code
 * @property string|null $sex
 * @property \Carbon\Carbon|null $dismissal_date
 * @property string|null $binding_field
 * @property mixed|null $avatar
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $directedUnits
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User directorUnits($units)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereBindingField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDismissalDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFiscalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereHiringDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUserNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User withRole($role)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User withoutAdmin()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @method static bool|null restore()
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $delegatedUnits
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $inChargeUnits
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Qualification[] $qualifications
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $workingUnits
 * @property-read string $hiring_date
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    use SoftDeletes {
        restore as private restoreSD;
    }

    use EntrustUserTrait {
        restore as private restoreEUT;
    }


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'firstname', 'lastname', 'email', 'user_number', 'fiscal_code', 'sex', 'dismissal_date', 'binding_field', 'avatar'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['dismissal_date', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $value
     * @return string
     */
    public function getDismissalDateAttribute($value)
    {
        return $value !== null ? (new Carbon($value))->format('Y-m-d') : null;
    }

    /**
     * @param $value
     * @return string
     */
    public function getHiringDateAttribute($value)
    {
        return $value !== null ? (new Carbon($value))->format('Y-m-d') : null;
    }

    /**
     * Set the account's pwd
     *
     * @param  string $value
     * @return User
     */
    public function setPasswordAttribute($value)
    {
        if ($value == "") {
            unset($this->attributes['password']);
        } else {
            $this->attributes['password'] = Hash::make($value);
        }

        return $this;
    }

    /**
     * Set the account's avatar height and width to max 100x100 px
     *
     * @param  string $value
     * @return User
     */
    public function setAvatarAttribute($value)
    {
        if ($value === "" || $value === null) {
            unset($this->attributes['avatar']);
        } else {

            $avatar = Image::make($value);

            if ($avatar->width() > 100 || $avatar->height() > 100) {
                $avatar->resize(100, 100);
            }

            $this->attributes['avatar'] = (string)$avatar->encode('data-url');
        }

        return $this;
    }

    /**
     *
     */
    public function restore()
    {
        $this->restoreSD();
        $this->restoreEUT();
    }

    /**
     * Get the units for the user.
     * @return BelongsToMany
     */
    public function units()
    {
        return $this
            ->belongsToMany('App\Models\Unit', 'users_units', 'user_id', 'unit_id')
            ->withPivot('is_working_unit', 'is_delegate')
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function workingUnits()
    {
        return $this
            ->belongsToMany('App\Models\Unit', 'users_units', 'user_id', 'unit_id')
            ->wherePivot('is_working_unit', '=', true)
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function inChargeUnits()
    {
        return $this
            ->belongsToMany('App\Models\Unit', 'users_units', 'user_id', 'unit_id')
            ->wherePivot('is_working_unit', '=', false)
            ->withTimestamps();
    }

    /**
     * @param array $unitIds
     * @return array
     */
    public function toggleWorkingUnits(array $unitIds = [])
    {
        $attributes = ['is_working_unit' => true];

        return $this->units()->toggle($unitIds, true, $attributes);
    }

    /**
     * @param int $unitId
     * @return void
     * @throws \Exception
     */
    public function attachInChargeUnits(int $unitId)
    {
        $attributes = ['is_working_unit' => false];

        if (count($this->workingUnits()->where('unit_id', '=', $unitId)->get()->toArray()) > 0) {
            $unit = Unit::find($unitId);

            throw new \Exception(
                "L’unità " . $unit->getAttribute('name')
                . " risulta già associata all’utente "
                . $this->getAttribute('firstname') . " " . $this->getAttribute('lastname')
            );
        }

        $this->units()->attach($unitId, $attributes);
    }

    /**
     * @param int $unitId
     * @return User
     */
    public function detachInChargeUnits(int $unitId)
    {
        $attributes = ['is_working_unit' => false];

        $this->units()->detach($unitId, $attributes);

        return $this;
    }

    /**
     * @param int $unitId
     * @return int
     * @throws \Exception
     */
    public function toggleDelegateToUnit(int $unitId)
    {
        $currentValue = $this->units()
            ->where('unit_id', '=', $unitId);

        if (!$currentValue->first())
            $this->attachInChargeUnits($unitId);

        $attributes = [
            'is_delegate' => !$currentValue->first()->pivot->is_delegate
        ];

        return $this->units()->updateExistingPivot($unitId, $attributes);
    }

    /**
     * Get the qualifications for the user.
     * @return BelongsToMany
     */
    public function qualifications()
    {
        return $this
            ->belongsToMany(
                'App\Models\Qualification',
                'users_qualifications',
                'user_id',
                'qualification_id');
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(Role::SUPERADMIN);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        $isAdminRole =
            $this->isSuperAdmin()
            || $this->can('subjects/roles');

        return $isAdminRole;
    }

    /**
     * @return bool
     */
    public function isUnitDirector()
    {
        return $this->hasRole(Role::UNIT_DIRECTOR);
    }

    /**
     * @return bool
     */
    public function isDelegate()
    {
        return $this->hasRole(Role::DELEGATE);
    }


    /**
     * @return BelongsToMany
     */
    public function directedUnits()
    {
        return $this
            ->belongsToMany('App\Models\Unit',
                'unit_directors',
                'user_id',
                'unit_id')
            ->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function delegatedUnits()
    {
        return $this
            ->belongsToMany('App\Models\Unit', 'users_units', 'user_id', 'unit_id')
            ->wherePivot('is_delegate', '=', true)
            ->withTimestamps();
    }

    /**
     * Filter only this director units
     *
     * @param Builder $query
     * @param array $units
     * @return Builder
     */
    public function scopeDirectorUnits(Builder $query, array $units): Builder
    {
        return $query->whereHas('directedUnits', function (Builder $query) use ($units) {
                return $query->whereIn('id', $units);
            });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWithoutAdmin(Builder $query): Builder
    {
        return $query->whereNotIn('users.id', [0]);
    }

    /**
     * @return $this
     */
    public function setUserInChargeRole()
    {
        $userInChargeRole = Role::getRoleByCode(Role::USER_IN_CHARGE);

        $this->attachRole($userInChargeRole);

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetUserInChargeRole()
    {
        $userInChargeRole = Role::getRoleByCode(Role::USER_IN_CHARGE);

        $this->detachRole($userInChargeRole);

        return $this;
    }

    /**
     * @return $this
     */
    public function setDirectorRole()
    {
        $this->unsetUserInChargeRole();

        $directorRole = Role::getRoleByCode(Role::UNIT_DIRECTOR);

        $this->attachRole($directorRole);

        return $this;
    }

    /**
     * @return $this
     */
    public function setSuperadminRole()
    {
        $this->unsetUserInChargeRole();

        $role = Role::getRoleByCode(Role::SUPERADMIN);

        $this->attachRole($role);

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetDirectorRole()
    {
        $directorRole = Role::getRoleByCode(Role::UNIT_DIRECTOR);

        $this->detachRole($directorRole);

        return $this;
    }

    /**
     * Alias to eloquent many-to-many relation's attach() method.
     *
     * @param mixed $role
     */
    public function attachRole(Role $role)
    {
        if (!$this->hasRole($role->getAttribute('name'))) $this->entrustAttachRole($role);
    }

    /**
     * Alias to eloquent many-to-many relation's attach() method.
     *
     * @param mixed $role
     */
    private function entrustAttachRole($role)
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        $this->roles()->attach($role);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function username()
    {
        return 'username';
    }
}
