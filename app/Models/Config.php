<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Config
 *
 * @property int $id
 * @property string $company_name
 * @property string|null $holder_firstname
 * @property string|null $holder_lastname
 * @property string|null $holder_title
 * @property string|null $holder_qualification
 * @property string $notification_recipient
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereHolderFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereHolderLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereHolderQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereHolderTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereNotificationRecipient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $controller_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereControllerId($value)
 * @property-read \App\Models\Controller $controller
 * @property-read \App\Models\Dpo $dpo
 * @property int $dpo_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereDpoId($value)
 * @property bool $internal_processor_active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereInternalProcessorActive($value)
 * @property bool $devices_active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereDevicesActive($value)
 * @property int|null $training_attempts
 * @property int|null $training_threshold
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereTrainingAttempts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Config whereTrainingThreshold($value)
 */
class Config extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'notification_recipient',
        'internal_processor_active',
        'controller_id',
        'dpo_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at', 'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'internal_processor_active' => 'boolean',
    ];

    /**
     * Get the controllers for the processing activity.
     */
    public function controller()
    {
        return $this
            ->belongsTo(
                'App\Models\Controller',
                'controller_id'
            );
    }

    /**
     * Get the controllers for the processing activity.
     */
    public function dpo()
    {
        return $this
            ->belongsTo(
                'App\Models\Dpo',
                'dpo_id'
            );
    }

    /**
     * @return bool
     */
    public function isInternalProcessorActive()
    {
        return $this->getAttribute('internal_processor_active') === true;
    }
}
