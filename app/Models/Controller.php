<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Controller
 *
 * @property int $id
 * @property string $company_name
 * @property string|null $holder_firstname
 * @property string|null $holder_lastname
 * @property string|null $holder_title
 * @property string|null $holder_qualification
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereHolderFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereHolderLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereHolderQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereHolderTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Controller onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Controller withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Controller withoutTrashed()
 * @property string|null $company_number
 * @property string|null $company_address
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereCompanyAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereCompanyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereDeletedAt($value)
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $city
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $web
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereWeb($value)
 * @property string|null $zip_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereZipCode($value)
 * @property string|null $fiscal_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereFiscalCode($value)
 * @property mixed|null $logo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Controller whereLogo($value)
 */
class Controller extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'controllers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'company_number',
        'fiscal_code',
        'address',
        'holder_firstname',
        'holder_lastname',
        'holder_title',
        'holder_qualification',
        'city',
        'zip_code',
        'phone',
        'fax',
        'email',
        'web',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'updated_at'];
}
