<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use App\Models\Pivot\AnswerPivot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DPIAProject
 *
 * @property int $id
 * @property string $name
 * @property string|null $goal
 * @property string $status
 * @property bool $is_data_verified
 * @property \Carbon\Carbon $review_date
 * @property string|null $notes
 * @property int $processing_activity_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $preAssessmentAnswers
 * @property-read \App\Models\ProcessingActivity $processingActivity
 * @property-read \App\Models\User $reporter
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Risk[] $risks
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DPIAProject onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereGoal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereIsDataVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereProcessingActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereReviewDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DPIAProject withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DPIAProject withoutTrashed()
 * @mixin \Eloquent
 * @property bool $proportionality_evaluation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereProportionalityEvaluation($value)
 * @property string $proportionality_reason
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DPIAProject whereProportionalityReason($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $principlesAnswers
 */
class DPIAProject extends Model
{
    use SoftDeletes;

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'proportionality_reason' => ''
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpia_projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'goal', 'notes', 'status', 'proportionality_evaluation',
        'proportionality_reason', 'review_date', 'user_id', 'processing_activity_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'proportionality_evaluation' => 'boolean',
        'review_date'                => 'date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reporter()
    {
        return $this->belongsTo(
            'App\Models\User',
            'user_id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * Added with trashed in order to get processing activity related data even if related pa is soft deleted
     */
    public function processingActivity()
    {
        return $this->belongsTo(
            'App\Models\ProcessingActivity',
            'processing_activity_id',
            'id'
        )->withTrashed();
    }

    /**
     * @return mixed
     */
    public function preAssessmentAnswers()
    {
        return $this->belongsToMany(
            'App\Models\Question',
            'dpia_pre_ass_answers',
            'dpia_project_id',
            'question_id'
        )
            ->withPivot('answer');
    }

    /**
     * @return mixed
     */
    public function principlesAnswers()
    {
        return $this->belongsToMany(
            'App\Models\Question',
            'dpia_principles_answers',
            'dpia_project_id',
            'question_id'
        )
            ->using('App\Models\Pivot\AnswerPivot')
            ->withPivot('answer', 'description');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function risks()
    {
        return $this->hasMany('App\Models\Risk', 'dpia_project_id', 'id');
    }
}
