<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProcessingActivity
 *
 * @property mixed $id
 * @property string $name
 * @property string $description
 * @property string|null $notes
 * @property string $scope
 * @property string|null $purpose
 * @property int|null $risk_estimate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Application[] $applications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PACompletion[] $completionState
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $controllers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DataCategory[] $dataCategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DataSubject[] $dataSubjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DataType[] $dataTypes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DisclosureSubjects[] $disclosureSubjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $dpos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThirdParty[] $externalProcessors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $internalProcessors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LegalBasis[] $legalBasis
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProcessType[] $processTypes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Retention[] $retentions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SecurityMeasure[] $securityMeasures
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Unit[] $units
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProcessingActivity onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity wherePurpose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereRiskEstimate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProcessingActivity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProcessingActivity withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity withUnits($unitIds)
 * @property bool $is_process_automated
 * @property string|null $automated_process_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereAutomatedProcessDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereIsProcessAutomated($value)
 * @property bool $has_profiling_activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereHasProfilingActivity($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ThirdParty[] $dataSources
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity withoutUnits()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereDataSourcesType($value)
 * @property int $data_sources_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DataSourceType[] $dataSourceTypes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity withInternalProcessor($processorId)
 * @property bool $is_active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereScope($value)
 * @property string|null $identification_code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereIdentificationCode($value)
 * @property string|null $legal_references
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereLegalReferences($value)
 * @property string|null $retention_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessingActivity whereRetentionDescription($value)
 */
class ProcessingActivity extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'processing_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'identification_code',
        'name',
        'description',
        'purpose',
        'legal_references',
        'risk_estimate',
        'notes',
        'is_process_automated',
        'automated_process_description',
        'has_profiling_activity',
        'retention_description',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id'                     => 'numeric',
        'is_process_automated'   => 'boolean',
        'has_profiling_activity' => 'boolean',
    ];

    /**
     * Get the process types for the processing activity.
     */
    public function completionState()
    {
        return $this->hasMany('App\Models\PACompletion', 'pa_id', 'id');
    }

    /**
     * Get the process types for the processing activity.
     */
    public function processTypes()
    {
        return $this->belongsToMany('App\Models\ProcessType', 'pa_process_types', 'pa_id', 'process_type_id');
    }

    /**
     * Get the data categories for the processing activity.
     */
    public function dataCategories()
    {
        return $this
            ->belongsToMany('App\Models\DataCategory', 'pa_data_categories', 'pa_id')
            ->withTimestamps();
    }

    /**
     * Get the data types for the processing activity.
     */
    public function dataTypes()
    {
        return $this
            ->belongsToMany('App\Models\DataType', 'pa_data_types', 'pa_id')
            ->withTimestamps();
    }

    /**
     * Get the legal basis for the processing activity.
     */
    public function legalBasis()
    {
        return $this
            ->belongsToMany('App\Models\LegalBasis', 'pa_legal_basis', 'pa_id', 'legal_basis_id')
            ->withTimestamps();
    }


    /**
     * Get the data subjects for the processing activity.
     */
    public function dataSubjects()
    {
        return $this
            ->belongsToMany('App\Models\DataSubject', 'pa_data_subjects', 'pa_id', 'data_subjects_id')
            ->withTimestamps();
    }

    /**
     * Get the controllers for the processing activity.
     */
    public function controllers()
    {
        return $this
            ->belongsToMany(
                'App\Models\Controller',
                'pa_controllers',
                'pa_id',
                'controller_id'
            )
            ->withTimestamps();
    }

    /**
     * Get the internal processors for the processing activity (a User).
     */
    public function internalProcessors()
    {
        return $this
            ->belongsToMany('App\Models\User', 'pa_internal_processors', 'pa_id', 'user_id')
            ->withTimestamps();
    }

    /**
     * Get the external processors for the processing activity (a Third parties).
     */
    public function externalProcessors()
    {
        return $this
            ->belongsToMany('App\Models\ThirdParty', 'pa_external_processors', 'pa_id', 'third_parties_id')
            ->withTimestamps();
    }

    /**
     * Get the DPOs for the processing activity (a Dpo).
     */
    public function dpos()
    {
        return $this
            ->belongsToMany('App\Models\Dpo', 'pa_dpos', 'pa_id', 'dpo_id')
            ->withTimestamps();
    }

    /**
     * Get the disclosure subjects for the processing activity.
     */
    public function disclosureSubjects()
    {
        return $this
            ->hasMany(
                'App\Models\DisclosureSubjects',
                'pa_id'
            );
    }

    /**
     * Get the retentions subjects for the processing activity.
     */
    public function retentions()
    {
        return $this
            ->belongsToMany('App\Models\Retention', 'pa_retentions', 'pa_id', 'retention_id')
            ->withTimestamps();
    }

    /**
     * Get the applications for the processing activity.
     */
    public function applications()
    {
        return $this
            ->belongsToMany('App\Models\Application', 'pa_applications', 'pa_id', 'application_id')
            ->withTimestamps();
    }

    /**
     * Get the security measures for the processing activity.
     */
    public function securityMeasures()
    {
        return $this
            ->belongsToMany(
                'App\Models\SecurityMeasure',
                'pa_security_measures',
                'pa_id',
                'security_measure_id')
            ->withTimestamps();
    }

    /**
     * Get the units for the processing activity.
     */
    public function units()
    {
        return $this
            ->belongsToMany('App\Models\Unit', 'pa_units', 'pa_id', 'unit_id')
            ->withTimestamps();
    }

    /**
     * Get the data sources for the processing activity.
     */
    public function dataSources()
    {
        return $this
            ->belongsToMany('App\Models\ThirdParty',
                'pa_data_sources',
                'pa_id',
                'third_party_id');
    }

    /**
     * Get the legal basis for the processing activity.
     */
    public function dataSourceTypes()
    {
        return $this
            ->belongsToMany(
                'App\Models\DataSourceType',
                'pa_data_source_types',
                'pa_id',
                'data_source_type_id');
    }

    /**
     * @param Builder $query
     * @param array $unitIds
     * @return Builder
     */
    public function scopeWithUnits(Builder $query, array $unitIds)
    {
        return $query->whereHas('units', function ($query) use ($unitIds) {
            $query->whereIn('unit_id', $unitIds);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWithoutUnits(Builder $query)
    {
        return $query->whereDoesntHave('units');
    }

    /**
     * @param Builder $query
     * @param int $processorId
     * @return Builder|static
     */
    public function scopeWithInternalProcessor(Builder $query, int $processorId)
    {
        return $query->whereHas("internalProcessors", function ($query) use ($processorId) {
            $query->where('user_id', '=', $processorId);
        });
    }

    /**
     * @param array $securityMeasures
     */
    public function attachSecurityMeasures(array $securityMeasures)
    {
        foreach ($securityMeasures as $sm) {
            if (!$this->securityMeasures->contains($sm))
                $this->securityMeasures()->attach($sm);
        }
    }
}
