<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use App\Libs\RiskEvaluationLogic;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ResidualRisk
 *
 * @property int $id
 * @property int $risk_id
 * @property mixed $likelihood
 * @property mixed $severity
 * @property mixed $risk_level
 * @property-read \App\Models\Risk $risk
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResidualRisk whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResidualRisk whereLikelihood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResidualRisk whereRiskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResidualRisk whereRiskLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResidualRisk whereSeverity($value)
 * @mixin \Eloquent
 */
class ResidualRisk extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpia_residual_risks';

    protected $attributes = [
        'likelihood' => 0,
        'severity'   => 0,
        'risk_level' => 0
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'risk_id',
        'likelihood',
        'severity',
        'risk_level',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'likelihood' => 'numeric',
        'severity'   => 'numeric',
        'risk_level' => 'numeric',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the risks for the measure.
     */
    public function risk()
    {
        return $this->belongsTo('App\Models\Risk', 'risk_id', 'id');
    }

    /**
     * @return $this
     */
    public function calculate()
    {
        $risk = $this->risk()->first();

        $mitigated_vulnerability = $risk->getAttribute('vulnerability');
        $mitigated_risk_sources_capabilities = $risk->getAttribute('risk_sources_capabilities');
        $mitigated_identifiability = $risk->getAttribute('identifiability');
        $mitigated_prejudicial_effect = $risk->getAttribute('prejudicial_effect');

        foreach ($risk->controls()->get() as $control) {
            $mitigated_vulnerability -= $control->pivot->vulnerability;
            $mitigated_risk_sources_capabilities -= $control->pivot->risk_sources_capabilities;
            $mitigated_identifiability -= $control->pivot->identifiability;
            $mitigated_prejudicial_effect -= $control->pivot->prejudicial_effect;
        }

        $this->attributes['likelihood'] =
            RiskEvaluationLogic::calculateLikelihood($mitigated_vulnerability, $mitigated_risk_sources_capabilities);

        $this->attributes['severity'] =
            RiskEvaluationLogic::calculateSeverity($mitigated_identifiability, $mitigated_prejudicial_effect);

        $this->attributes['risk_level'] =
            RiskEvaluationLogic::calculateRiskLevel($this->attributes['severity'], $this->attributes['likelihood']);

        return $this;
    }
}
