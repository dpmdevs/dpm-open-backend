<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Control
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $treatment_type
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Risk[] $risks
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Control onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereTreatmentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Control whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Control withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Control withoutTrashed()
 * @mixin \Eloquent
 */
class Control extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpia_controls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'treatment_type',
        'description',
        'identifiability',
        'prejudicial_effect',
        'vulnerability',
        'risk_sources_capabilities'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the risks for the measure.
     */
    public function risks()
    {
        return $this
            ->belongsToMany('App\Models\Risk',
                'dpia_risk_controls',
                'control_id',
                'risk_id')
            ->withPivot([
                'identifiability',
                'prejudicial_effect',
                'vulnerability',
                'risk_sources_capabilities'
            ]);
    }
}
