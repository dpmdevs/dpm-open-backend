<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use App\Libs\RiskEvaluationLogic;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Risk
 *
 * @property mixed $id
 * @property string $name
 * @property string|null $description
 * @property mixed $dpia_project_id
 * @property string|null $acceptance_motivation
 * @property string $protection_goals
 * @property int $protection_category_id
 * @property mixed $identifiability
 * @property mixed $prejudicial_effect
 * @property mixed $vulnerability
 * @property mixed $risk_sources_capabilities
 * @property int $severity
 * @property int $likelihood
 * @property int $risk_level
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Control[] $controls
 * @property-read \App\Models\ProtectionCategory $protectionCategory
 * @property-read \App\Models\ResidualRisk $residualRisk
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RiskSource[] $riskSources
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk dPIAProject($dpia_project_id)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Risk onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereAcceptanceMotivation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereDpiaProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereIdentifiability($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereLikelihood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk wherePrejudicialEffect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereProtectionCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereProtectionGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereRiskLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereRiskSourcesCapabilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereSeverity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk whereVulnerability($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Risk withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Risk withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Risk protectionCategory($protectionCategoryId)
 */
class Risk extends \Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpia_risks';

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'identifiability'           => 0,
        'prejudicial_effect'        => 0,
        'vulnerability'             => 0,
        'risk_sources_capabilities' => 0,
        'severity'                  => 0,
        'likelihood'                => 0,
        'risk_level'                => 0,
        'acceptance_motivation'     => '',
        'protection_goals'          => ''
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'protection_goals', 'acceptance_motivation', 'dpia_project_id', 'protection_category_id',
        'identifiability', 'prejudicial_effect', 'vulnerability', 'risk_sources_capabilities',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id'                        => 'numeric',
        'dpia_project_id'           => 'numeric',
        'identifiability'           => 'numeric',
        'prejudicial_effect'        => 'numeric',
        'vulnerability'             => 'numeric',
        'risk_sources_capabilities' => 'numeric',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->forceDeleting = true;
    }

    /**
     * Save the model to the database.
     *
     * @param  array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $result = parent::save($options);

        $this->residualRisk()
            ->firstOrCreate(['risk_id' => $this->getKey()])
            ->calculate()
            ->save();

        return $result;
    }

    /**
     * Update the model in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $result = parent::update($attributes, $options);

        $this->residualRisk()
            ->first()
            ->calculate()
            ->save();

        return $result;
    }

    /**
     * Set the risk identifiability and updates severity
     *
     * @param  int $identifiability
     * @return void
     */
    public function setIdentifiabilityAttribute(int $identifiability)
    {
        $this->attributes['identifiability'] = $identifiability;

        $this->updateSeverity();
        $this->updateRiskLevel();
    }

    /**
     * @param int $vulnerability
     */
    public function setVulnerabilityAttribute(int $vulnerability)
    {
        $this->attributes['vulnerability'] = $vulnerability;

        $this->updateLikelihood();
        $this->updateRiskLevel();
    }

    /**
     * @param int $prejudicial_effect
     */
    public function setPrejudicialEffectAttribute(int $prejudicial_effect)
    {
        $this->attributes['prejudicial_effect'] = $prejudicial_effect;

        $this->updateSeverity();
        $this->updateRiskLevel();
    }

    /**
     * @param int $risk_sources_capabilities
     */
    public function setRiskSourcesCapabilitiesAttribute(int $risk_sources_capabilities)
    {
        $this->attributes['risk_sources_capabilities'] = $risk_sources_capabilities;

        $this->updateLikelihood();
        $this->updateRiskLevel();
    }

    /**
     * Get the risks for the measure.
     */
    public function controls()
    {
        return $this
            ->belongsToMany('App\Models\Control',
                'dpia_risk_controls',
                'risk_id',
                'control_id')
            ->withPivot([
                'identifiability',
                'prejudicial_effect',
                'vulnerability',
                'risk_sources_capabilities'
            ]);
    }

    /**
     * Get the risks for the measure.
     */
    public function residualRisk()
    {
        return $this->belongsTo('App\Models\ResidualRisk', 'id', 'risk_id');
    }

    /**
     * Get the protectionCategory for the risk
     */
    public function protectionCategory()
    {
        return $this->belongsTo('App\Models\ProtectionCategory', 'protection_category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function riskSources()
    {
        return $this
            ->belongsToMany('App\Models\RiskSource',
                'dpia_risk_to_risk_sources',
                'risk_id',
                'risk_source_id');
    }

    /**
     * @param int $controlId
     * @param array $attributes
     */
    public function attachControl(int $controlId, array $attributes)
    {
        $residualData = [
            'risk_id' => $this->getAttribute('id'),
        ];

        $this->controls()
            ->attach($controlId, $attributes);

        $this->residualRisk()
            ->firstOrCreate($residualData)
            ->calculate()
            ->save();
    }

    public function detachControl($controlId)
    {
        $this->controls()->detach($controlId);

        $this->residualRisk()
            ->first()
            ->calculate()
            ->save();
    }

    /**
     * Scope a query to only include related project risks.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $dpia_project_id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDPIAProject($query, $dpia_project_id)
    {
        return $query->where('dpia_project_id', '=', $dpia_project_id);
    }

    /**
     * Scope a query to only include related protection goal.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $protectionCategoryId
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProtectionCategory($query, $protectionCategoryId)
    {
        return $query->where('protection_category_id', '=', $protectionCategoryId);
    }

    private function updateLikelihood()
    {
        $this->attributes['likelihood'] =
            RiskEvaluationLogic::calculateLikelihood($this->attributes['vulnerability'], $this->attributes['risk_sources_capabilities']);
    }

    private function updateSeverity()
    {
        $this->attributes['severity'] =
            RiskEvaluationLogic::calculateSeverity($this->attributes['identifiability'], $this->attributes['prejudicial_effect']);
    }

    private function updateRiskLevel()
    {
        $this->attributes['risk_level'] =
            RiskEvaluationLogic::calculateRiskLevel($this->attributes['severity'], $this->attributes['likelihood']);
    }
}
