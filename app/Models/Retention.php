<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Retention
 *
 * @property mixed $id
 * @property string $name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProcessingActivity[] $processActivity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Retention onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retention whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Retention withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Retention withoutTrashed()
 * @mixin \Eloquent
 */
class Retention extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'retentions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'numeric'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name', 'description', 'deleted_at'];

    /**
     * Get the process activity that owns the process Type.
     */
    public function processActivity()
    {
        return $this->belongsToMany('App\Models\ProcessingActivity', 'pa_retentions', 'retention_id', 'pa_id');
    }
}
