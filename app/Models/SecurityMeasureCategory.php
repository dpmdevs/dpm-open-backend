<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SecurityMeasureCategory
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $family
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SecurityMeasure[] $minSecurityMeasures
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasureCategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereFamily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasureCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasureCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasureCategory withoutTrashed()
 * @mixin \Eloquent
 */
class SecurityMeasureCategory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'security_measures_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'family'
    ];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [
        'id', 'name', 'description', 'family', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the min security measure for the category.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function minSecurityMeasures(): HasMany
    {
        return $this->hasMany('App\Models\SecurityMeasure', 'security_measures_categories_id');
    }
}
