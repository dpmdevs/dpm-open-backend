<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Dpo
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dpo onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dpo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dpo withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $company_name
 * @property string|null $company_number
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $title
 * @property string $email
 * @property string|null $phone
 * @property string|null $address
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereCompanyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereUpdatedAt($value)
 * @property string|null $certified_mail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Dpo whereCertifiedMail($value)
 */
class Dpo extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'company_number',
        'firstname',
        'lastname',
        'title',
        'email',
        'phone',
        'address',
        'certified_mail'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'updated_at'];
}
