<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

/**
 * App\Models\unitClosure
 *
 * @property int $closure_id
 * @property int $ancestor
 * @property int $descendant
 * @property int $depth
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\unitClosure whereAncestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\unitClosure whereClosureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\unitClosure whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\unitClosure whereDescendant($value)
 * @mixin \Eloquent
 */
class unitClosure extends ClosureTable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'units_closure';
}
