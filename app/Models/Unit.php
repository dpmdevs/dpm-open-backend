<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Franzose\ClosureTable\Models\Entity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * App\Models\Unit
 *
 * @property int $id
 * @property string $code
 * @property string|null $name
 * @property string|null $description
 * @property int $parent_id
 * @property int $position
 * @property int $real_depth
 * @property string|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $directors
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereRealDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unit withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProcessingActivity[] $processingActivities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit withDirector($directorId)
 */
class Unit extends Entity
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'units';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'description', 'position', 'parent_id'
    ];

    protected $visible = [
        'id', 'code', 'name', 'directors', 'position', 'description', 'deleted_at', 'children'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['updated_at', 'deleted_at'];

    /**
     * ClosureTable model instance.
     *
     * @var unitClosure
     */
    protected $closure = 'App\Models\UnitClosure';

    /**
     * Restores a subtree from database.
     *
     * @param bool $withSelf
     * @return void
     */
    public function restoreSubtree($withSelf = false)
    {
        $ids = $this->getDescendants([$this->getKeyName()], true)->toTree();

        $this->whereIn($this->getKeyName(), $ids)->restore();

        if ($withSelf)
            $this->restore();
    }

    /**
     * Retrieves all descendants of a model.
     *
     * @param array $columns
     * @param bool $withTrashed
     * @return Collection
     */
    public function getDescendants(array $columns = ['*'], $withTrashed = false)
    {
        $query = $this->joinClosureBy('descendant');

        if ($withTrashed) return $query->withTrashed()->get($columns);
        else return $query->get($columns);
    }

    /**
     * Get the directors for the units.
     */
    public function directors()
    {
        return $this
            ->belongsToMany('App\Models\User',
                'unit_directors',
                'unit_id',
                'user_id')
            ->withTrashed()
            ->withTimestamps();
    }

    /**
     * Get the directors for the units.
     */
    public function processingActivities()
    {
        return $this
            ->belongsToMany(
                'App\Models\ProcessingActivity',
                'pa_units',
                'unit_id',
                'pa_id')
            ->withTimestamps();
    }

    /**
     * @param Builder $query
     * @param int $directorId
     * @return Builder|static
     */
    public function scopeWithDirector(Builder $query, int $directorId)
    {
        return $query->whereHas("directors", function ($query) use ($directorId) {
            $query->where('user_id', '=', $directorId);
        });
    }
}
