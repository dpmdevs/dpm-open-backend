<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RiskSource
 *
 * @property mixed $id
 * @property string $name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Risk[] $risks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RiskSource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RiskSource whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RiskSource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RiskSource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RiskSource whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RiskSource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dpia_risk_sources';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'numeric'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function risks()
    {
        return $this
            ->belongsToMany('App\Models\Risk',
                'dpia_risk_to_risk_sources',
                'risk_source_id',
                'risk_id');
    }
}
