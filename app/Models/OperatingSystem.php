<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\OperatingSystem
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property bool $is_server
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem isClient()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem isServer()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperatingSystem whereIsServer($value)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperatingSystem onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperatingSystem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperatingSystem withoutTrashed()
 */
class OperatingSystem extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'operating_systems';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_server'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'is_server' => 'boolean',
    ];

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeIsServer(Builder $query) {
        return $query->whereIsServer(true);
    }

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeIsClient(Builder $query) {
        return $query->whereIsServer(false);
    }
}
