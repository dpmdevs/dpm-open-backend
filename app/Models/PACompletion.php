<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

/**
 * App\Models\PACompletion
 *
 * @property int $id
 * @property mixed $pa_id
 * @property mixed $step_id
 * @property bool $visited
 * @property bool $completed
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion wherePaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereStepId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PACompletion whereVisited($value)
 * @mixin \Eloquent
 */
class PACompletion extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pa_completion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pa_id', 'step_id', 'visited', 'completed'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['step_id', 'visited', 'completed'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'pa_id'     => 'numeric',
        'step_id'   => 'numeric',
        'visited'   => 'boolean',
        'completed' => 'boolean'
    ];
}
