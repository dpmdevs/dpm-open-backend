<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Server
 *
 * @property int $id
 * @property string|null $brand
 * @property string|null $supplier
 * @property string|null $model
 * @property string $description
 * @property string|null $ip
 * @property string|null $notes
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Application[] $applications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereSupplier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Server withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $operating_system_id
 * @property-read \App\Models\OperatingSystem|null $operatingSystem
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereOperatingSystemId($value)
 * @property string|null $hostname
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Server whereHostname($value)
 */
class Server extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'supplier', 'hostname', 'brand', 'model', 'ip', 'notes', 'operating_system_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['updated_at', 'created_at', 'deleted_at'];

    /**
     * Get applications for the third party.
     */
    public function applications()
    {
        return $this
            ->belongsToMany('App\Models\Application',
                'app_servers',
                'server_id',
                'application_id')
            ->withTimestamps();
    }

    /**
     * Get the operating system for the server.
     */
    public function operatingSystem()
    {
        return $this->belongsTo(
            'App\Models\OperatingSystem',
            'operating_system_id',
            'id');
    }
}
