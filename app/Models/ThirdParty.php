<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ThirdParty
 *
 * @property int $id
 * @property string $company_name
 * @property string|null $description
 * @property string|null $registration_number
 * @property string|null $processor_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Application[] $applications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereProcessorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereRegistrationNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ThirdParty whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ThirdParty withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProcessingActivity[] $processingActivities
 */
class ThirdParty extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'third_parties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 'description', 'processor_name',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get applications for the third party.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this
            ->belongsToMany('App\Models\Application',
                'app_third_parties',
                'third_party_id',
                'application_id')
            ->withTimestamps();
    }

    /**
     * Get processing activities for the third party.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function processingActivities()
    {
        return $this
            ->belongsToMany('App\Models\ProcessingActivity',
                'pa_external_processors',
                'third_parties_id',
                'pa_id')
            ->withTimestamps();
    }
}
