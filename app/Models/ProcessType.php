<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

/**
 * App\Models\ProcessType
 *
 * @property mixed $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProcessingActivity[] $processActivity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProcessType whereName($value)
 * @mixin \Eloquent
 */
class ProcessType extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'process_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'numeric'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name'];


    public $timestamps = false;

    /**
     * Get the process activity that owns the process Type.
     */
    public function processActivity()
    {
        return $this->belongsToMany('App\Models\ProcessingActivity', 'pa_process_types', 'process_type_id', 'pa_id');
    }
}
