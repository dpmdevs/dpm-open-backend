<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SecurityMeasure
 *
 * @property mixed $id
 * @property string $name
 * @property mixed $security_level
 * @property int $security_measures_categories_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasure onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereSecurityLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereSecurityMeasuresCategoriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasure withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SecurityMeasure withoutTrashed()
 * @mixin \Eloquent
 * @property bool $is_adopted
 * @property-read \App\Models\SecurityMeasureCategory $securityMeasureCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure isAdopted($adopted = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure whereIsAdopted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SecurityMeasure withCategoryFamily($family = true)
 */
class SecurityMeasure extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'security_measures';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'security_level', 'is_adopted', 'security_measures_categories_id'
    ];

    /**
     * The attributes that should be hidden in serialization.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'id'             => 'numeric',
        'security_level' => 'numeric',
        'answer'         => 'boolean',
        'is_adopted'     => 'boolean'
    ];

    public function securityMeasureCategory()
    {
        return $this->belongsTo(
            'App\Models\SecurityMeasureCategory',
            'security_measures_categories_id',
            'id');
    }

    /**
     * @param Builder $query
     * @param bool $adopted
     * @return $this
     */
    public function scopeIsAdopted(Builder $query, $adopted = true)
    {
        return $query->where('is_adopted', '=', $adopted);
    }

    public function scopeWithCategoryFamily(Builder $query, $family = true)
    {
        return $query->whereHas('securityMeasureCategory', function (Builder $subQuery) use ($family) {
            $subQuery->where('family', '=', $family);
        });
    }
}
