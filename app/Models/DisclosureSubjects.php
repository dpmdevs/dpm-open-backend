<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DisclosureSubjects
 *
 * @property mixed $id
 * @property bool $extra_ue
 * @property string $company_name
 * @property int $pa_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $answers
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DisclosureSubjects onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereExtraUe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects wherePaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DisclosureSubjects whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DisclosureSubjects withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DisclosureSubjects withoutTrashed()
 * @mixin \Eloquent
 */
class DisclosureSubjects extends \Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pa_disclosure_subjects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name', 'extra_ue'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'numeric',
        'extra_ue' => 'boolean'
    ];


    public function answers()
    {
        return $this->belongsToMany(
            'App\Models\Question',
            'disclosure_subjects_answers',
            'disc_subj_id',
            'question_id'
        )
            ->withPivot('answer');
    }

}
