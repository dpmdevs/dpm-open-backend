<?php
/**
 *     Data Protection Manager (DPM) - Open Source GDPR Compliance
 *     Copyright (C)  2018  Studio Storti Srl
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DataType
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $data_category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DataType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereDataCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DataType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DataType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DataType withoutTrashed()
 * @mixin \Eloquent
 */
class DataType extends Eloquent
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'data_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [
        'id', 'name', 'description', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
