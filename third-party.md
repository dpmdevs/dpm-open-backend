##DPM dpm-backend Third Party Licenses
###Components Licensed Under the [MIT License](https://opensource.org/licenses/MIT)

[Laravel framework](https://github.com/laravel/laravel) Copyright © Taylor Otwell

[Laravel tinker](https://github.com/laravel/tinker) Copyright © Taylor Otwell

[Adldap2-Laravel](https://github.com/Adldap2/Adldap2-Laravel) Copyright © Steve Bauman

[doctrine/dbal](https://github.com/doctrine/dbal) Copyright © Doctrine Project

[franzose/ClosureTable](https://github.com/franzose/ClosureTable) Copyright © Jan Iwanow

[guzzlehttp/guzzle](https://github.com/guzzle/guzzle/) Copyright © Michael Dowling

[Intervention/image](https://github.com/Intervention/image) Copyright © Oliver Vogel

[tymon/jwt-auth](https://github.com/tymondesigns/jwt-auth) Copyright © Sean Tymon

[Zizaco/entrust](https://github.com/Zizaco/entrust/blob/master/composer.json) Copyright © Zizaco/entrust authors



## Full text MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.